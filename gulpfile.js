const gulp = require('gulp');
const less = require('gulp-less');
const jade = require('gulp-jade');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const del = require('del');
const browserSync = require('browser-sync').create();
const notify = require('gulp-notify');
const path = require('path');
const data = require('gulp-data');
const include = require('gulp-include');
const flatten = require('gulp-flatten');
const rename = require("gulp-rename");

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

// --------------------------------------------------------------

gulp.task('clean', function () {
  return del('app');
});


gulp.task('html', function () {

  return gulp.src('assets/index.jade')
    .pipe(jade({
        pretty: true
    }))
    .pipe(rename("index.php"))
    .pipe(gulp.dest('app/'));

});


gulp.task('styles', function () {

  return gulp.src('assets/index.less')
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(less({
        paths: [ path.join(__dirname, 'less', 'includes') ]
      }))
    .on('error', notify.onError(function (err) {
      return {
        title: 'Less crash',
        message: err.message
      }
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('app/'));

});


gulp.task('js', function () {
  return gulp.src('assets/js/index.js')
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(include())
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('app/js'));
});


gulp.task('img', function () {
  return gulp.src('app/img/*.*')
    .pipe(gulp.dest('app'));
});


gulp.task('assets', function () {
  return gulp.src(['assets/fonts/**', 'assets/libs/**', 'assets/img/**'], {base: 'assets', since: gulp.lastRun('assets')})
    .pipe(gulp.dest('app/'));
});

gulp.task('test_php', function () {
  return gulp.src('app/**/*.*')
    .pipe(gulp.dest('/home/stasizon/server/local.io/adminPanel'));
});

// --------------------------------------------------------------

gulp.task('serve', function () {
  browserSync.init({
    server: 'app'
  });

  browserSync.watch('app/**/*.*').on('change', browserSync.reload);
});

gulp.task('watch', function () {
    gulp.watch(['assets/blocks/**/*.jade', 'assets/index.jade'], gulp.series('html', 'test_php'));
    gulp.watch(['assets/blocks/**/*.less', 'assets/index.less'], gulp.series('styles', 'test_php'));
    gulp.watch('assets/img/', gulp.series('img'));
    gulp.watch('assets/libs/**/*.*', gulp.series('assets'));
    gulp.watch('assets/fonts/**/*.*', gulp.series('assets'));
    gulp.watch('assets/db/**/*.*', gulp.series('test_php'));
    gulp.watch('assets/js/**/*.*', gulp.series('js', 'test_php'));
});

gulp.task('build', gulp.series('clean', 'html', 'styles', 'js', 'img', 'assets', 'test_php'));

gulp.task('dev', gulp.series('build', gulp.parallel(['watch', 'serve'])));
