# AdminPanel API

> **Note:**

> All requests need send to **/connects/connects.php**

> Settings for setup database and admin email: **/classes/_class.config.php**


## Product List

#####Get products:
```javascript
$http.post('/connects/connects.php', {
	type: 'read',
	item: 'product'
});
```

#####Write products:
```javascript
$http.post('/connects/connects.php', {
	type: 'write',
	item: 'product',
	product1: {
		name: 'Example name',
		description: 'Example description',
		price: '100',
		...
	}
	product2: {
		...
	}
});
```

##Custom information:

##### Write:
```javascript
$http.post('/connects/connects.php', {
	type: 'write',
	item: 'configs',
	configs1: {
		name: 'mainTextHeader',
		content: 'Exaple text in Header'
		...
	}
	configs2: {
		...
	}
});
```

##### Read:
```javascript
$http.post('/connects/connects.php', {
	type: 'read',
	item: 'configs'
});
```

##Read history of order:

```javascript
$http.post('/connects/connects.php', {
	type: 'read',
	item: 'mail'
});
```