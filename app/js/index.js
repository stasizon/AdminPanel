(function() {

	var app = angular.module('navigation', []);

    app.controller('NavigationController', function() {

        this.showOnWorkField = function(element, event) {

			for (var i = 0; i < document.getElementById('nav').children.length; i++) {
                document.getElementById('nav').children[i].classList.remove('nav__item-active');
            }

			event.target.classList.add('nav__item-active');

            for (var i = 0; i < document.getElementById('workField').children.length; i++) {
                document.getElementById('workField').children[i].style.display = 'none';
            }

    	    document.getElementById(element).style.display = 'block';
    	}

    });

})();

/*
 * ngDialog - easy modals and popup windows
 * http://github.com/likeastore/ngDialog
 * (c) 2013-2015 MIT License, https://likeastore.com
 */

(function (root, factory) {
    if (typeof module !== 'undefined' && module.exports) {
        // CommonJS
        if (typeof angular === 'undefined') {
            factory(require('angular'));
        } else {
            factory(angular);
        }
        module.exports = 'ngDialog';
    } else if (typeof define === 'function' && define.amd) {
        // AMD
        define(['angular'], factory);
    } else {
        // Global Variables
        factory(root.angular);
    }
}(this, function (angular) {
    'use strict';

    var m = angular.module('ngDialog', []);

    var $el = angular.element;
    var isDef = angular.isDefined;
    var style = (document.body || document.documentElement).style;
    var animationEndSupport = isDef(style.animation) || isDef(style.WebkitAnimation) || isDef(style.MozAnimation) || isDef(style.MsAnimation) || isDef(style.OAnimation);
    var animationEndEvent = 'animationend webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend';
    var focusableElementSelector = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';
    var disabledAnimationClass = 'ngdialog-disabled-animation';
    var forceElementsReload = { html: false, body: false };
    var scopes = {};
    var openIdStack = [];
    var keydownIsBound = false;
    var openOnePerName = false;


    m.provider('ngDialog', function () {
        var defaults = this.defaults = {
            className: 'ngdialog-theme-default',
            appendClassName: '',
            disableAnimation: false,
            plain: false,
            showClose: true,
            closeByDocument: true,
            closeByEscape: true,
            closeByNavigation: false,
            appendTo: false,
            preCloseCallback: false,
            overlay: true,
            cache: true,
            trapFocus: true,
            preserveFocus: true,
            ariaAuto: true,
            ariaRole: null,
            ariaLabelledById: null,
            ariaLabelledBySelector: null,
            ariaDescribedById: null,
            ariaDescribedBySelector: null,
            bodyClassName: 'ngdialog-open',
            width: null,
            height: null
        };

        this.setForceHtmlReload = function (_useIt) {
            forceElementsReload.html = _useIt || false;
        };

        this.setForceBodyReload = function (_useIt) {
            forceElementsReload.body = _useIt || false;
        };

        this.setDefaults = function (newDefaults) {
            angular.extend(defaults, newDefaults);
        };

        this.setOpenOnePerName = function (isOpenOne) {
            openOnePerName = isOpenOne || false;
        };

        var globalID = 0, dialogsCount = 0, closeByDocumentHandler, defers = {};

        this.$get = ['$document', '$templateCache', '$compile', '$q', '$http', '$rootScope', '$timeout', '$window', '$controller', '$injector',
            function ($document, $templateCache, $compile, $q, $http, $rootScope, $timeout, $window, $controller, $injector) {
                var $elements = [];

                var privateMethods = {
                    onDocumentKeydown: function (event) {
                        if (event.keyCode === 27) {
                            publicMethods.close('$escape');
                        }
                    },

                    activate: function($dialog) {
                        var options = $dialog.data('$ngDialogOptions');

                        if (options.trapFocus) {
                            $dialog.on('keydown', privateMethods.onTrapFocusKeydown);

                            // Catch rogue changes (eg. after unfocusing everything by clicking a non-focusable element)
                            $elements.body.on('keydown', privateMethods.onTrapFocusKeydown);
                        }
                    },

                    deactivate: function ($dialog) {
                        $dialog.off('keydown', privateMethods.onTrapFocusKeydown);
                        $elements.body.off('keydown', privateMethods.onTrapFocusKeydown);
                    },

                    deactivateAll: function (els) {
                        angular.forEach(els,function(el) {
                            var $dialog = angular.element(el);
                            privateMethods.deactivate($dialog);
                        });
                    },

                    setBodyPadding: function (width) {
                        var originalBodyPadding = parseInt(($elements.body.css('padding-right') || 0), 10);
                        $elements.body.css('padding-right', (originalBodyPadding + width) + 'px');
                        $elements.body.data('ng-dialog-original-padding', originalBodyPadding);
                        $rootScope.$broadcast('ngDialog.setPadding', width);
                    },

                    resetBodyPadding: function () {
                        var originalBodyPadding = $elements.body.data('ng-dialog-original-padding');
                        if (originalBodyPadding) {
                            $elements.body.css('padding-right', originalBodyPadding + 'px');
                        } else {
                            $elements.body.css('padding-right', '');
                        }
                        $rootScope.$broadcast('ngDialog.setPadding', 0);
                    },

                    performCloseDialog: function ($dialog, value) {
                        var options = $dialog.data('$ngDialogOptions');
                        var id = $dialog.attr('id');
                        var scope = scopes[id];

                        if (!scope) {
                            // Already closed
                            return;
                        }

                        if (typeof $window.Hammer !== 'undefined') {
                            var hammerTime = scope.hammerTime;
                            hammerTime.off('tap', closeByDocumentHandler);
                            hammerTime.destroy && hammerTime.destroy();
                            delete scope.hammerTime;
                        } else {
                            $dialog.unbind('click');
                        }

                        if (dialogsCount === 1) {
                            $elements.body.unbind('keydown', privateMethods.onDocumentKeydown);
                        }

                        if (!$dialog.hasClass('ngdialog-closing')){
                            dialogsCount -= 1;
                        }

                        var previousFocus = $dialog.data('$ngDialogPreviousFocus');
                        if (previousFocus && previousFocus.focus) {
                            previousFocus.focus();
                        }

                        $rootScope.$broadcast('ngDialog.closing', $dialog, value);
                        dialogsCount = dialogsCount < 0 ? 0 : dialogsCount;
                        if (animationEndSupport && !options.disableAnimation) {
                            scope.$destroy();
                            $dialog.unbind(animationEndEvent).bind(animationEndEvent, function () {
                                privateMethods.closeDialogElement($dialog, value);
                            }).addClass('ngdialog-closing');
                        } else {
                            scope.$destroy();
                            privateMethods.closeDialogElement($dialog, value);
                        }
                        if (defers[id]) {
                            defers[id].resolve({
                                id: id,
                                value: value,
                                $dialog: $dialog,
                                remainingDialogs: dialogsCount
                            });
                            delete defers[id];
                        }
                        if (scopes[id]) {
                            delete scopes[id];
                        }
                        openIdStack.splice(openIdStack.indexOf(id), 1);
                        if (!openIdStack.length) {
                            $elements.body.unbind('keydown', privateMethods.onDocumentKeydown);
                            keydownIsBound = false;
                        }
                    },

                    closeDialogElement: function($dialog, value) {
                        var options = $dialog.data('$ngDialogOptions');
                        $dialog.remove();
                        if (dialogsCount === 0) {
                            $elements.html.removeClass(options.bodyClassName);
                            $elements.body.removeClass(options.bodyClassName);
                            privateMethods.resetBodyPadding();
                        }
                        $rootScope.$broadcast('ngDialog.closed', $dialog, value);
                    },

                    closeDialog: function ($dialog, value) {
                        var preCloseCallback = $dialog.data('$ngDialogPreCloseCallback');

                        if (preCloseCallback && angular.isFunction(preCloseCallback)) {

                            var preCloseCallbackResult = preCloseCallback.call($dialog, value);

                            if (angular.isObject(preCloseCallbackResult)) {
                                if (preCloseCallbackResult.closePromise) {
                                    preCloseCallbackResult.closePromise.then(function () {
                                        privateMethods.performCloseDialog($dialog, value);
                                    }, function () {
                                        return false;
                                    });
                                } else {
                                    preCloseCallbackResult.then(function () {
                                        privateMethods.performCloseDialog($dialog, value);
                                    }, function () {
                                        return false;
                                    });
                                }
                            } else if (preCloseCallbackResult !== false) {
                                privateMethods.performCloseDialog($dialog, value);
                            } else {
                                return false;
                            }
                        } else {
                            privateMethods.performCloseDialog($dialog, value);
                        }
                    },

                    onTrapFocusKeydown: function(ev) {
                        var el = angular.element(ev.currentTarget);
                        var $dialog;

                        if (el.hasClass('ngdialog')) {
                            $dialog = el;
                        } else {
                            $dialog = privateMethods.getActiveDialog();

                            if ($dialog === null) {
                                return;
                            }
                        }

                        var isTab = (ev.keyCode === 9);
                        var backward = (ev.shiftKey === true);

                        if (isTab) {
                            privateMethods.handleTab($dialog, ev, backward);
                        }
                    },

                    handleTab: function($dialog, ev, backward) {
                        var focusableElements = privateMethods.getFocusableElements($dialog);

                        if (focusableElements.length === 0) {
                            if (document.activeElement && document.activeElement.blur) {
                                document.activeElement.blur();
                            }
                            return;
                        }

                        var currentFocus = document.activeElement;
                        var focusIndex = Array.prototype.indexOf.call(focusableElements, currentFocus);

                        var isFocusIndexUnknown = (focusIndex === -1);
                        var isFirstElementFocused = (focusIndex === 0);
                        var isLastElementFocused = (focusIndex === focusableElements.length - 1);

                        var cancelEvent = false;

                        if (backward) {
                            if (isFocusIndexUnknown || isFirstElementFocused) {
                                focusableElements[focusableElements.length - 1].focus();
                                cancelEvent = true;
                            }
                        } else {
                            if (isFocusIndexUnknown || isLastElementFocused) {
                                focusableElements[0].focus();
                                cancelEvent = true;
                            }
                        }

                        if (cancelEvent) {
                            ev.preventDefault();
                            ev.stopPropagation();
                        }
                    },

                    autoFocus: function($dialog) {
                        var dialogEl = $dialog[0];

                        // Browser's (Chrome 40, Forefix 37, IE 11) don't appear to honor autofocus on the dialog, but we should
                        var autoFocusEl = dialogEl.querySelector('*[autofocus]');
                        if (autoFocusEl !== null) {
                            autoFocusEl.focus();

                            if (document.activeElement === autoFocusEl) {
                                return;
                            }

                            // Autofocus element might was display: none, so let's continue
                        }

                        var focusableElements = privateMethods.getFocusableElements($dialog);

                        if (focusableElements.length > 0) {
                            focusableElements[0].focus();
                            return;
                        }

                        // We need to focus something for the screen readers to notice the dialog
                        var contentElements = privateMethods.filterVisibleElements(dialogEl.querySelectorAll('h1,h2,h3,h4,h5,h6,p,span'));

                        if (contentElements.length > 0) {
                            var contentElement = contentElements[0];
                            $el(contentElement).attr('tabindex', '-1').css('outline', '0');
                            contentElement.focus();
                        }
                    },

                    getFocusableElements: function ($dialog) {
                        var dialogEl = $dialog[0];

                        var rawElements = dialogEl.querySelectorAll(focusableElementSelector);

                        // Ignore untabbable elements, ie. those with tabindex = -1
                        var tabbableElements = privateMethods.filterTabbableElements(rawElements);

                        return privateMethods.filterVisibleElements(tabbableElements);
                    },

                    filterTabbableElements: function (els) {
                        var tabbableFocusableElements = [];

                        for (var i = 0; i < els.length; i++) {
                            var el = els[i];

                            if ($el(el).attr('tabindex') !== '-1') {
                                tabbableFocusableElements.push(el);
                            }
                        }

                        return tabbableFocusableElements;
                    },

                    filterVisibleElements: function (els) {
                        var visibleFocusableElements = [];

                        for (var i = 0; i < els.length; i++) {
                            var el = els[i];

                            if (el.offsetWidth > 0 || el.offsetHeight > 0) {
                                visibleFocusableElements.push(el);
                            }
                        }

                        return visibleFocusableElements;
                    },

                    getActiveDialog: function () {
                        var dialogs = document.querySelectorAll('.ngdialog');

                        if (dialogs.length === 0) {
                            return null;
                        }

                        // TODO: This might be incorrect if there are a mix of open dialogs with different 'appendTo' values
                        return $el(dialogs[dialogs.length - 1]);
                    },

                    applyAriaAttributes: function ($dialog, options) {
                        if (options.ariaAuto) {
                            if (!options.ariaRole) {
                                var detectedRole = (privateMethods.getFocusableElements($dialog).length > 0) ?
                                    'dialog' :
                                    'alertdialog';

                                options.ariaRole = detectedRole;
                            }

                            if (!options.ariaLabelledBySelector) {
                                options.ariaLabelledBySelector = 'h1,h2,h3,h4,h5,h6';
                            }

                            if (!options.ariaDescribedBySelector) {
                                options.ariaDescribedBySelector = 'article,section,p';
                            }
                        }

                        if (options.ariaRole) {
                            $dialog.attr('role', options.ariaRole);
                        }

                        privateMethods.applyAriaAttribute(
                            $dialog, 'aria-labelledby', options.ariaLabelledById, options.ariaLabelledBySelector);

                        privateMethods.applyAriaAttribute(
                            $dialog, 'aria-describedby', options.ariaDescribedById, options.ariaDescribedBySelector);
                    },

                    applyAriaAttribute: function($dialog, attr, id, selector) {
                        if (id) {
                            $dialog.attr(attr, id);
                        }

                        if (selector) {
                            var dialogId = $dialog.attr('id');

                            var firstMatch = $dialog[0].querySelector(selector);

                            if (!firstMatch) {
                                return;
                            }

                            var generatedId = dialogId + '-' + attr;

                            $el(firstMatch).attr('id', generatedId);

                            $dialog.attr(attr, generatedId);

                            return generatedId;
                        }
                    },

                    detectUIRouter: function() {
                        //Detect if ui-router module is installed if not return false
                        try {
                            angular.module('ui.router');
                            return true;
                        } catch(err) {
                            return false;
                        }
                    },

                    getRouterLocationEventName: function() {
                        if(privateMethods.detectUIRouter()) {
                            return '$stateChangeStart';
                        }
                        return '$locationChangeStart';
                    }
                };

                var publicMethods = {
                    __PRIVATE__: privateMethods,

                    /*
                     * @param {Object} options:
                     * - template {String} - id of ng-template, url for partial, plain string (if enabled)
                     * - plain {Boolean} - enable plain string templates, default false
                     * - scope {Object}
                     * - controller {String}
                     * - controllerAs {String}
                     * - className {String} - dialog theme class
                     * - appendClassName {String} - dialog theme class to be appended to defaults
                     * - disableAnimation {Boolean} - set to true to disable animation
                     * - showClose {Boolean} - show close button, default true
                     * - closeByEscape {Boolean} - default true
                     * - closeByDocument {Boolean} - default true
                     * - preCloseCallback {String|Function} - user supplied function name/function called before closing dialog (if set)
                     * - bodyClassName {String} - class added to body at open dialog
                     * @return {Object} dialog
                     */
                    open: function (opts) {
                        var dialogID = null;
                        opts = opts || {};
                        if (openOnePerName && opts.name) {
                            dialogID = opts.name+' dialog';
                            if (this.isOpen(dialogID)) {
                                return;
                            }
                        }
                        var options = angular.copy(defaults);
                        var localID = ++globalID;
                        dialogID = dialogID || 'ngdialog' + localID;
                        openIdStack.push(dialogID);

                        // Merge opts.data with predefined via setDefaults
                        if (typeof options.data !== 'undefined') {
                            if (typeof opts.data === 'undefined') {
                                opts.data = {};
                            }
                            opts.data = angular.merge(angular.copy(options.data), opts.data);
                        }

                        angular.extend(options, opts);

                        var defer;
                        defers[dialogID] = defer = $q.defer();

                        var scope;
                        scopes[dialogID] = scope = angular.isObject(options.scope) ? options.scope.$new() : $rootScope.$new();

                        var $dialog, $dialogParent, $dialogContent;

                        var resolve = angular.extend({}, options.resolve);

                        angular.forEach(resolve, function (value, key) {
                            resolve[key] = angular.isString(value) ? $injector.get(value) : $injector.invoke(value, null, null, key);
                        });

                        $q.all({
                            template: loadTemplate(options.template || options.templateUrl),
                            locals: $q.all(resolve)
                        }).then(function (setup) {
                            var template = setup.template,
                                locals = setup.locals;

                            if (options.showClose) {
                                template += '<div class="ngdialog-close"></div>';
                            }

                            var hasOverlayClass = options.overlay ? '' : ' ngdialog-no-overlay';
                            $dialog = $el('<div id="'+dialogID + '" class="ngdialog' + hasOverlayClass + '"></div>');
                            $dialog.html((options.overlay ?
                                '<div class="ngdialog-overlay"></div><div class="ngdialog-content" role="document">' + template + '</div>' :
                                '<div class="ngdialog-content" role="document">' + template + '</div>'));

                            $dialog.data('$ngDialogOptions', options);

                            scope.ngDialogId = dialogID;

                            if (options.data && angular.isString(options.data)) {
                                var firstLetter = options.data.replace(/^\s*/, '')[0];
                                scope.ngDialogData = (firstLetter === '{' || firstLetter === '[') ? angular.fromJson(options.data) : new String(options.data);
                                scope.ngDialogData.ngDialogId = dialogID;
                            } else if (options.data && angular.isObject(options.data)) {
                                scope.ngDialogData = options.data;
                                scope.ngDialogData.ngDialogId = dialogID;
                            }

                            if (options.className) {
                                $dialog.addClass(options.className);
                            }

                            if (options.appendClassName) {
                                $dialog.addClass(options.appendClassName);
                            }

                            if (options.width) {
                                $dialogContent = $dialog[0].querySelector('.ngdialog-content');
                                if (angular.isString(options.width)) {
                                    $dialogContent.style.width = options.width;
                                } else {
                                    $dialogContent.style.width = options.width + 'px';
                                }
                            }

                            if (options.height) {
                                $dialogContent = $dialog[0].querySelector('.ngdialog-content');
                                if (angular.isString(options.height)) {
                                    $dialogContent.style.height = options.height;
                                } else {
                                    $dialogContent.style.height = options.height + 'px';
                                }
                            }

                            if (options.disableAnimation) {
                                $dialog.addClass(disabledAnimationClass);
                            }

                            if (options.appendTo && angular.isString(options.appendTo)) {
                                $dialogParent = angular.element(document.querySelector(options.appendTo));
                            } else {
                                $dialogParent = $elements.body;
                            }

                            privateMethods.applyAriaAttributes($dialog, options);

                            if (options.preCloseCallback) {
                                var preCloseCallback;

                                if (angular.isFunction(options.preCloseCallback)) {
                                    preCloseCallback = options.preCloseCallback;
                                } else if (angular.isString(options.preCloseCallback)) {
                                    if (scope) {
                                        if (angular.isFunction(scope[options.preCloseCallback])) {
                                            preCloseCallback = scope[options.preCloseCallback];
                                        } else if (scope.$parent && angular.isFunction(scope.$parent[options.preCloseCallback])) {
                                            preCloseCallback = scope.$parent[options.preCloseCallback];
                                        } else if ($rootScope && angular.isFunction($rootScope[options.preCloseCallback])) {
                                            preCloseCallback = $rootScope[options.preCloseCallback];
                                        }
                                    }
                                }

                                if (preCloseCallback) {
                                    $dialog.data('$ngDialogPreCloseCallback', preCloseCallback);
                                }
                            }

                            scope.closeThisDialog = function (value) {
                                privateMethods.closeDialog($dialog, value);
                            };

                            if (options.controller && (angular.isString(options.controller) || angular.isArray(options.controller) || angular.isFunction(options.controller))) {

                                var label;

                                if (options.controllerAs && angular.isString(options.controllerAs)) {
                                    label = options.controllerAs;
                                }

                                var controllerInstance = $controller(options.controller, angular.extend(
                                    locals,
                                    {
                                        $scope: scope,
                                        $element: $dialog
                                    }),
                                    true,
                                    label
                                );

                                if(options.bindToController) {
                                    angular.extend(controllerInstance.instance, {ngDialogId: scope.ngDialogId, ngDialogData: scope.ngDialogData, closeThisDialog: scope.closeThisDialog, confirm: scope.confirm});
                                }

                                if(typeof controllerInstance === 'function'){
                                    $dialog.data('$ngDialogControllerController', controllerInstance());
                                } else {
                                    $dialog.data('$ngDialogControllerController', controllerInstance);
                                }
                            }

                            $timeout(function () {
                                var $activeDialogs = document.querySelectorAll('.ngdialog');
                                privateMethods.deactivateAll($activeDialogs);

                                $compile($dialog)(scope);
                                var widthDiffs = $window.innerWidth - $elements.body.prop('clientWidth');
                                $elements.html.addClass(options.bodyClassName);
                                $elements.body.addClass(options.bodyClassName);
                                var scrollBarWidth = widthDiffs - ($window.innerWidth - $elements.body.prop('clientWidth'));
                                if (scrollBarWidth > 0) {
                                    privateMethods.setBodyPadding(scrollBarWidth);
                                }
                                $dialogParent.append($dialog);

                                privateMethods.activate($dialog);

                                if (options.trapFocus) {
                                    privateMethods.autoFocus($dialog);
                                }

                                if (options.name) {
                                    $rootScope.$broadcast('ngDialog.opened', {dialog: $dialog, name: options.name});
                                } else {
                                    $rootScope.$broadcast('ngDialog.opened', $dialog);
                                }
                            });

                            if (!keydownIsBound) {
                                $elements.body.bind('keydown', privateMethods.onDocumentKeydown);
                                keydownIsBound = true;
                            }

                            if (options.closeByNavigation) {
                                var eventName = privateMethods.getRouterLocationEventName();
                                $rootScope.$on(eventName, function ($event) {
                                    if (privateMethods.closeDialog($dialog) === false)
                                        $event.preventDefault();
                                });
                            }

                            if (options.preserveFocus) {
                                $dialog.data('$ngDialogPreviousFocus', document.activeElement);
                            }

                            closeByDocumentHandler = function (event) {
                                var isOverlay = options.closeByDocument ? $el(event.target).hasClass('ngdialog-overlay') : false;
                                var isCloseBtn = $el(event.target).hasClass('ngdialog-close');

                                if (isOverlay || isCloseBtn) {
                                    publicMethods.close($dialog.attr('id'), isCloseBtn ? '$closeButton' : '$document');
                                }
                            };

                            if (typeof $window.Hammer !== 'undefined') {
                                var hammerTime = scope.hammerTime = $window.Hammer($dialog[0]);
                                hammerTime.on('tap', closeByDocumentHandler);
                            } else {
                                $dialog.bind('click', closeByDocumentHandler);
                            }

                            dialogsCount += 1;

                            return publicMethods;
                        });

                        return {
                            id: dialogID,
                            closePromise: defer.promise,
                            close: function (value) {
                                privateMethods.closeDialog($dialog, value);
                            }
                        };

                        function loadTemplateUrl (tmpl, config) {
                            $rootScope.$broadcast('ngDialog.templateLoading', tmpl);
                            return $http.get(tmpl, (config || {})).then(function(res) {
                                $rootScope.$broadcast('ngDialog.templateLoaded', tmpl);
                                return res.data || '';
                            });
                        }

                        function loadTemplate (tmpl) {
                            if (!tmpl) {
                                return 'Empty template';
                            }

                            if (angular.isString(tmpl) && options.plain) {
                                return tmpl;
                            }

                            if (typeof options.cache === 'boolean' && !options.cache) {
                                return loadTemplateUrl(tmpl, {cache: false});
                            }

                            return loadTemplateUrl(tmpl, {cache: $templateCache});
                        }
                    },

                    /*
                     * @param {Object} options:
                     * - template {String} - id of ng-template, url for partial, plain string (if enabled)
                     * - plain {Boolean} - enable plain string templates, default false
                     * - name {String}
                     * - scope {Object}
                     * - controller {String}
                     * - controllerAs {String}
                     * - className {String} - dialog theme class
                     * - appendClassName {String} - dialog theme class to be appended to defaults
                     * - showClose {Boolean} - show close button, default true
                     * - closeByEscape {Boolean} - default false
                     * - closeByDocument {Boolean} - default false
                     * - preCloseCallback {String|Function} - user supplied function name/function called before closing dialog (if set); not called on confirm
                     * - bodyClassName {String} - class added to body at open dialog
                     *
                     * @return {Object} dialog
                     */
                    openConfirm: function (opts) {
                        var defer = $q.defer();
                        var options = angular.copy(defaults);

                        opts = opts || {};

                        // Merge opts.data with predefined via setDefaults
                        if (typeof options.data !== 'undefined') {
                            if (typeof opts.data === 'undefined') {
                                opts.data = {};
                            }
                            opts.data = angular.merge(angular.copy(options.data), opts.data);
                        }

                        angular.extend(options, opts);

                        options.scope = angular.isObject(options.scope) ? options.scope.$new() : $rootScope.$new();
                        options.scope.confirm = function (value) {
                            defer.resolve(value);
                            var $dialog = $el(document.getElementById(openResult.id));
                            privateMethods.performCloseDialog($dialog, value);
                        };

                        var openResult = publicMethods.open(options);
                        if (openResult) {
                            openResult.closePromise.then(function (data) {
                                if (data) {
                                    return defer.reject(data.value);
                                }
                                return defer.reject();
                            });
                            return defer.promise;
                        }
                    },

                    isOpen: function(id) {
                        var $dialog = $el(document.getElementById(id));
                        return $dialog.length > 0;
                    },

                    /*
                     * @param {String} id
                     * @return {Object} dialog
                     */
                    close: function (id, value) {
                        var $dialog = $el(document.getElementById(id));

                        if ($dialog.length) {
                            privateMethods.closeDialog($dialog, value);
                        } else {
                            if (id === '$escape') {
                                var topDialogId = openIdStack[openIdStack.length - 1];
                                $dialog = $el(document.getElementById(topDialogId));
                                if ($dialog.data('$ngDialogOptions').closeByEscape) {
                                    privateMethods.closeDialog($dialog, '$escape');
                                }
                            } else {
                                publicMethods.closeAll(value);
                            }
                        }

                        return publicMethods;
                    },

                    closeAll: function (value) {
                        var $all = document.querySelectorAll('.ngdialog');

                        // Reverse order to ensure focus restoration works as expected
                        for (var i = $all.length - 1; i >= 0; i--) {
                            var dialog = $all[i];
                            privateMethods.closeDialog($el(dialog), value);
                        }
                    },

                    getOpenDialogs: function() {
                        return openIdStack;
                    },

                    getDefaults: function () {
                        return defaults;
                    }
                };

                angular.forEach(
                    ['html', 'body'],
                    function(elementName) {
                        $elements[elementName] = $document.find(elementName);
                        if (forceElementsReload[elementName]) {
                            var eventName = privateMethods.getRouterLocationEventName();
                            $rootScope.$on(eventName, function () {
                                $elements[elementName] = $document.find(elementName);
                            });
                        }
                    }
                );

                return publicMethods;
            }];
    });

    m.directive('ngDialog', ['ngDialog', function (ngDialog) {
        return {
            restrict: 'A',
            scope: {
                ngDialogScope: '='
            },
            link: function (scope, elem, attrs) {
                elem.on('click', function (e) {
                    e.preventDefault();

                    var ngDialogScope = angular.isDefined(scope.ngDialogScope) ? scope.ngDialogScope : 'noScope';
                    angular.isDefined(attrs.ngDialogClosePrevious) && ngDialog.close(attrs.ngDialogClosePrevious);

                    var defaults = ngDialog.getDefaults();

                    ngDialog.open({
                        template: attrs.ngDialog,
                        className: attrs.ngDialogClass || defaults.className,
                        appendClassName: attrs.ngDialogAppendClass,
                        controller: attrs.ngDialogController,
                        controllerAs: attrs.ngDialogControllerAs,
                        bindToController: attrs.ngDialogBindToController,
                        scope: ngDialogScope,
                        data: attrs.ngDialogData,
                        showClose: attrs.ngDialogShowClose === 'false' ? false : (attrs.ngDialogShowClose === 'true' ? true : defaults.showClose),
                        closeByDocument: attrs.ngDialogCloseByDocument === 'false' ? false : (attrs.ngDialogCloseByDocument === 'true' ? true : defaults.closeByDocument),
                        closeByEscape: attrs.ngDialogCloseByEscape === 'false' ? false : (attrs.ngDialogCloseByEscape === 'true' ? true : defaults.closeByEscape),
                        overlay: attrs.ngDialogOverlay === 'false' ? false : (attrs.ngDialogOverlay === 'true' ? true : defaults.overlay),
                        preCloseCallback: attrs.ngDialogPreCloseCallback || defaults.preCloseCallback,
                        bodyClassName: attrs.ngDialogBodyClass || defaults.bodyClassName
                    });
                });
            }
        };
    }]);

    return m;
}));

/**
 * angular-ui-notification - Angular.js service providing simple notifications using Bootstrap 3 styles with css transitions for animating
 * @author Alex_Crack
 * @version v0.2.0
 * @link https://github.com/alexcrack/angular-ui-notification
 * @license MIT
 */
angular.module("ui-notification",[]),angular.module("ui-notification").provider("Notification",function(){this.options={delay:5e3,startTop:10,startRight:10,verticalSpacing:10,horizontalSpacing:10,positionX:"right",positionY:"top",replaceMessage:!1,templateUrl:"angular-ui-notification.html",onClose:void 0,closeOnClick:!0,maxCount:0},this.setOptions=function(e){if(!angular.isObject(e))throw new Error("Options should be an object!");this.options=angular.extend({},this.options,e)},this.$get=["$timeout","$http","$compile","$templateCache","$rootScope","$injector","$sce","$q","$window",function(e,t,n,i,o,s,a,l,r){var c=this.options,p=c.startTop,d=c.startRight,u=c.verticalSpacing,m=c.horizontalSpacing,f=c.delay,g=[],h=!1,C=function(s,C){var y=l.defer();return"object"!=typeof s&&(s={message:s}),s.scope=s.scope?s.scope:o,s.template=s.templateUrl?s.templateUrl:c.templateUrl,s.delay=angular.isUndefined(s.delay)?f:s.delay,s.type=C||c.type||"",s.positionY=s.positionY?s.positionY:c.positionY,s.positionX=s.positionX?s.positionX:c.positionX,s.replaceMessage=s.replaceMessage?s.replaceMessage:c.replaceMessage,s.onClose=s.onClose?s.onClose:c.onClose,s.closeOnClick=null!==s.closeOnClick&&void 0!==s.closeOnClick?s.closeOnClick:c.closeOnClick,t.get(s.template,{cache:i}).success(function(t){function i(e){["-webkit-transition","-o-transition","transition"].forEach(function(t){f.css(t,e)})}var o=s.scope.$new();o.message=a.trustAsHtml(s.message),o.title=a.trustAsHtml(s.title),o.t=s.type.substr(0,1),o.delay=s.delay,o.onClose=s.onClose;var l=function(){for(var e=0,t=0,n=p,i=d,o=[],a=g.length-1;a>=0;a--){var l=g[a];if(s.replaceMessage&&a<g.length-1)l.addClass("killed");else{var r=parseInt(l[0].offsetHeight),f=parseInt(l[0].offsetWidth),h=o[l._positionY+l._positionX];C+r>window.innerHeight&&(h=p,t++,e=0);var C=n=h?0===e?h:h+u:p,y=i+t*(m+f);l.css(l._positionY,C+"px"),"center"==l._positionX?l.css("left",parseInt(window.innerWidth/2-f/2)+"px"):l.css(l._positionX,y+"px"),o[l._positionY+l._positionX]=C+r,c.maxCount>0&&g.length>c.maxCount&&0===a&&l.scope().kill(!0),e++}}},f=n(t)(o);f._positionY=s.positionY,f._positionX=s.positionX,f.addClass(s.type);var C=function(e){e=e.originalEvent||e,("click"===e.type||"opacity"===e.propertyName&&e.elapsedTime>=1)&&(o.onClose&&o.$apply(o.onClose(f)),f.remove(),g.splice(g.indexOf(f),1),o.$destroy(),l())};s.closeOnClick&&(f.addClass("clickable"),f.bind("click",C)),f.bind("webkitTransitionEnd oTransitionEnd otransitionend transitionend msTransitionEnd",C),angular.isNumber(s.delay)&&e(function(){f.addClass("killed")},s.delay),i("none"),angular.element(document.getElementsByTagName("body")).append(f);var v=-(parseInt(f[0].offsetHeight)+50);if(f.css(f._positionY,v+"px"),g.push(f),"center"==s.positionX){var k=parseInt(f[0].offsetWidth);f.css("left",parseInt(window.innerWidth/2-k/2)+"px")}e(function(){i("")}),o._templateElement=f,o.kill=function(t){t?(o.onClose&&o.$apply(o.onClose(o._templateElement)),g.splice(g.indexOf(o._templateElement),1),o._templateElement.remove(),o.$destroy(),e(l)):o._templateElement.addClass("killed")},e(l),h||(angular.element(r).bind("resize",function(){e(l)}),h=!0),y.resolve(o)}).error(function(e){throw new Error("Template ("+s.template+") could not be loaded. "+e)}),y.promise};return C.primary=function(e){return this(e,"primary")},C.error=function(e){return this(e,"error")},C.success=function(e){return this(e,"success")},C.info=function(e){return this(e,"info")},C.warning=function(e){return this(e,"warning")},C.clearAll=function(){angular.forEach(g,function(e){e.addClass("killed")})},C}]}),angular.module("ui-notification").run(["$templateCache",function(e){e.put("angular-ui-notification.html",'<div class="ui-notification"><h3 ng-show="title" ng-bind-html="title"></h3><div class="message" ng-bind-html="message"></div></div>')}]);
/*
 angular-file-upload v2.3.4
 https://github.com/nervgh/angular-file-upload
*/

!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports["angular-file-upload"]=t():e["angular-file-upload"]=t()}(this,function(){return function(e){function t(n){if(o[n])return o[n].exports;var r=o[n]={exports:{},id:n,loaded:!1};return e[n].call(r.exports,r,r.exports,t),r.loaded=!0,r.exports}var o={};return t.m=e,t.c=o,t.p="",t(0)}([function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}var r=o(1),i=n(r),s=o(2),a=n(s),l=o(3),u=n(l),p=o(4),c=n(p),f=o(5),d=n(f),h=o(6),y=n(h),m=o(7),_=n(m),v=o(8),g=n(v),F=o(9),b=n(F),O=o(10),C=n(O),I=o(11),U=n(I),w=o(12),A=n(w);angular.module(i["default"].name,[]).value("fileUploaderOptions",a["default"]).factory("FileUploader",u["default"]).factory("FileLikeObject",c["default"]).factory("FileItem",d["default"]).factory("FileDirective",y["default"]).factory("FileSelect",_["default"]).factory("FileDrop",g["default"]).factory("FileOver",b["default"]).directive("nvFileSelect",C["default"]).directive("nvFileDrop",U["default"]).directive("nvFileOver",A["default"]).run(["FileUploader","FileLikeObject","FileItem","FileDirective","FileSelect","FileDrop","FileOver",function(e,t,o,n,r,i,s){e.FileLikeObject=t,e.FileItem=o,e.FileDirective=n,e.FileSelect=r,e.FileDrop=i,e.FileOver=s}])},function(e,t){e.exports={name:"angularFileUpload"}},function(e,t){"use strict";Object.defineProperty(t,"__esModule",{value:!0}),t["default"]={url:"/",alias:"file",headers:{},queue:[],progress:0,autoUpload:!1,removeAfterUpload:!1,method:"POST",filters:[],formData:[],queueLimit:Number.MAX_VALUE,withCredentials:!1,disableMultipart:!1}},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function i(e,t,o,n,i,s,a){var m=n.File,_=n.FormData,v=function(){function n(t){r(this,n);var o=l(e);u(this,o,t,{isUploading:!1,_nextIndex:0,_failFilterIndex:-1,_directives:{select:[],drop:[],over:[]}}),this.filters.unshift({name:"queueLimit",fn:this._queueLimitFilter}),this.filters.unshift({name:"folder",fn:this._folderFilter})}return n.prototype.addToQueue=function(e,t,o){var n=this,r=this.isArrayLikeObject(e)?e:[e],i=this._getFilters(o),l=this.queue.length,u=[];p(r,function(e){var o=new s(e);if(n._isValidFile(o,i,t)){var r=new a(n,e,t);u.push(r),n.queue.push(r),n._onAfterAddingFile(r)}else{var l=i[n._failFilterIndex];n._onWhenAddingFileFailed(o,l,t)}}),this.queue.length!==l&&(this._onAfterAddingAll(u),this.progress=this._getTotalProgress()),this._render(),this.autoUpload&&this.uploadAll()},n.prototype.removeFromQueue=function(e){var t=this.getIndexOfItem(e),o=this.queue[t];o.isUploading&&o.cancel(),this.queue.splice(t,1),o._destroy(),this.progress=this._getTotalProgress()},n.prototype.clearQueue=function(){for(;this.queue.length;)this.queue[0].remove();this.progress=0},n.prototype.uploadItem=function(e){var t=this.getIndexOfItem(e),o=this.queue[t],n=this.isHTML5?"_xhrTransport":"_iframeTransport";o._prepareToUploading(),this.isUploading||(this._onBeforeUploadItem(o),o.isCancel||(o.isUploading=!0,this.isUploading=!0,this[n](o),this._render()))},n.prototype.cancelItem=function(e){var t=this,o=this.getIndexOfItem(e),n=this.queue[o],r=this.isHTML5?"_xhr":"_form";n&&(n.isCancel=!0,n.isUploading?n[r].abort():!function(){var e=[void 0,0,{}],o=function(){t._onCancelItem.apply(t,[n].concat(e)),t._onCompleteItem.apply(t,[n].concat(e))};i(o)}())},n.prototype.uploadAll=function(){var e=this.getNotUploadedItems().filter(function(e){return!e.isUploading});e.length&&(p(e,function(e){return e._prepareToUploading()}),e[0].upload())},n.prototype.cancelAll=function(){var e=this.getNotUploadedItems();p(e,function(e){return e.cancel()})},n.prototype.isFile=function(e){return this.constructor.isFile(e)},n.prototype.isFileLikeObject=function(e){return this.constructor.isFileLikeObject(e)},n.prototype.isArrayLikeObject=function(e){return this.constructor.isArrayLikeObject(e)},n.prototype.getIndexOfItem=function(e){return f(e)?e:this.queue.indexOf(e)},n.prototype.getNotUploadedItems=function(){return this.queue.filter(function(e){return!e.isUploaded})},n.prototype.getReadyItems=function(){return this.queue.filter(function(e){return e.isReady&&!e.isUploading}).sort(function(e,t){return e.index-t.index})},n.prototype.destroy=function(){var e=this;p(this._directives,function(t){p(e._directives[t],function(e){e.destroy()})})},n.prototype.onAfterAddingAll=function(e){},n.prototype.onAfterAddingFile=function(e){},n.prototype.onWhenAddingFileFailed=function(e,t,o){},n.prototype.onBeforeUploadItem=function(e){},n.prototype.onProgressItem=function(e,t){},n.prototype.onProgressAll=function(e){},n.prototype.onSuccessItem=function(e,t,o,n){},n.prototype.onErrorItem=function(e,t,o,n){},n.prototype.onCancelItem=function(e,t,o,n){},n.prototype.onCompleteItem=function(e,t,o,n){},n.prototype.onCompleteAll=function(){},n.prototype._getTotalProgress=function(e){if(this.removeAfterUpload)return e||0;var t=this.getNotUploadedItems().length,o=t?this.queue.length-t:this.queue.length,n=100/this.queue.length,r=(e||0)*n/100;return Math.round(o*n+r)},n.prototype._getFilters=function(e){if(!e)return this.filters;if(h(e))return e;var t=e.match(/[^\s,]+/g);return this.filters.filter(function(e){return-1!==t.indexOf(e.name)})},n.prototype._render=function(){t.$$phase||t.$apply()},n.prototype._folderFilter=function(e){return!(!e.size&&!e.type)},n.prototype._queueLimitFilter=function(){return this.queue.length<this.queueLimit},n.prototype._isValidFile=function(e,t,o){var n=this;return this._failFilterIndex=-1,t.length?t.every(function(t){return n._failFilterIndex++,t.fn.call(n,e,o)}):!0},n.prototype._isSuccessCode=function(e){return e>=200&&300>e||304===e},n.prototype._transformResponse=function(e,t){var n=this._headersGetter(t);return p(o.defaults.transformResponse,function(t){e=t(e,n)}),e},n.prototype._parseHeaders=function(e){var t,o,n,r={};return e?(p(e.split("\n"),function(e){n=e.indexOf(":"),t=e.slice(0,n).trim().toLowerCase(),o=e.slice(n+1).trim(),t&&(r[t]=r[t]?r[t]+", "+o:o)}),r):r},n.prototype._headersGetter=function(e){return function(t){return t?e[t.toLowerCase()]||null:e}},n.prototype._xhrTransport=function(e){var t,o=this,n=e._xhr=new XMLHttpRequest;if(e.disableMultipart?t=e._file:(t=new _,p(e.formData,function(e){p(e,function(e,o){t.append(o,e)})}),t.append(e.alias,e._file,e.file.name)),"number"!=typeof e._file.size)throw new TypeError("The file specified is no longer valid");n.upload.onprogress=function(t){var n=Math.round(t.lengthComputable?100*t.loaded/t.total:0);o._onProgressItem(e,n)},n.onload=function(){var t=o._parseHeaders(n.getAllResponseHeaders()),r=o._transformResponse(n.response,t),i=o._isSuccessCode(n.status)?"Success":"Error",s="_on"+i+"Item";o[s](e,r,n.status,t),o._onCompleteItem(e,r,n.status,t)},n.onerror=function(){var t=o._parseHeaders(n.getAllResponseHeaders()),r=o._transformResponse(n.response,t);o._onErrorItem(e,r,n.status,t),o._onCompleteItem(e,r,n.status,t)},n.onabort=function(){var t=o._parseHeaders(n.getAllResponseHeaders()),r=o._transformResponse(n.response,t);o._onCancelItem(e,r,n.status,t),o._onCompleteItem(e,r,n.status,t)},n.open(e.method,e.url,!0),n.withCredentials=e.withCredentials,p(e.headers,function(e,t){n.setRequestHeader(t,e)}),n.send(t)},n.prototype._iframeTransport=function(e){var t=this,o=y('<form style="display: none;" />'),n=y('<iframe name="iframeTransport'+Date.now()+'">'),r=e._input;e._form&&e._form.replaceWith(r),e._form=o,r.prop("name",e.alias),p(e.formData,function(e){p(e,function(e,t){var n=y('<input type="hidden" name="'+t+'" />');n.val(e),o.append(n)})}),o.prop({action:e.url,method:"POST",target:n.prop("name"),enctype:"multipart/form-data",encoding:"multipart/form-data"}),n.bind("load",function(){var o="",r=200;try{o=n[0].contentDocument.body.innerHTML}catch(i){r=500}var s={response:o,status:r,dummy:!0},a={},l=t._transformResponse(s.response,a);t._onSuccessItem(e,l,s.status,a),t._onCompleteItem(e,l,s.status,a)}),o.abort=function(){var i,s={status:0,dummy:!0},a={};n.unbind("load").prop("src","javascript:false;"),o.replaceWith(r),t._onCancelItem(e,i,s.status,a),t._onCompleteItem(e,i,s.status,a)},r.after(o),o.append(r).append(n),o[0].submit()},n.prototype._onWhenAddingFileFailed=function(e,t,o){this.onWhenAddingFileFailed(e,t,o)},n.prototype._onAfterAddingFile=function(e){this.onAfterAddingFile(e)},n.prototype._onAfterAddingAll=function(e){this.onAfterAddingAll(e)},n.prototype._onBeforeUploadItem=function(e){e._onBeforeUpload(),this.onBeforeUploadItem(e)},n.prototype._onProgressItem=function(e,t){var o=this._getTotalProgress(t);this.progress=o,e._onProgress(t),this.onProgressItem(e,t),this.onProgressAll(o),this._render()},n.prototype._onSuccessItem=function(e,t,o,n){e._onSuccess(t,o,n),this.onSuccessItem(e,t,o,n)},n.prototype._onErrorItem=function(e,t,o,n){e._onError(t,o,n),this.onErrorItem(e,t,o,n)},n.prototype._onCancelItem=function(e,t,o,n){e._onCancel(t,o,n),this.onCancelItem(e,t,o,n)},n.prototype._onCompleteItem=function(e,t,o,n){e._onComplete(t,o,n),this.onCompleteItem(e,t,o,n);var r=this.getReadyItems()[0];return this.isUploading=!1,d(r)?void r.upload():(this.onCompleteAll(),this.progress=this._getTotalProgress(),void this._render())},n.isFile=function(e){return m&&e instanceof m},n.isFileLikeObject=function(e){return e instanceof s},n.isArrayLikeObject=function(e){return c(e)&&"length"in e},n.inherit=function(e,t){e.prototype=Object.create(t.prototype),e.prototype.constructor=e,e.super_=t},n}();return v.prototype.isHTML5=!(!m||!_),v.isHTML5=v.prototype.isHTML5,v}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=i;var s=o(1),a=(n(s),angular),l=a.copy,u=a.extend,p=a.forEach,c=a.isObject,f=a.isNumber,d=a.isDefined,h=a.isArray,y=a.element;i.$inject=["fileUploaderOptions","$rootScope","$http","$window","$timeout","FileLikeObject","FileItem"]},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function i(){return function(){function e(t){r(this,e);var o=u(t),n=o?t.value:t,i=p(n)?"FakePath":"Object",s="_createFrom"+i;this[s](n)}return e.prototype._createFromFakePath=function(e){this.lastModifiedDate=null,this.size=null,this.type="like/"+e.slice(e.lastIndexOf(".")+1).toLowerCase(),this.name=e.slice(e.lastIndexOf("/")+e.lastIndexOf("\\")+2)},e.prototype._createFromObject=function(e){this.lastModifiedDate=l(e.lastModifiedDate),this.size=e.size,this.type=e.type,this.name=e.name},e}()}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=i;var s=o(1),a=(n(s),angular),l=a.copy,u=a.isElement,p=a.isString},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function i(e,t){return function(){function o(e,n,i){r(this,o);var s=c(n),a=s?p(n):null,f=s?null:n;u(this,{url:e.url,alias:e.alias,headers:l(e.headers),formData:l(e.formData),removeAfterUpload:e.removeAfterUpload,withCredentials:e.withCredentials,disableMultipart:e.disableMultipart,method:e.method},i,{uploader:e,file:new t(n),isReady:!1,isUploading:!1,isUploaded:!1,isSuccess:!1,isCancel:!1,isError:!1,progress:0,index:null,_file:f,_input:a}),a&&this._replaceNode(a)}return o.prototype.upload=function(){try{this.uploader.uploadItem(this)}catch(e){this.uploader._onCompleteItem(this,"",0,[]),this.uploader._onErrorItem(this,"",0,[])}},o.prototype.cancel=function(){this.uploader.cancelItem(this)},o.prototype.remove=function(){this.uploader.removeFromQueue(this)},o.prototype.onBeforeUpload=function(){},o.prototype.onProgress=function(e){},o.prototype.onSuccess=function(e,t,o){},o.prototype.onError=function(e,t,o){},o.prototype.onCancel=function(e,t,o){},o.prototype.onComplete=function(e,t,o){},o.prototype._onBeforeUpload=function(){this.isReady=!0,this.isUploading=!1,this.isUploaded=!1,this.isSuccess=!1,this.isCancel=!1,this.isError=!1,this.progress=0,this.onBeforeUpload()},o.prototype._onProgress=function(e){this.progress=e,this.onProgress(e)},o.prototype._onSuccess=function(e,t,o){this.isReady=!1,this.isUploading=!1,this.isUploaded=!0,this.isSuccess=!0,this.isCancel=!1,this.isError=!1,this.progress=100,this.index=null,this.onSuccess(e,t,o)},o.prototype._onError=function(e,t,o){this.isReady=!1,this.isUploading=!1,this.isUploaded=!0,this.isSuccess=!1,this.isCancel=!1,this.isError=!0,this.progress=0,this.index=null,this.onError(e,t,o)},o.prototype._onCancel=function(e,t,o){this.isReady=!1,this.isUploading=!1,this.isUploaded=!1,this.isSuccess=!1,this.isCancel=!0,this.isError=!1,this.progress=0,this.index=null,this.onCancel(e,t,o)},o.prototype._onComplete=function(e,t,o){this.onComplete(e,t,o),this.removeAfterUpload&&this.remove()},o.prototype._destroy=function(){this._input&&this._input.remove(),this._form&&this._form.remove(),delete this._form,delete this._input},o.prototype._prepareToUploading=function(){this.index=this.index||++this.uploader._nextIndex,this.isReady=!0},o.prototype._replaceNode=function(t){var o=e(t.clone())(t.scope());o.prop("value",null),t.css("display","none"),t.after(o)},o}()}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=i;var s=o(1),a=(n(s),angular),l=a.copy,u=a.extend,p=a.element,c=a.isElement;i.$inject=["$compile","FileLikeObject"]},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function i(){var e=function(){function e(t){r(this,e),l(this,t),this.uploader._directives[this.prop].push(this),this._saveLinks(),this.bind()}return e.prototype.bind=function(){for(var e in this.events){var t=this.events[e];this.element.bind(e,this[t])}},e.prototype.unbind=function(){for(var e in this.events)this.element.unbind(e,this.events[e])},e.prototype.destroy=function(){var e=this.uploader._directives[this.prop].indexOf(this);this.uploader._directives[this.prop].splice(e,1),this.unbind()},e.prototype._saveLinks=function(){for(var e in this.events){var t=this.events[e];this[t]=this[t].bind(this)}},e}();return e.prototype.events={},e}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=i;var s=o(1),a=(n(s),angular),l=a.extend},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function i(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function s(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}function a(e,t){return function(t){function o(e){r(this,o);var n=p(e,{events:{$destroy:"destroy",change:"onChange"},prop:"select"}),s=i(this,t.call(this,n));return s.uploader.isHTML5||s.element.removeAttr("multiple"),s.element.prop("value",null),s}return s(o,t),o.prototype.getOptions=function(){},o.prototype.getFilters=function(){},o.prototype.isEmptyAfterSelection=function(){return!!this.element.attr("multiple")},o.prototype.onChange=function(){var t=this.uploader.isHTML5?this.element[0].files:this.element[0],o=this.getOptions(),n=this.getFilters();this.uploader.isHTML5||this.destroy(),this.uploader.addToQueue(t,o,n),this.isEmptyAfterSelection()&&(this.element.prop("value",null),this.element.replaceWith(e(this.element.clone())(this.scope)))},o}(t)}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=a;var l=o(1),u=(n(l),angular),p=u.extend;a.$inject=["$compile","FileDirective"]},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function i(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function s(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}function a(e){return function(e){function t(o){r(this,t);var n=p(o,{events:{$destroy:"destroy",drop:"onDrop",dragover:"onDragOver",dragleave:"onDragLeave"},prop:"drop"});return i(this,e.call(this,n))}return s(t,e),t.prototype.getOptions=function(){},t.prototype.getFilters=function(){},t.prototype.onDrop=function(e){var t=this._getTransfer(e);if(t){var o=this.getOptions(),n=this.getFilters();this._preventAndStop(e),c(this.uploader._directives.over,this._removeOverClass,this),this.uploader.addToQueue(t.files,o,n)}},t.prototype.onDragOver=function(e){var t=this._getTransfer(e);this._haveFiles(t.types)&&(t.dropEffect="copy",this._preventAndStop(e),c(this.uploader._directives.over,this._addOverClass,this))},t.prototype.onDragLeave=function(e){e.currentTarget!==this.element[0]&&(this._preventAndStop(e),c(this.uploader._directives.over,this._removeOverClass,this))},t.prototype._getTransfer=function(e){return e.dataTransfer?e.dataTransfer:e.originalEvent.dataTransfer},t.prototype._preventAndStop=function(e){e.preventDefault(),e.stopPropagation()},t.prototype._haveFiles=function(e){return e?e.indexOf?-1!==e.indexOf("Files"):e.contains?e.contains("Files"):!1:!1},t.prototype._addOverClass=function(e){e.addOverClass()},t.prototype._removeOverClass=function(e){e.removeOverClass()},t}(e)}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=a;var l=o(1),u=(n(l),angular),p=u.extend,c=u.forEach;a.$inject=["FileDirective"]},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function i(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function s(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}function a(e){return function(e){function t(o){r(this,t);var n=p(o,{events:{$destroy:"destroy"},prop:"over",overClass:"nv-file-over"});return i(this,e.call(this,n))}return s(t,e),t.prototype.addOverClass=function(){this.element.addClass(this.getOverClass())},t.prototype.removeOverClass=function(){this.element.removeClass(this.getOverClass())},t.prototype.getOverClass=function(){return this.overClass},t}(e)}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=a;var l=o(1),u=(n(l),angular),p=u.extend;a.$inject=["FileDirective"]},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t,o){return{link:function(n,r,i){var s=n.$eval(i.uploader);if(!(s instanceof t))throw new TypeError('"Uploader" must be an instance of FileUploader');var a=new o({uploader:s,element:r,scope:n});a.getOptions=e(i.options).bind(a,n),a.getFilters=function(){return i.filters}}}}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=r;var i=o(1);n(i);r.$inject=["$parse","FileUploader","FileSelect"]},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t,o){return{link:function(n,r,i){var s=n.$eval(i.uploader);if(!(s instanceof t))throw new TypeError('"Uploader" must be an instance of FileUploader');if(s.isHTML5){var a=new o({uploader:s,element:r});a.getOptions=e(i.options).bind(a,n),a.getFilters=function(){return i.filters}}}}}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=r;var i=o(1);n(i);r.$inject=["$parse","FileUploader","FileDrop"]},function(e,t,o){"use strict";function n(e){return e&&e.__esModule?e:{"default":e}}function r(e,t){return{link:function(o,n,r){var i=o.$eval(r.uploader);if(!(i instanceof e))throw new TypeError('"Uploader" must be an instance of FileUploader');var s=new t({uploader:i,element:n});s.getOverClass=function(){return r.overClass||s.overClass}}}}Object.defineProperty(t,"__esModule",{value:!0}),t["default"]=r;var i=o(1);n(i);r.$inject=["FileUploader","FileOver"]}])});
//# sourceMappingURL=angular-file-upload.min.js.map
(function() {

	var app = angular.module('productPage', ['ui-notification', 'angularFileUpload']);

    app.controller('ProductPageController', ['ngDialog', 'Notification', 'productsList', 'FileUploader', '$rootScope', function(ngDialog, Notification, productsList, FileUploader, $rootScope) {

		$rootScope.$on('ngDialog.opened', function (e, $dialog) {
    		$('select').material_select();
			Materialize.updateTextFields();
		});

		this.uploader = new FileUploader({ url: '/connects/upload.php' });

        this.uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };

		this.remove = function(event, product, videoIndex) {
			productsList[product.index].video.splice(videoIndex, 1)
			// delete productsList[product.index].video[videoIndex];
			// event.srcElement.parentNode.style.display = 'none';
			// productsList[product.index].video.length = productsList[product.index].video.length - 1;
			// console.log(event, product, videoIndex);
			console.log(productsList);
		}

		this.deleteVideo = function(index, productIndex) {
			console.log(index, productIndex);
		}

		this.saveProduct = function(product, que) {

			if (que.length == 0) {

				console.log('Image not upload');
				productsList[product.index].image = 'video';

			}

			this.uploader.uploadAll();

			for (var i = 0; i < que.length; i++) {
				console.log(que);
				productsList[product.index].image = que[0].file.name;
			};


			// console.log(product.index, que[0].file.name);
		    Notification.success('Товар "' + product.product.name +'" изменен');
		};

    }]);

})();

(function() {

	var app = angular.module('productList', ['ngDialog', 'ui-notification']);

    app.controller('ProductController', ['$http', 'ngDialog', 'Notification', 'productsList', function($http, ngDialog, Notification, productsList) {

		var store = this;
		store.products = productsList;

		// $http.post('/connects/connects.php', {
		// 	type: 'read',
		// 	item: 'product'
		// 	}).success(function(response) {
		// 	store.products = response;
		// 	console.log(response);
		// });

		// var handleSuccess = function(data, status) {
		// 	store.products = data;
  //  		};
		//
		// productsList.getSessions().success(handleSuccess);

        this.removeProduct = function(index) {
            document.getElementsByClassName('productList__item')[index].style.display='none';
            delete productsList[index];
			console.log(productsList);
        }

		this.openEditPage = function(index) {

			console.log(index);

			ngDialog.open({
				template: 'productEditPage',
				className: 'ngdialog-theme-plain',
                data: {
					product: store.products[index],
					index: index
				}
			});

		}

		this.addProduct = function() {
			store.products[store.products.length] = {
				name: "Новый товар",
		        description: "Описание товара.",
		        price: "100",
		        image: "img/server.svg",
		        video: ""
			}

			console.log(store.products);
		}

		this.save = function() {

			// product.product.video = product.product.video.join(',');

			var changedList = {};

			for (var i = 1; i < store.products.length + 1; i++) {
				changedList.type = 'write';
				changedList.item = 'product';
				changedList['product' + i] = store.products[i - 1];

				if (changedList['product' + i] === true) {

					changedList['product' + i].video = changedList['product' + i].video.join(',');
					console.log(changedList['product' + i]);

				}

				delete changedList.id;
			}

			console.log(changedList);

			$http.post('/connects/connects.php', changedList).success(function(response) {

				Notification.success(response);

				// location.reload();

			 });

		}

    }]);

})();

(function() {

	var app = angular.module('storeInfo', ['ui-notification', 'angularFileUpload']);

    app.controller('StoreInfoController', ['ngDialog', 'Notification', '$http', 'FileUploader', function(ngDialog, Notification, $http, FileUploader) {

		this.uploaderOne = new FileUploader({ url: '/connects/upload.php' });
		this.uploaderTwo = new FileUploader({ url: '/connects/upload.php' });

		this.uploaderOne.onSuccessItem = function(fileItem, response, status, headers) {
			console.info('onSuccessItem', fileItem, response, status, headers);
		};

		this.uploaderTwo.onSuccessItem = function(fileItem, response, status, headers) {
			console.info('onSuccessItem', fileItem, response, status, headers);
		};

        var info = function() {

			var xhr = new XMLHttpRequest();

			var body = 'type=read&item=configs';

			xhr.open("POST", '/connects/connects.php', false)
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

			xhr.send(body);

			var info = JSON.parse(xhr.responseText);

			return info;

        }

        this.info = info();

		this.save = function(info, img1, img2) {

			console.log(img1.length, img2.length);

			if (img1.length != 0 && img2.length != 0) {

				$http.post('/connects/connects.php', {
					type: 'write',
					item: 'configs',
					configs1: {
						name: 'text1',
						content: info[0].content
					},
					configs2: {
						name: 'text2',
						content: info[1].content
					},
					configs3: {
						name: 'text3',
						content: info[2].content
					},
					configs4: {
						name: 'text4',
						content: info[3].content
					},
					configs5: {
						name: 'text5',
						content: info[4].content
					},
					configs6: {
						name: 'text6',
						content: info[5].content
					},
					configs7: {
						name: 'text7',
						content: info[6].content
					},
					configs8: {
						name: 'text8',
						content: info[7].content
					},
					configs9: {
						name: 'imagePath1',
						content: img1[0].file.name
					},
					configs10: {
						name: 'imagePath2',
						content: img2[0].file.name
					},
					configs11: {
						name: 'heroVideo',
						content: info[10].content || undefined
					}
				}).success(function (e) {
					Notification.success('Сохранено');
				});

			} else {
				Notification.error('Не все поля заполнены');
			}


		}

    }]);

})();

(function() {

	var app = angular.module('metaInfo', ['ui-notification']);

    app.controller('MetaInfoController', ['ngDialog', 'Notification', '$http', function(ngDialog, Notification, $http) {

		var metaInfo = function() {

			var xhr = new XMLHttpRequest();

			var body = 'type=read&item=meta';

			xhr.open("POST", '/connects/connects.php', false)
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

			xhr.send(body);

			var meta = JSON.parse(xhr.responseText);

			return meta;

		};

		this.metaInfo = metaInfo();

		this.save = function(info) {

			console.log(info);

			$http.post('/connects/connects.php', {

                type: 'write',
                item: 'meta',
				title: document.getElementById('title').value,
				description: document.getElementById('metaDescription').value,
				keywords: document.getElementById('metaTags').value

				}).success(function(response) {

				Notification.success('Сохранено');

            });

		};

    }]);

})();

(function() {

	var app = angular.module('adminLogin', []);

    app.controller('AdminLoginController', ['$http', 'Notification', function($http, Notification) {

        this.submit = function() {


            $http.post('/connects/connects.php', {
                type: 'admlogin',
                login: document.getElementById('adminLogin').value,
                pass: document.getElementById('adminPass').value
                }).success(function(response) {

                if (response.replace(/\n$/m, '') == 'ok') {
                    location.reload();
                } else {
					console.log(response, 'wrong');
					console.log(response == 'ok&#010', response == 'ok');
					Notification.error({message: response, delay: 1000});
                    location.reload();
				}
            });

        }


    }]);


})();

(function() {

	var app = angular.module('productCover', []);

    app.controller('productCoverController', ['$sce', 'getYotubeVideoID', function($sce, getYotubeVideoID) {

        this.isVideo = false;
        this.isImage = true;

        this.changeType = function () {

            switch (this.productCoverType) {
                case 'image':
                    this.isImage = true;
                    this.isVideo = false;
                    break;
                case 'video':
                    this.isVideo = true;
                    this.isImage = false;
                    break;

            }

        };

        this.isVideoCover = function (product) {

            if (product == 'video') {
                return true;
            } else {
                return false;
            }
        };

        this.setVideo = function(url) {

			return getYotubeVideoID.ID(url);

        };

		this.test = 'work';


    }]);

})();

(function() {

    var app = angular.module('mailHistory', []);

    app.controller('MailHistoryController', ['$http', function($http) {

        var mails = function() {

            var xhr = new XMLHttpRequest();

            var body = 'type=read&item=mail';

            xhr.open("POST", '/connects/connects.php', false)
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

            xhr.send(body);

            var info = JSON.parse(xhr.responseText);

            return info;

        }

        this.mails = mails();

        $http.post('/connects/connects.php', {
            type: 'read',
            item: 'mail'
        }).success(function (e) {

        });

    }]);


})();


$(document).ready(function() {
    Materialize.updateTextFields();
 });

(function() {

	var app = angular.module('store', ['navigation', 'productPage', 'productList', 'storeInfo', 'metaInfo', 'adminLogin', 'productCover', 'mailHistory'], function($httpProvider) {

		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

		$httpProvider.defaults.transformRequest = [function(data) {

		   var param = function(obj) {
			 var query = '';
			 var name, value, fullSubName, subValue, innerObj, i;

			 for(name in obj)
			 {
			   value = obj[name];

			   if(value instanceof Array)
			   {
				 for(i=0; i<value.length; ++i)
				 {
				   subValue = value[i];
				   fullSubName = name + '[' + i + ']';
				   innerObj = {};
				   innerObj[fullSubName] = subValue;
				   query += param(innerObj) + '&';
				 }
			   }
			   else if(value instanceof Object)
			   {
				 for(subName in value)
				 {
				   subValue = value[subName];
				   fullSubName = name + '[' + subName + ']';
				   innerObj = {};
				   innerObj[fullSubName] = subValue;
				   query += param(innerObj) + '&';
				 }
			   }
			   else if(value !== undefined && value !== null)
			   {
				 query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
			   }
			 }

			 return query.length ? query.substr(0, query.length - 1) : query;
		   };

		   return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
		 }];

	});

	app.service('productsList', ['$http', function($http) {

		var products = [];

		var xhr = new XMLHttpRequest();

		var body = 'type=read&item=product';

		xhr.open("POST", '/connects/connects.php', false)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

		xhr.send(body);

		products = JSON.parse(xhr.responseText);

		for (var i = 0; i < products.length; i++) {
			products[i].video = products[i].video.split(',');
		}

		return products;

    }]);

    app.service('getYotubeVideoID', ['$sce', function($sce) {

		this.ID = function(url) {

            if (typeof url === 'string' && url != '') {

				var videoID = url.match(/=([\w-]+)/)[1];
	            var videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoID);

                console.log(videoUrl);

                return videoUrl;

			} else {
                // console.error('bad url ' + url);
            }

		}

    }]);

	app.directive('backImg', function(){
        return function(scope, element, attrs){
            var url = attrs.backImg;
			var size = attrs.backSize;

            element.css({
                'background': 'url(' + url +') no-repeat center',
                'background-size' : size || 'cover'
            });
        };
    });

	app.directive('elastic', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
		}
	]);

	app.directive('ngThumb', ['$window', function($window) {
	        var helper = {
	            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
	            isFile: function(item) {
	                return angular.isObject(item) && item instanceof $window.File;
	            },
	            isImage: function(file) {
	                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
	                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	            }
	        };

	        return {
	            restrict: 'A',
	            template: '<canvas/>',
	            link: function(scope, element, attributes) {
	                if (!helper.support) return;

	                var params = scope.$eval(attributes.ngThumb);

	                if (!helper.isFile(params.file)) return;
	                if (!helper.isImage(params.file)) return;

	                var canvas = element.find('canvas');
	                var reader = new FileReader();

	                reader.onload = onLoadFile;
	                reader.readAsDataURL(params.file);

	                function onLoadFile(event) {
	                    var img = new Image();
	                    img.onload = onLoadImage;
	                    img.src = event.target.result;
	                }

	                function onLoadImage() {
	                    var width = params.width || this.width / this.height * params.height;
	                    var height = params.height || this.height / this.width * params.width;
	                    canvas.attr({ width: width, height: height });
	                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
	                }
	            }
	        };
	    }]);

})();

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hdmlnYXRpb24vbmF2aWdhdGlvbi5qcyIsIi4uL2xpYnMvbmdEaWFsb2cuanMiLCIuLi9saWJzL2FuZ3VsYXItdWktbm90aWZpY2F0aW9uLm1pbi5qcyIsIi4uL2xpYnMvYW5ndWxhci1maWxlLXVwbG9hZC5taW4uanMiLCJwcm9kdWN0UGFnZS9wcm9kdWN0UGFnZS5qcyIsInByb2R1Y3RMaXN0L3Byb2R1Y3RMaXN0LmpzIiwic3RvcmVJbmZvL3N0b3JlSW5mby5qcyIsIm1ldGFJbmZvL21ldGFJbmZvLmpzIiwiYWRtaW5Mb2dpbi9hZG1pbkxvZ2luLmpzIiwicHJvZHVjdENvdmVyL3Byb2R1Y3RDb3Zlci5qcyIsIm1haWxIaXN0b3J5L21haWxIaXN0b3J5LmpzIiwiaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ozQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN0REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDNUZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN2R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNoREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM5Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJpbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpIHtcblxuXHR2YXIgYXBwID0gYW5ndWxhci5tb2R1bGUoJ25hdmlnYXRpb24nLCBbXSk7XG5cbiAgICBhcHAuY29udHJvbGxlcignTmF2aWdhdGlvbkNvbnRyb2xsZXInLCBmdW5jdGlvbigpIHtcblxuICAgICAgICB0aGlzLnNob3dPbldvcmtGaWVsZCA9IGZ1bmN0aW9uKGVsZW1lbnQsIGV2ZW50KSB7XG5cblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ25hdicpLmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ25hdicpLmNoaWxkcmVuW2ldLmNsYXNzTGlzdC5yZW1vdmUoJ25hdl9faXRlbS1hY3RpdmUnKTtcbiAgICAgICAgICAgIH1cblxuXHRcdFx0ZXZlbnQudGFyZ2V0LmNsYXNzTGlzdC5hZGQoJ25hdl9faXRlbS1hY3RpdmUnKTtcblxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnd29ya0ZpZWxkJykuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnd29ya0ZpZWxkJykuY2hpbGRyZW5baV0uc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgICAgICAgICAgIH1cblxuICAgIFx0ICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGVsZW1lbnQpLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xuICAgIFx0fVxuXG4gICAgfSk7XG5cbn0pKCk7XG4iLCIvKlxuICogbmdEaWFsb2cgLSBlYXN5IG1vZGFscyBhbmQgcG9wdXAgd2luZG93c1xuICogaHR0cDovL2dpdGh1Yi5jb20vbGlrZWFzdG9yZS9uZ0RpYWxvZ1xuICogKGMpIDIwMTMtMjAxNSBNSVQgTGljZW5zZSwgaHR0cHM6Ly9saWtlYXN0b3JlLmNvbVxuICovXG5cbihmdW5jdGlvbiAocm9vdCwgZmFjdG9yeSkge1xuICAgIGlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyAmJiBtb2R1bGUuZXhwb3J0cykge1xuICAgICAgICAvLyBDb21tb25KU1xuICAgICAgICBpZiAodHlwZW9mIGFuZ3VsYXIgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICBmYWN0b3J5KHJlcXVpcmUoJ2FuZ3VsYXInKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBmYWN0b3J5KGFuZ3VsYXIpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5leHBvcnRzID0gJ25nRGlhbG9nJztcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xuICAgICAgICAvLyBBTURcbiAgICAgICAgZGVmaW5lKFsnYW5ndWxhciddLCBmYWN0b3J5KTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAvLyBHbG9iYWwgVmFyaWFibGVzXG4gICAgICAgIGZhY3Rvcnkocm9vdC5hbmd1bGFyKTtcbiAgICB9XG59KHRoaXMsIGZ1bmN0aW9uIChhbmd1bGFyKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyIG0gPSBhbmd1bGFyLm1vZHVsZSgnbmdEaWFsb2cnLCBbXSk7XG5cbiAgICB2YXIgJGVsID0gYW5ndWxhci5lbGVtZW50O1xuICAgIHZhciBpc0RlZiA9IGFuZ3VsYXIuaXNEZWZpbmVkO1xuICAgIHZhciBzdHlsZSA9IChkb2N1bWVudC5ib2R5IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCkuc3R5bGU7XG4gICAgdmFyIGFuaW1hdGlvbkVuZFN1cHBvcnQgPSBpc0RlZihzdHlsZS5hbmltYXRpb24pIHx8IGlzRGVmKHN0eWxlLldlYmtpdEFuaW1hdGlvbikgfHwgaXNEZWYoc3R5bGUuTW96QW5pbWF0aW9uKSB8fCBpc0RlZihzdHlsZS5Nc0FuaW1hdGlvbikgfHwgaXNEZWYoc3R5bGUuT0FuaW1hdGlvbik7XG4gICAgdmFyIGFuaW1hdGlvbkVuZEV2ZW50ID0gJ2FuaW1hdGlvbmVuZCB3ZWJraXRBbmltYXRpb25FbmQgbW96QW5pbWF0aW9uRW5kIE1TQW5pbWF0aW9uRW5kIG9hbmltYXRpb25lbmQnO1xuICAgIHZhciBmb2N1c2FibGVFbGVtZW50U2VsZWN0b3IgPSAnYVtocmVmXSwgYXJlYVtocmVmXSwgaW5wdXQ6bm90KFtkaXNhYmxlZF0pLCBzZWxlY3Q6bm90KFtkaXNhYmxlZF0pLCB0ZXh0YXJlYTpub3QoW2Rpc2FibGVkXSksIGJ1dHRvbjpub3QoW2Rpc2FibGVkXSksIGlmcmFtZSwgb2JqZWN0LCBlbWJlZCwgKlt0YWJpbmRleF0sICpbY29udGVudGVkaXRhYmxlXSc7XG4gICAgdmFyIGRpc2FibGVkQW5pbWF0aW9uQ2xhc3MgPSAnbmdkaWFsb2ctZGlzYWJsZWQtYW5pbWF0aW9uJztcbiAgICB2YXIgZm9yY2VFbGVtZW50c1JlbG9hZCA9IHsgaHRtbDogZmFsc2UsIGJvZHk6IGZhbHNlIH07XG4gICAgdmFyIHNjb3BlcyA9IHt9O1xuICAgIHZhciBvcGVuSWRTdGFjayA9IFtdO1xuICAgIHZhciBrZXlkb3duSXNCb3VuZCA9IGZhbHNlO1xuICAgIHZhciBvcGVuT25lUGVyTmFtZSA9IGZhbHNlO1xuXG5cbiAgICBtLnByb3ZpZGVyKCduZ0RpYWxvZycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGRlZmF1bHRzID0gdGhpcy5kZWZhdWx0cyA9IHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogJ25nZGlhbG9nLXRoZW1lLWRlZmF1bHQnLFxuICAgICAgICAgICAgYXBwZW5kQ2xhc3NOYW1lOiAnJyxcbiAgICAgICAgICAgIGRpc2FibGVBbmltYXRpb246IGZhbHNlLFxuICAgICAgICAgICAgcGxhaW46IGZhbHNlLFxuICAgICAgICAgICAgc2hvd0Nsb3NlOiB0cnVlLFxuICAgICAgICAgICAgY2xvc2VCeURvY3VtZW50OiB0cnVlLFxuICAgICAgICAgICAgY2xvc2VCeUVzY2FwZTogdHJ1ZSxcbiAgICAgICAgICAgIGNsb3NlQnlOYXZpZ2F0aW9uOiBmYWxzZSxcbiAgICAgICAgICAgIGFwcGVuZFRvOiBmYWxzZSxcbiAgICAgICAgICAgIHByZUNsb3NlQ2FsbGJhY2s6IGZhbHNlLFxuICAgICAgICAgICAgb3ZlcmxheTogdHJ1ZSxcbiAgICAgICAgICAgIGNhY2hlOiB0cnVlLFxuICAgICAgICAgICAgdHJhcEZvY3VzOiB0cnVlLFxuICAgICAgICAgICAgcHJlc2VydmVGb2N1czogdHJ1ZSxcbiAgICAgICAgICAgIGFyaWFBdXRvOiB0cnVlLFxuICAgICAgICAgICAgYXJpYVJvbGU6IG51bGwsXG4gICAgICAgICAgICBhcmlhTGFiZWxsZWRCeUlkOiBudWxsLFxuICAgICAgICAgICAgYXJpYUxhYmVsbGVkQnlTZWxlY3RvcjogbnVsbCxcbiAgICAgICAgICAgIGFyaWFEZXNjcmliZWRCeUlkOiBudWxsLFxuICAgICAgICAgICAgYXJpYURlc2NyaWJlZEJ5U2VsZWN0b3I6IG51bGwsXG4gICAgICAgICAgICBib2R5Q2xhc3NOYW1lOiAnbmdkaWFsb2ctb3BlbicsXG4gICAgICAgICAgICB3aWR0aDogbnVsbCxcbiAgICAgICAgICAgIGhlaWdodDogbnVsbFxuICAgICAgICB9O1xuXG4gICAgICAgIHRoaXMuc2V0Rm9yY2VIdG1sUmVsb2FkID0gZnVuY3Rpb24gKF91c2VJdCkge1xuICAgICAgICAgICAgZm9yY2VFbGVtZW50c1JlbG9hZC5odG1sID0gX3VzZUl0IHx8IGZhbHNlO1xuICAgICAgICB9O1xuXG4gICAgICAgIHRoaXMuc2V0Rm9yY2VCb2R5UmVsb2FkID0gZnVuY3Rpb24gKF91c2VJdCkge1xuICAgICAgICAgICAgZm9yY2VFbGVtZW50c1JlbG9hZC5ib2R5ID0gX3VzZUl0IHx8IGZhbHNlO1xuICAgICAgICB9O1xuXG4gICAgICAgIHRoaXMuc2V0RGVmYXVsdHMgPSBmdW5jdGlvbiAobmV3RGVmYXVsdHMpIHtcbiAgICAgICAgICAgIGFuZ3VsYXIuZXh0ZW5kKGRlZmF1bHRzLCBuZXdEZWZhdWx0cyk7XG4gICAgICAgIH07XG5cbiAgICAgICAgdGhpcy5zZXRPcGVuT25lUGVyTmFtZSA9IGZ1bmN0aW9uIChpc09wZW5PbmUpIHtcbiAgICAgICAgICAgIG9wZW5PbmVQZXJOYW1lID0gaXNPcGVuT25lIHx8IGZhbHNlO1xuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBnbG9iYWxJRCA9IDAsIGRpYWxvZ3NDb3VudCA9IDAsIGNsb3NlQnlEb2N1bWVudEhhbmRsZXIsIGRlZmVycyA9IHt9O1xuXG4gICAgICAgIHRoaXMuJGdldCA9IFsnJGRvY3VtZW50JywgJyR0ZW1wbGF0ZUNhY2hlJywgJyRjb21waWxlJywgJyRxJywgJyRodHRwJywgJyRyb290U2NvcGUnLCAnJHRpbWVvdXQnLCAnJHdpbmRvdycsICckY29udHJvbGxlcicsICckaW5qZWN0b3InLFxuICAgICAgICAgICAgZnVuY3Rpb24gKCRkb2N1bWVudCwgJHRlbXBsYXRlQ2FjaGUsICRjb21waWxlLCAkcSwgJGh0dHAsICRyb290U2NvcGUsICR0aW1lb3V0LCAkd2luZG93LCAkY29udHJvbGxlciwgJGluamVjdG9yKSB7XG4gICAgICAgICAgICAgICAgdmFyICRlbGVtZW50cyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgdmFyIHByaXZhdGVNZXRob2RzID0ge1xuICAgICAgICAgICAgICAgICAgICBvbkRvY3VtZW50S2V5ZG93bjogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMjcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwdWJsaWNNZXRob2RzLmNsb3NlKCckZXNjYXBlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgYWN0aXZhdGU6IGZ1bmN0aW9uKCRkaWFsb2cpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvcHRpb25zID0gJGRpYWxvZy5kYXRhKCckbmdEaWFsb2dPcHRpb25zJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLnRyYXBGb2N1cykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cub24oJ2tleWRvd24nLCBwcml2YXRlTWV0aG9kcy5vblRyYXBGb2N1c0tleWRvd24pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gQ2F0Y2ggcm9ndWUgY2hhbmdlcyAoZWcuIGFmdGVyIHVuZm9jdXNpbmcgZXZlcnl0aGluZyBieSBjbGlja2luZyBhIG5vbi1mb2N1c2FibGUgZWxlbWVudClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuYm9keS5vbigna2V5ZG93bicsIHByaXZhdGVNZXRob2RzLm9uVHJhcEZvY3VzS2V5ZG93bik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgZGVhY3RpdmF0ZTogZnVuY3Rpb24gKCRkaWFsb2cpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cub2ZmKCdrZXlkb3duJywgcHJpdmF0ZU1ldGhvZHMub25UcmFwRm9jdXNLZXlkb3duKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRlbGVtZW50cy5ib2R5Lm9mZigna2V5ZG93bicsIHByaXZhdGVNZXRob2RzLm9uVHJhcEZvY3VzS2V5ZG93bik7XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgZGVhY3RpdmF0ZUFsbDogZnVuY3Rpb24gKGVscykge1xuICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5mb3JFYWNoKGVscyxmdW5jdGlvbihlbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkZGlhbG9nID0gYW5ndWxhci5lbGVtZW50KGVsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcml2YXRlTWV0aG9kcy5kZWFjdGl2YXRlKCRkaWFsb2cpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgc2V0Qm9keVBhZGRpbmc6IGZ1bmN0aW9uICh3aWR0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9yaWdpbmFsQm9keVBhZGRpbmcgPSBwYXJzZUludCgoJGVsZW1lbnRzLmJvZHkuY3NzKCdwYWRkaW5nLXJpZ2h0JykgfHwgMCksIDEwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRlbGVtZW50cy5ib2R5LmNzcygncGFkZGluZy1yaWdodCcsIChvcmlnaW5hbEJvZHlQYWRkaW5nICsgd2lkdGgpICsgJ3B4Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuYm9keS5kYXRhKCduZy1kaWFsb2ctb3JpZ2luYWwtcGFkZGluZycsIG9yaWdpbmFsQm9keVBhZGRpbmcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KCduZ0RpYWxvZy5zZXRQYWRkaW5nJywgd2lkdGgpO1xuICAgICAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgICAgIHJlc2V0Qm9keVBhZGRpbmc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvcmlnaW5hbEJvZHlQYWRkaW5nID0gJGVsZW1lbnRzLmJvZHkuZGF0YSgnbmctZGlhbG9nLW9yaWdpbmFsLXBhZGRpbmcnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcmlnaW5hbEJvZHlQYWRkaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGVsZW1lbnRzLmJvZHkuY3NzKCdwYWRkaW5nLXJpZ2h0Jywgb3JpZ2luYWxCb2R5UGFkZGluZyArICdweCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuYm9keS5jc3MoJ3BhZGRpbmctcmlnaHQnLCAnJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoJ25nRGlhbG9nLnNldFBhZGRpbmcnLCAwKTtcbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBwZXJmb3JtQ2xvc2VEaWFsb2c6IGZ1bmN0aW9uICgkZGlhbG9nLCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9wdGlvbnMgPSAkZGlhbG9nLmRhdGEoJyRuZ0RpYWxvZ09wdGlvbnMnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpZCA9ICRkaWFsb2cuYXR0cignaWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzY29wZSA9IHNjb3Blc1tpZF07XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc2NvcGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBBbHJlYWR5IGNsb3NlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiAkd2luZG93LkhhbW1lciAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaGFtbWVyVGltZSA9IHNjb3BlLmhhbW1lclRpbWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFtbWVyVGltZS5vZmYoJ3RhcCcsIGNsb3NlQnlEb2N1bWVudEhhbmRsZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbW1lclRpbWUuZGVzdHJveSAmJiBoYW1tZXJUaW1lLmRlc3Ryb3koKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWxldGUgc2NvcGUuaGFtbWVyVGltZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZy51bmJpbmQoJ2NsaWNrJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkaWFsb2dzQ291bnQgPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuYm9keS51bmJpbmQoJ2tleWRvd24nLCBwcml2YXRlTWV0aG9kcy5vbkRvY3VtZW50S2V5ZG93bik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghJGRpYWxvZy5oYXNDbGFzcygnbmdkaWFsb2ctY2xvc2luZycpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaWFsb2dzQ291bnQgLT0gMTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHByZXZpb3VzRm9jdXMgPSAkZGlhbG9nLmRhdGEoJyRuZ0RpYWxvZ1ByZXZpb3VzRm9jdXMnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwcmV2aW91c0ZvY3VzICYmIHByZXZpb3VzRm9jdXMuZm9jdXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmV2aW91c0ZvY3VzLmZvY3VzKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdCgnbmdEaWFsb2cuY2xvc2luZycsICRkaWFsb2csIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpYWxvZ3NDb3VudCA9IGRpYWxvZ3NDb3VudCA8IDAgPyAwIDogZGlhbG9nc0NvdW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFuaW1hdGlvbkVuZFN1cHBvcnQgJiYgIW9wdGlvbnMuZGlzYWJsZUFuaW1hdGlvbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLiRkZXN0cm95KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZy51bmJpbmQoYW5pbWF0aW9uRW5kRXZlbnQpLmJpbmQoYW5pbWF0aW9uRW5kRXZlbnQsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMuY2xvc2VEaWFsb2dFbGVtZW50KCRkaWFsb2csIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS5hZGRDbGFzcygnbmdkaWFsb2ctY2xvc2luZycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLmNsb3NlRGlhbG9nRWxlbWVudCgkZGlhbG9nLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGVmZXJzW2lkXSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmVyc1tpZF0ucmVzb2x2ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBpZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZGlhbG9nOiAkZGlhbG9nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZW1haW5pbmdEaWFsb2dzOiBkaWFsb2dzQ291bnRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWxldGUgZGVmZXJzW2lkXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZXNbaWRdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHNjb3Blc1tpZF07XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGVuSWRTdGFjay5zcGxpY2Uob3BlbklkU3RhY2suaW5kZXhPZihpZCksIDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFvcGVuSWRTdGFjay5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuYm9keS51bmJpbmQoJ2tleWRvd24nLCBwcml2YXRlTWV0aG9kcy5vbkRvY3VtZW50S2V5ZG93bik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5ZG93bklzQm91bmQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBjbG9zZURpYWxvZ0VsZW1lbnQ6IGZ1bmN0aW9uKCRkaWFsb2csIHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb3B0aW9ucyA9ICRkaWFsb2cuZGF0YSgnJG5nRGlhbG9nT3B0aW9ucycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZy5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkaWFsb2dzQ291bnQgPT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuaHRtbC5yZW1vdmVDbGFzcyhvcHRpb25zLmJvZHlDbGFzc05hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRlbGVtZW50cy5ib2R5LnJlbW92ZUNsYXNzKG9wdGlvbnMuYm9keUNsYXNzTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMucmVzZXRCb2R5UGFkZGluZygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KCduZ0RpYWxvZy5jbG9zZWQnLCAkZGlhbG9nLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgY2xvc2VEaWFsb2c6IGZ1bmN0aW9uICgkZGlhbG9nLCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHByZUNsb3NlQ2FsbGJhY2sgPSAkZGlhbG9nLmRhdGEoJyRuZ0RpYWxvZ1ByZUNsb3NlQ2FsbGJhY2snKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByZUNsb3NlQ2FsbGJhY2sgJiYgYW5ndWxhci5pc0Z1bmN0aW9uKHByZUNsb3NlQ2FsbGJhY2spKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcHJlQ2xvc2VDYWxsYmFja1Jlc3VsdCA9IHByZUNsb3NlQ2FsbGJhY2suY2FsbCgkZGlhbG9nLCB2YWx1ZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYW5ndWxhci5pc09iamVjdChwcmVDbG9zZUNhbGxiYWNrUmVzdWx0KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJlQ2xvc2VDYWxsYmFja1Jlc3VsdC5jbG9zZVByb21pc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZUNsb3NlQ2FsbGJhY2tSZXN1bHQuY2xvc2VQcm9taXNlLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLnBlcmZvcm1DbG9zZURpYWxvZygkZGlhbG9nLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVDbG9zZUNhbGxiYWNrUmVzdWx0LnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLnBlcmZvcm1DbG9zZURpYWxvZygkZGlhbG9nLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHByZUNsb3NlQ2FsbGJhY2tSZXN1bHQgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLnBlcmZvcm1DbG9zZURpYWxvZygkZGlhbG9nLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMucGVyZm9ybUNsb3NlRGlhbG9nKCRkaWFsb2csIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBvblRyYXBGb2N1c0tleWRvd246IGZ1bmN0aW9uKGV2KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZWwgPSBhbmd1bGFyLmVsZW1lbnQoZXYuY3VycmVudFRhcmdldCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgJGRpYWxvZztcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVsLmhhc0NsYXNzKCduZ2RpYWxvZycpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZyA9IGVsO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZGlhbG9nID0gcHJpdmF0ZU1ldGhvZHMuZ2V0QWN0aXZlRGlhbG9nKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGRpYWxvZyA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgaXNUYWIgPSAoZXYua2V5Q29kZSA9PT0gOSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYmFja3dhcmQgPSAoZXYuc2hpZnRLZXkgPT09IHRydWUpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNUYWIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcml2YXRlTWV0aG9kcy5oYW5kbGVUYWIoJGRpYWxvZywgZXYsIGJhY2t3YXJkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWI6IGZ1bmN0aW9uKCRkaWFsb2csIGV2LCBiYWNrd2FyZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGZvY3VzYWJsZUVsZW1lbnRzID0gcHJpdmF0ZU1ldGhvZHMuZ2V0Rm9jdXNhYmxlRWxlbWVudHMoJGRpYWxvZyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmb2N1c2FibGVFbGVtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCAmJiBkb2N1bWVudC5hY3RpdmVFbGVtZW50LmJsdXIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuYWN0aXZlRWxlbWVudC5ibHVyKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRGb2N1cyA9IGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZm9jdXNJbmRleCA9IEFycmF5LnByb3RvdHlwZS5pbmRleE9mLmNhbGwoZm9jdXNhYmxlRWxlbWVudHMsIGN1cnJlbnRGb2N1cyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpc0ZvY3VzSW5kZXhVbmtub3duID0gKGZvY3VzSW5kZXggPT09IC0xKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpc0ZpcnN0RWxlbWVudEZvY3VzZWQgPSAoZm9jdXNJbmRleCA9PT0gMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgaXNMYXN0RWxlbWVudEZvY3VzZWQgPSAoZm9jdXNJbmRleCA9PT0gZm9jdXNhYmxlRWxlbWVudHMubGVuZ3RoIC0gMSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjYW5jZWxFdmVudCA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmFja3dhcmQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNGb2N1c0luZGV4VW5rbm93biB8fCBpc0ZpcnN0RWxlbWVudEZvY3VzZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9jdXNhYmxlRWxlbWVudHNbZm9jdXNhYmxlRWxlbWVudHMubGVuZ3RoIC0gMV0uZm9jdXMoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FuY2VsRXZlbnQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzRm9jdXNJbmRleFVua25vd24gfHwgaXNMYXN0RWxlbWVudEZvY3VzZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9jdXNhYmxlRWxlbWVudHNbMF0uZm9jdXMoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FuY2VsRXZlbnQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNhbmNlbEV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBhdXRvRm9jdXM6IGZ1bmN0aW9uKCRkaWFsb2cpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBkaWFsb2dFbCA9ICRkaWFsb2dbMF07XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEJyb3dzZXIncyAoQ2hyb21lIDQwLCBGb3JlZml4IDM3LCBJRSAxMSkgZG9uJ3QgYXBwZWFyIHRvIGhvbm9yIGF1dG9mb2N1cyBvbiB0aGUgZGlhbG9nLCBidXQgd2Ugc2hvdWxkXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYXV0b0ZvY3VzRWwgPSBkaWFsb2dFbC5xdWVyeVNlbGVjdG9yKCcqW2F1dG9mb2N1c10nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhdXRvRm9jdXNFbCAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1c0VsLmZvY3VzKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCA9PT0gYXV0b0ZvY3VzRWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEF1dG9mb2N1cyBlbGVtZW50IG1pZ2h0IHdhcyBkaXNwbGF5OiBub25lLCBzbyBsZXQncyBjb250aW51ZVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZm9jdXNhYmxlRWxlbWVudHMgPSBwcml2YXRlTWV0aG9kcy5nZXRGb2N1c2FibGVFbGVtZW50cygkZGlhbG9nKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZvY3VzYWJsZUVsZW1lbnRzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb2N1c2FibGVFbGVtZW50c1swXS5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gV2UgbmVlZCB0byBmb2N1cyBzb21ldGhpbmcgZm9yIHRoZSBzY3JlZW4gcmVhZGVycyB0byBub3RpY2UgdGhlIGRpYWxvZ1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNvbnRlbnRFbGVtZW50cyA9IHByaXZhdGVNZXRob2RzLmZpbHRlclZpc2libGVFbGVtZW50cyhkaWFsb2dFbC5xdWVyeVNlbGVjdG9yQWxsKCdoMSxoMixoMyxoNCxoNSxoNixwLHNwYW4nKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjb250ZW50RWxlbWVudHMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjb250ZW50RWxlbWVudCA9IGNvbnRlbnRFbGVtZW50c1swXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWwoY29udGVudEVsZW1lbnQpLmF0dHIoJ3RhYmluZGV4JywgJy0xJykuY3NzKCdvdXRsaW5lJywgJzAnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50RWxlbWVudC5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgICAgIGdldEZvY3VzYWJsZUVsZW1lbnRzOiBmdW5jdGlvbiAoJGRpYWxvZykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRpYWxvZ0VsID0gJGRpYWxvZ1swXTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJhd0VsZW1lbnRzID0gZGlhbG9nRWwucXVlcnlTZWxlY3RvckFsbChmb2N1c2FibGVFbGVtZW50U2VsZWN0b3IpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJZ25vcmUgdW50YWJiYWJsZSBlbGVtZW50cywgaWUuIHRob3NlIHdpdGggdGFiaW5kZXggPSAtMVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRhYmJhYmxlRWxlbWVudHMgPSBwcml2YXRlTWV0aG9kcy5maWx0ZXJUYWJiYWJsZUVsZW1lbnRzKHJhd0VsZW1lbnRzKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHByaXZhdGVNZXRob2RzLmZpbHRlclZpc2libGVFbGVtZW50cyh0YWJiYWJsZUVsZW1lbnRzKTtcbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJUYWJiYWJsZUVsZW1lbnRzOiBmdW5jdGlvbiAoZWxzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGFiYmFibGVGb2N1c2FibGVFbGVtZW50cyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGVscy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlbCA9IGVsc1tpXTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkZWwoZWwpLmF0dHIoJ3RhYmluZGV4JykgIT09ICctMScpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFiYmFibGVGb2N1c2FibGVFbGVtZW50cy5wdXNoKGVsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0YWJiYWJsZUZvY3VzYWJsZUVsZW1lbnRzO1xuICAgICAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlclZpc2libGVFbGVtZW50czogZnVuY3Rpb24gKGVscykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHZpc2libGVGb2N1c2FibGVFbGVtZW50cyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGVscy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlbCA9IGVsc1tpXTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbC5vZmZzZXRXaWR0aCA+IDAgfHwgZWwub2Zmc2V0SGVpZ2h0ID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmxlRm9jdXNhYmxlRWxlbWVudHMucHVzaChlbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmlzaWJsZUZvY3VzYWJsZUVsZW1lbnRzO1xuICAgICAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgICAgIGdldEFjdGl2ZURpYWxvZzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRpYWxvZ3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubmdkaWFsb2cnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRpYWxvZ3MubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IFRoaXMgbWlnaHQgYmUgaW5jb3JyZWN0IGlmIHRoZXJlIGFyZSBhIG1peCBvZiBvcGVuIGRpYWxvZ3Mgd2l0aCBkaWZmZXJlbnQgJ2FwcGVuZFRvJyB2YWx1ZXNcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAkZWwoZGlhbG9nc1tkaWFsb2dzLmxlbmd0aCAtIDFdKTtcbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBhcHBseUFyaWFBdHRyaWJ1dGVzOiBmdW5jdGlvbiAoJGRpYWxvZywgb3B0aW9ucykge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMuYXJpYUF1dG8pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIW9wdGlvbnMuYXJpYVJvbGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRldGVjdGVkUm9sZSA9IChwcml2YXRlTWV0aG9kcy5nZXRGb2N1c2FibGVFbGVtZW50cygkZGlhbG9nKS5sZW5ndGggPiAwKSA/XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZGlhbG9nJyA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYWxlcnRkaWFsb2cnO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnMuYXJpYVJvbGUgPSBkZXRlY3RlZFJvbGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFvcHRpb25zLmFyaWFMYWJlbGxlZEJ5U2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5hcmlhTGFiZWxsZWRCeVNlbGVjdG9yID0gJ2gxLGgyLGgzLGg0LGg1LGg2JztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIW9wdGlvbnMuYXJpYURlc2NyaWJlZEJ5U2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5hcmlhRGVzY3JpYmVkQnlTZWxlY3RvciA9ICdhcnRpY2xlLHNlY3Rpb24scCc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5hcmlhUm9sZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cuYXR0cigncm9sZScsIG9wdGlvbnMuYXJpYVJvbGUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBwcml2YXRlTWV0aG9kcy5hcHBseUFyaWFBdHRyaWJ1dGUoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZywgJ2FyaWEtbGFiZWxsZWRieScsIG9wdGlvbnMuYXJpYUxhYmVsbGVkQnlJZCwgb3B0aW9ucy5hcmlhTGFiZWxsZWRCeVNlbGVjdG9yKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMuYXBwbHlBcmlhQXR0cmlidXRlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2csICdhcmlhLWRlc2NyaWJlZGJ5Jywgb3B0aW9ucy5hcmlhRGVzY3JpYmVkQnlJZCwgb3B0aW9ucy5hcmlhRGVzY3JpYmVkQnlTZWxlY3Rvcik7XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgYXBwbHlBcmlhQXR0cmlidXRlOiBmdW5jdGlvbigkZGlhbG9nLCBhdHRyLCBpZCwgc2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cuYXR0cihhdHRyLCBpZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3Rvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBkaWFsb2dJZCA9ICRkaWFsb2cuYXR0cignaWQnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBmaXJzdE1hdGNoID0gJGRpYWxvZ1swXS5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZmlyc3RNYXRjaCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGdlbmVyYXRlZElkID0gZGlhbG9nSWQgKyAnLScgKyBhdHRyO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGVsKGZpcnN0TWF0Y2gpLmF0dHIoJ2lkJywgZ2VuZXJhdGVkSWQpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZy5hdHRyKGF0dHIsIGdlbmVyYXRlZElkKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBnZW5lcmF0ZWRJZDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBkZXRlY3RVSVJvdXRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvL0RldGVjdCBpZiB1aS1yb3V0ZXIgbW9kdWxlIGlzIGluc3RhbGxlZCBpZiBub3QgcmV0dXJuIGZhbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIubW9kdWxlKCd1aS5yb3V0ZXInKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gY2F0Y2goZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgICAgIGdldFJvdXRlckxvY2F0aW9uRXZlbnROYW1lOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHByaXZhdGVNZXRob2RzLmRldGVjdFVJUm91dGVyKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJyRzdGF0ZUNoYW5nZVN0YXJ0JztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnJGxvY2F0aW9uQ2hhbmdlU3RhcnQnO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgIHZhciBwdWJsaWNNZXRob2RzID0ge1xuICAgICAgICAgICAgICAgICAgICBfX1BSSVZBVEVfXzogcHJpdmF0ZU1ldGhvZHMsXG5cbiAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnM6XG4gICAgICAgICAgICAgICAgICAgICAqIC0gdGVtcGxhdGUge1N0cmluZ30gLSBpZCBvZiBuZy10ZW1wbGF0ZSwgdXJsIGZvciBwYXJ0aWFsLCBwbGFpbiBzdHJpbmcgKGlmIGVuYWJsZWQpXG4gICAgICAgICAgICAgICAgICAgICAqIC0gcGxhaW4ge0Jvb2xlYW59IC0gZW5hYmxlIHBsYWluIHN0cmluZyB0ZW1wbGF0ZXMsIGRlZmF1bHQgZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgICogLSBzY29wZSB7T2JqZWN0fVxuICAgICAgICAgICAgICAgICAgICAgKiAtIGNvbnRyb2xsZXIge1N0cmluZ31cbiAgICAgICAgICAgICAgICAgICAgICogLSBjb250cm9sbGVyQXMge1N0cmluZ31cbiAgICAgICAgICAgICAgICAgICAgICogLSBjbGFzc05hbWUge1N0cmluZ30gLSBkaWFsb2cgdGhlbWUgY2xhc3NcbiAgICAgICAgICAgICAgICAgICAgICogLSBhcHBlbmRDbGFzc05hbWUge1N0cmluZ30gLSBkaWFsb2cgdGhlbWUgY2xhc3MgdG8gYmUgYXBwZW5kZWQgdG8gZGVmYXVsdHNcbiAgICAgICAgICAgICAgICAgICAgICogLSBkaXNhYmxlQW5pbWF0aW9uIHtCb29sZWFufSAtIHNldCB0byB0cnVlIHRvIGRpc2FibGUgYW5pbWF0aW9uXG4gICAgICAgICAgICAgICAgICAgICAqIC0gc2hvd0Nsb3NlIHtCb29sZWFufSAtIHNob3cgY2xvc2UgYnV0dG9uLCBkZWZhdWx0IHRydWVcbiAgICAgICAgICAgICAgICAgICAgICogLSBjbG9zZUJ5RXNjYXBlIHtCb29sZWFufSAtIGRlZmF1bHQgdHJ1ZVxuICAgICAgICAgICAgICAgICAgICAgKiAtIGNsb3NlQnlEb2N1bWVudCB7Qm9vbGVhbn0gLSBkZWZhdWx0IHRydWVcbiAgICAgICAgICAgICAgICAgICAgICogLSBwcmVDbG9zZUNhbGxiYWNrIHtTdHJpbmd8RnVuY3Rpb259IC0gdXNlciBzdXBwbGllZCBmdW5jdGlvbiBuYW1lL2Z1bmN0aW9uIGNhbGxlZCBiZWZvcmUgY2xvc2luZyBkaWFsb2cgKGlmIHNldClcbiAgICAgICAgICAgICAgICAgICAgICogLSBib2R5Q2xhc3NOYW1lIHtTdHJpbmd9IC0gY2xhc3MgYWRkZWQgdG8gYm9keSBhdCBvcGVuIGRpYWxvZ1xuICAgICAgICAgICAgICAgICAgICAgKiBAcmV0dXJuIHtPYmplY3R9IGRpYWxvZ1xuICAgICAgICAgICAgICAgICAgICAgKi9cbiAgICAgICAgICAgICAgICAgICAgb3BlbjogZnVuY3Rpb24gKG9wdHMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBkaWFsb2dJRCA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRzID0gb3B0cyB8fCB7fTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcGVuT25lUGVyTmFtZSAmJiBvcHRzLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaWFsb2dJRCA9IG9wdHMubmFtZSsnIGRpYWxvZyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNPcGVuKGRpYWxvZ0lEKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9wdGlvbnMgPSBhbmd1bGFyLmNvcHkoZGVmYXVsdHMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxvY2FsSUQgPSArK2dsb2JhbElEO1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlhbG9nSUQgPSBkaWFsb2dJRCB8fCAnbmdkaWFsb2cnICsgbG9jYWxJRDtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wZW5JZFN0YWNrLnB1c2goZGlhbG9nSUQpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBNZXJnZSBvcHRzLmRhdGEgd2l0aCBwcmVkZWZpbmVkIHZpYSBzZXREZWZhdWx0c1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmRhdGEgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvcHRzLmRhdGEgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdHMuZGF0YSA9IHt9O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRzLmRhdGEgPSBhbmd1bGFyLm1lcmdlKGFuZ3VsYXIuY29weShvcHRpb25zLmRhdGEpLCBvcHRzLmRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmV4dGVuZChvcHRpb25zLCBvcHRzKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRlZmVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgZGVmZXJzW2RpYWxvZ0lEXSA9IGRlZmVyID0gJHEuZGVmZXIoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHNjb3BlO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGVzW2RpYWxvZ0lEXSA9IHNjb3BlID0gYW5ndWxhci5pc09iamVjdChvcHRpb25zLnNjb3BlKSA/IG9wdGlvbnMuc2NvcGUuJG5ldygpIDogJHJvb3RTY29wZS4kbmV3KCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkZGlhbG9nLCAkZGlhbG9nUGFyZW50LCAkZGlhbG9nQ29udGVudDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJlc29sdmUgPSBhbmd1bGFyLmV4dGVuZCh7fSwgb3B0aW9ucy5yZXNvbHZlKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5mb3JFYWNoKHJlc29sdmUsIGZ1bmN0aW9uICh2YWx1ZSwga2V5KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZVtrZXldID0gYW5ndWxhci5pc1N0cmluZyh2YWx1ZSkgPyAkaW5qZWN0b3IuZ2V0KHZhbHVlKSA6ICRpbmplY3Rvci5pbnZva2UodmFsdWUsIG51bGwsIG51bGwsIGtleSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJHEuYWxsKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZTogbG9hZFRlbXBsYXRlKG9wdGlvbnMudGVtcGxhdGUgfHwgb3B0aW9ucy50ZW1wbGF0ZVVybCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbG9jYWxzOiAkcS5hbGwocmVzb2x2ZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKHNldHVwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRlbXBsYXRlID0gc2V0dXAudGVtcGxhdGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FscyA9IHNldHVwLmxvY2FscztcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLnNob3dDbG9zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZSArPSAnPGRpdiBjbGFzcz1cIm5nZGlhbG9nLWNsb3NlXCI+PC9kaXY+JztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaGFzT3ZlcmxheUNsYXNzID0gb3B0aW9ucy5vdmVybGF5ID8gJycgOiAnIG5nZGlhbG9nLW5vLW92ZXJsYXknO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cgPSAkZWwoJzxkaXYgaWQ9XCInK2RpYWxvZ0lEICsgJ1wiIGNsYXNzPVwibmdkaWFsb2cnICsgaGFzT3ZlcmxheUNsYXNzICsgJ1wiPjwvZGl2PicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cuaHRtbCgob3B0aW9ucy5vdmVybGF5ID9cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJuZ2RpYWxvZy1vdmVybGF5XCI+PC9kaXY+PGRpdiBjbGFzcz1cIm5nZGlhbG9nLWNvbnRlbnRcIiByb2xlPVwiZG9jdW1lbnRcIj4nICsgdGVtcGxhdGUgKyAnPC9kaXY+JyA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibmdkaWFsb2ctY29udGVudFwiIHJvbGU9XCJkb2N1bWVudFwiPicgKyB0ZW1wbGF0ZSArICc8L2Rpdj4nKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZGlhbG9nLmRhdGEoJyRuZ0RpYWxvZ09wdGlvbnMnLCBvcHRpb25zKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLm5nRGlhbG9nSWQgPSBkaWFsb2dJRDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLmRhdGEgJiYgYW5ndWxhci5pc1N0cmluZyhvcHRpb25zLmRhdGEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBmaXJzdExldHRlciA9IG9wdGlvbnMuZGF0YS5yZXBsYWNlKC9eXFxzKi8sICcnKVswXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubmdEaWFsb2dEYXRhID0gKGZpcnN0TGV0dGVyID09PSAneycgfHwgZmlyc3RMZXR0ZXIgPT09ICdbJykgPyBhbmd1bGFyLmZyb21Kc29uKG9wdGlvbnMuZGF0YSkgOiBuZXcgU3RyaW5nKG9wdGlvbnMuZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLm5nRGlhbG9nRGF0YS5uZ0RpYWxvZ0lkID0gZGlhbG9nSUQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChvcHRpb25zLmRhdGEgJiYgYW5ndWxhci5pc09iamVjdChvcHRpb25zLmRhdGEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLm5nRGlhbG9nRGF0YSA9IG9wdGlvbnMuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubmdEaWFsb2dEYXRhLm5nRGlhbG9nSWQgPSBkaWFsb2dJRDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5jbGFzc05hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZy5hZGRDbGFzcyhvcHRpb25zLmNsYXNzTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMuYXBwZW5kQ2xhc3NOYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cuYWRkQ2xhc3Mob3B0aW9ucy5hcHBlbmRDbGFzc05hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLndpZHRoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2dDb250ZW50ID0gJGRpYWxvZ1swXS5xdWVyeVNlbGVjdG9yKCcubmdkaWFsb2ctY29udGVudCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYW5ndWxhci5pc1N0cmluZyhvcHRpb25zLndpZHRoKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZ0NvbnRlbnQuc3R5bGUud2lkdGggPSBvcHRpb25zLndpZHRoO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZ0NvbnRlbnQuc3R5bGUud2lkdGggPSBvcHRpb25zLndpZHRoICsgJ3B4JztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLmhlaWdodCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZGlhbG9nQ29udGVudCA9ICRkaWFsb2dbMF0ucXVlcnlTZWxlY3RvcignLm5nZGlhbG9nLWNvbnRlbnQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFuZ3VsYXIuaXNTdHJpbmcob3B0aW9ucy5oZWlnaHQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZGlhbG9nQ29udGVudC5zdHlsZS5oZWlnaHQgPSBvcHRpb25zLmhlaWdodDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2dDb250ZW50LnN0eWxlLmhlaWdodCA9IG9wdGlvbnMuaGVpZ2h0ICsgJ3B4JztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLmRpc2FibGVBbmltYXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZy5hZGRDbGFzcyhkaXNhYmxlZEFuaW1hdGlvbkNsYXNzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5hcHBlbmRUbyAmJiBhbmd1bGFyLmlzU3RyaW5nKG9wdGlvbnMuYXBwZW5kVG8pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2dQYXJlbnQgPSBhbmd1bGFyLmVsZW1lbnQoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihvcHRpb25zLmFwcGVuZFRvKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZ1BhcmVudCA9ICRlbGVtZW50cy5ib2R5O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLmFwcGx5QXJpYUF0dHJpYnV0ZXMoJGRpYWxvZywgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5wcmVDbG9zZUNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwcmVDbG9zZUNhbGxiYWNrO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhbmd1bGFyLmlzRnVuY3Rpb24ob3B0aW9ucy5wcmVDbG9zZUNhbGxiYWNrKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlQ2xvc2VDYWxsYmFjayA9IG9wdGlvbnMucHJlQ2xvc2VDYWxsYmFjaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChhbmd1bGFyLmlzU3RyaW5nKG9wdGlvbnMucHJlQ2xvc2VDYWxsYmFjaykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhbmd1bGFyLmlzRnVuY3Rpb24oc2NvcGVbb3B0aW9ucy5wcmVDbG9zZUNhbGxiYWNrXSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlQ2xvc2VDYWxsYmFjayA9IHNjb3BlW29wdGlvbnMucHJlQ2xvc2VDYWxsYmFja107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzY29wZS4kcGFyZW50ICYmIGFuZ3VsYXIuaXNGdW5jdGlvbihzY29wZS4kcGFyZW50W29wdGlvbnMucHJlQ2xvc2VDYWxsYmFja10pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZUNsb3NlQ2FsbGJhY2sgPSBzY29wZS4kcGFyZW50W29wdGlvbnMucHJlQ2xvc2VDYWxsYmFja107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICgkcm9vdFNjb3BlICYmIGFuZ3VsYXIuaXNGdW5jdGlvbigkcm9vdFNjb3BlW29wdGlvbnMucHJlQ2xvc2VDYWxsYmFja10pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZUNsb3NlQ2FsbGJhY2sgPSAkcm9vdFNjb3BlW29wdGlvbnMucHJlQ2xvc2VDYWxsYmFja107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByZUNsb3NlQ2FsbGJhY2spIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cuZGF0YSgnJG5nRGlhbG9nUHJlQ2xvc2VDYWxsYmFjaycsIHByZUNsb3NlQ2FsbGJhY2spO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuY2xvc2VUaGlzRGlhbG9nID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLmNsb3NlRGlhbG9nKCRkaWFsb2csIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMuY29udHJvbGxlciAmJiAoYW5ndWxhci5pc1N0cmluZyhvcHRpb25zLmNvbnRyb2xsZXIpIHx8IGFuZ3VsYXIuaXNBcnJheShvcHRpb25zLmNvbnRyb2xsZXIpIHx8IGFuZ3VsYXIuaXNGdW5jdGlvbihvcHRpb25zLmNvbnRyb2xsZXIpKSkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBsYWJlbDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5jb250cm9sbGVyQXMgJiYgYW5ndWxhci5pc1N0cmluZyhvcHRpb25zLmNvbnRyb2xsZXJBcykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsID0gb3B0aW9ucy5jb250cm9sbGVyQXM7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY29udHJvbGxlckluc3RhbmNlID0gJGNvbnRyb2xsZXIob3B0aW9ucy5jb250cm9sbGVyLCBhbmd1bGFyLmV4dGVuZChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FscyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGU6IHNjb3BlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRlbGVtZW50OiAkZGlhbG9nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKG9wdGlvbnMuYmluZFRvQ29udHJvbGxlcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5leHRlbmQoY29udHJvbGxlckluc3RhbmNlLmluc3RhbmNlLCB7bmdEaWFsb2dJZDogc2NvcGUubmdEaWFsb2dJZCwgbmdEaWFsb2dEYXRhOiBzY29wZS5uZ0RpYWxvZ0RhdGEsIGNsb3NlVGhpc0RpYWxvZzogc2NvcGUuY2xvc2VUaGlzRGlhbG9nLCBjb25maXJtOiBzY29wZS5jb25maXJtfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZih0eXBlb2YgY29udHJvbGxlckluc3RhbmNlID09PSAnZnVuY3Rpb24nKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cuZGF0YSgnJG5nRGlhbG9nQ29udHJvbGxlckNvbnRyb2xsZXInLCBjb250cm9sbGVySW5zdGFuY2UoKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZGlhbG9nLmRhdGEoJyRuZ0RpYWxvZ0NvbnRyb2xsZXJDb250cm9sbGVyJywgY29udHJvbGxlckluc3RhbmNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRhY3RpdmVEaWFsb2dzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm5nZGlhbG9nJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLmRlYWN0aXZhdGVBbGwoJGFjdGl2ZURpYWxvZ3MpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb21waWxlKCRkaWFsb2cpKHNjb3BlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHdpZHRoRGlmZnMgPSAkd2luZG93LmlubmVyV2lkdGggLSAkZWxlbWVudHMuYm9keS5wcm9wKCdjbGllbnRXaWR0aCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuaHRtbC5hZGRDbGFzcyhvcHRpb25zLmJvZHlDbGFzc05hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuYm9keS5hZGRDbGFzcyhvcHRpb25zLmJvZHlDbGFzc05hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgc2Nyb2xsQmFyV2lkdGggPSB3aWR0aERpZmZzIC0gKCR3aW5kb3cuaW5uZXJXaWR0aCAtICRlbGVtZW50cy5ib2R5LnByb3AoJ2NsaWVudFdpZHRoJykpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2Nyb2xsQmFyV2lkdGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcml2YXRlTWV0aG9kcy5zZXRCb2R5UGFkZGluZyhzY3JvbGxCYXJXaWR0aCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZ1BhcmVudC5hcHBlbmQoJGRpYWxvZyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMuYWN0aXZhdGUoJGRpYWxvZyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMudHJhcEZvY3VzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcml2YXRlTWV0aG9kcy5hdXRvRm9jdXMoJGRpYWxvZyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5uYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoJ25nRGlhbG9nLm9wZW5lZCcsIHtkaWFsb2c6ICRkaWFsb2csIG5hbWU6IG9wdGlvbnMubmFtZX0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KCduZ0RpYWxvZy5vcGVuZWQnLCAkZGlhbG9nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFrZXlkb3duSXNCb3VuZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHMuYm9keS5iaW5kKCdrZXlkb3duJywgcHJpdmF0ZU1ldGhvZHMub25Eb2N1bWVudEtleWRvd24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXlkb3duSXNCb3VuZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMuY2xvc2VCeU5hdmlnYXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGV2ZW50TmFtZSA9IHByaXZhdGVNZXRob2RzLmdldFJvdXRlckxvY2F0aW9uRXZlbnROYW1lKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJG9uKGV2ZW50TmFtZSwgZnVuY3Rpb24gKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByaXZhdGVNZXRob2RzLmNsb3NlRGlhbG9nKCRkaWFsb2cpID09PSBmYWxzZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wdGlvbnMucHJlc2VydmVGb2N1cykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZGlhbG9nLmRhdGEoJyRuZ0RpYWxvZ1ByZXZpb3VzRm9jdXMnLCBkb2N1bWVudC5hY3RpdmVFbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbG9zZUJ5RG9jdW1lbnRIYW5kbGVyID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpc092ZXJsYXkgPSBvcHRpb25zLmNsb3NlQnlEb2N1bWVudCA/ICRlbChldmVudC50YXJnZXQpLmhhc0NsYXNzKCduZ2RpYWxvZy1vdmVybGF5JykgOiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGlzQ2xvc2VCdG4gPSAkZWwoZXZlbnQudGFyZ2V0KS5oYXNDbGFzcygnbmdkaWFsb2ctY2xvc2UnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNPdmVybGF5IHx8IGlzQ2xvc2VCdG4pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHB1YmxpY01ldGhvZHMuY2xvc2UoJGRpYWxvZy5hdHRyKCdpZCcpLCBpc0Nsb3NlQnRuID8gJyRjbG9zZUJ1dHRvbicgOiAnJGRvY3VtZW50Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiAkd2luZG93LkhhbW1lciAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGhhbW1lclRpbWUgPSBzY29wZS5oYW1tZXJUaW1lID0gJHdpbmRvdy5IYW1tZXIoJGRpYWxvZ1swXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbW1lclRpbWUub24oJ3RhcCcsIGNsb3NlQnlEb2N1bWVudEhhbmRsZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRkaWFsb2cuYmluZCgnY2xpY2snLCBjbG9zZUJ5RG9jdW1lbnRIYW5kbGVyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaWFsb2dzQ291bnQgKz0gMTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBwdWJsaWNNZXRob2RzO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IGRpYWxvZ0lELFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsb3NlUHJvbWlzZTogZGVmZXIucHJvbWlzZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbG9zZTogZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLmNsb3NlRGlhbG9nKCRkaWFsb2csIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiBsb2FkVGVtcGxhdGVVcmwgKHRtcGwsIGNvbmZpZykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdCgnbmdEaWFsb2cudGVtcGxhdGVMb2FkaW5nJywgdG1wbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCh0bXBsLCAoY29uZmlnIHx8IHt9KSkudGhlbihmdW5jdGlvbihyZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KCduZ0RpYWxvZy50ZW1wbGF0ZUxvYWRlZCcsIHRtcGwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzLmRhdGEgfHwgJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGxvYWRUZW1wbGF0ZSAodG1wbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghdG1wbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ0VtcHR5IHRlbXBsYXRlJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYW5ndWxhci5pc1N0cmluZyh0bXBsKSAmJiBvcHRpb25zLnBsYWluKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0bXBsO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy5jYWNoZSA9PT0gJ2Jvb2xlYW4nICYmICFvcHRpb25zLmNhY2hlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBsb2FkVGVtcGxhdGVVcmwodG1wbCwge2NhY2hlOiBmYWxzZX0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBsb2FkVGVtcGxhdGVVcmwodG1wbCwge2NhY2hlOiAkdGVtcGxhdGVDYWNoZX0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zOlxuICAgICAgICAgICAgICAgICAgICAgKiAtIHRlbXBsYXRlIHtTdHJpbmd9IC0gaWQgb2YgbmctdGVtcGxhdGUsIHVybCBmb3IgcGFydGlhbCwgcGxhaW4gc3RyaW5nIChpZiBlbmFibGVkKVxuICAgICAgICAgICAgICAgICAgICAgKiAtIHBsYWluIHtCb29sZWFufSAtIGVuYWJsZSBwbGFpbiBzdHJpbmcgdGVtcGxhdGVzLCBkZWZhdWx0IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgICAqIC0gbmFtZSB7U3RyaW5nfVxuICAgICAgICAgICAgICAgICAgICAgKiAtIHNjb3BlIHtPYmplY3R9XG4gICAgICAgICAgICAgICAgICAgICAqIC0gY29udHJvbGxlciB7U3RyaW5nfVxuICAgICAgICAgICAgICAgICAgICAgKiAtIGNvbnRyb2xsZXJBcyB7U3RyaW5nfVxuICAgICAgICAgICAgICAgICAgICAgKiAtIGNsYXNzTmFtZSB7U3RyaW5nfSAtIGRpYWxvZyB0aGVtZSBjbGFzc1xuICAgICAgICAgICAgICAgICAgICAgKiAtIGFwcGVuZENsYXNzTmFtZSB7U3RyaW5nfSAtIGRpYWxvZyB0aGVtZSBjbGFzcyB0byBiZSBhcHBlbmRlZCB0byBkZWZhdWx0c1xuICAgICAgICAgICAgICAgICAgICAgKiAtIHNob3dDbG9zZSB7Qm9vbGVhbn0gLSBzaG93IGNsb3NlIGJ1dHRvbiwgZGVmYXVsdCB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAqIC0gY2xvc2VCeUVzY2FwZSB7Qm9vbGVhbn0gLSBkZWZhdWx0IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgICAqIC0gY2xvc2VCeURvY3VtZW50IHtCb29sZWFufSAtIGRlZmF1bHQgZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgICogLSBwcmVDbG9zZUNhbGxiYWNrIHtTdHJpbmd8RnVuY3Rpb259IC0gdXNlciBzdXBwbGllZCBmdW5jdGlvbiBuYW1lL2Z1bmN0aW9uIGNhbGxlZCBiZWZvcmUgY2xvc2luZyBkaWFsb2cgKGlmIHNldCk7IG5vdCBjYWxsZWQgb24gY29uZmlybVxuICAgICAgICAgICAgICAgICAgICAgKiAtIGJvZHlDbGFzc05hbWUge1N0cmluZ30gLSBjbGFzcyBhZGRlZCB0byBib2R5IGF0IG9wZW4gZGlhbG9nXG4gICAgICAgICAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgICAgICAgICAqIEByZXR1cm4ge09iamVjdH0gZGlhbG9nXG4gICAgICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgICAgICBvcGVuQ29uZmlybTogZnVuY3Rpb24gKG9wdHMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBkZWZlciA9ICRxLmRlZmVyKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb3B0aW9ucyA9IGFuZ3VsYXIuY29weShkZWZhdWx0cyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdHMgPSBvcHRzIHx8IHt9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBNZXJnZSBvcHRzLmRhdGEgd2l0aCBwcmVkZWZpbmVkIHZpYSBzZXREZWZhdWx0c1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmRhdGEgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBvcHRzLmRhdGEgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdHMuZGF0YSA9IHt9O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRzLmRhdGEgPSBhbmd1bGFyLm1lcmdlKGFuZ3VsYXIuY29weShvcHRpb25zLmRhdGEpLCBvcHRzLmRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBhbmd1bGFyLmV4dGVuZChvcHRpb25zLCBvcHRzKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5zY29wZSA9IGFuZ3VsYXIuaXNPYmplY3Qob3B0aW9ucy5zY29wZSkgPyBvcHRpb25zLnNjb3BlLiRuZXcoKSA6ICRyb290U2NvcGUuJG5ldygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5zY29wZS5jb25maXJtID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmZXIucmVzb2x2ZSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRkaWFsb2cgPSAkZWwoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQob3BlblJlc3VsdC5pZCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaXZhdGVNZXRob2RzLnBlcmZvcm1DbG9zZURpYWxvZygkZGlhbG9nLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb3BlblJlc3VsdCA9IHB1YmxpY01ldGhvZHMub3BlbihvcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcGVuUmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BlblJlc3VsdC5jbG9zZVByb21pc2UudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGRlZmVyLnJlamVjdChkYXRhLnZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZGVmZXIucmVqZWN0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGRlZmVyLnByb21pc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgaXNPcGVuOiBmdW5jdGlvbihpZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRkaWFsb2cgPSAkZWwoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWQpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAkZGlhbG9nLmxlbmd0aCA+IDA7XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IGlkXG4gICAgICAgICAgICAgICAgICAgICAqIEByZXR1cm4ge09iamVjdH0gZGlhbG9nXG4gICAgICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgICAgICBjbG9zZTogZnVuY3Rpb24gKGlkLCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRkaWFsb2cgPSAkZWwoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoaWQpKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRkaWFsb2cubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMuY2xvc2VEaWFsb2coJGRpYWxvZywgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaWQgPT09ICckZXNjYXBlJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG9wRGlhbG9nSWQgPSBvcGVuSWRTdGFja1tvcGVuSWRTdGFjay5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGRpYWxvZyA9ICRlbChkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0b3BEaWFsb2dJZCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGRpYWxvZy5kYXRhKCckbmdEaWFsb2dPcHRpb25zJykuY2xvc2VCeUVzY2FwZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMuY2xvc2VEaWFsb2coJGRpYWxvZywgJyRlc2NhcGUnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHB1YmxpY01ldGhvZHMuY2xvc2VBbGwodmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHB1YmxpY01ldGhvZHM7XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgY2xvc2VBbGw6IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRhbGwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubmdkaWFsb2cnKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gUmV2ZXJzZSBvcmRlciB0byBlbnN1cmUgZm9jdXMgcmVzdG9yYXRpb24gd29ya3MgYXMgZXhwZWN0ZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAkYWxsLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRpYWxvZyA9ICRhbGxbaV07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpdmF0ZU1ldGhvZHMuY2xvc2VEaWFsb2coJGVsKGRpYWxvZyksIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgICAgICAgICBnZXRPcGVuRGlhbG9nczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gb3BlbklkU3RhY2s7XG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAgICAgZ2V0RGVmYXVsdHM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBkZWZhdWx0cztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICBhbmd1bGFyLmZvckVhY2goXG4gICAgICAgICAgICAgICAgICAgIFsnaHRtbCcsICdib2R5J10sXG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uKGVsZW1lbnROYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudHNbZWxlbWVudE5hbWVdID0gJGRvY3VtZW50LmZpbmQoZWxlbWVudE5hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZvcmNlRWxlbWVudHNSZWxvYWRbZWxlbWVudE5hbWVdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGV2ZW50TmFtZSA9IHByaXZhdGVNZXRob2RzLmdldFJvdXRlckxvY2F0aW9uRXZlbnROYW1lKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kb24oZXZlbnROYW1lLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRlbGVtZW50c1tlbGVtZW50TmFtZV0gPSAkZG9jdW1lbnQuZmluZChlbGVtZW50TmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHB1YmxpY01ldGhvZHM7XG4gICAgICAgICAgICB9XTtcbiAgICB9KTtcblxuICAgIG0uZGlyZWN0aXZlKCduZ0RpYWxvZycsIFsnbmdEaWFsb2cnLCBmdW5jdGlvbiAobmdEaWFsb2cpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIG5nRGlhbG9nU2NvcGU6ICc9J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbSwgYXR0cnMpIHtcbiAgICAgICAgICAgICAgICBlbGVtLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgbmdEaWFsb2dTY29wZSA9IGFuZ3VsYXIuaXNEZWZpbmVkKHNjb3BlLm5nRGlhbG9nU2NvcGUpID8gc2NvcGUubmdEaWFsb2dTY29wZSA6ICdub1Njb3BlJztcbiAgICAgICAgICAgICAgICAgICAgYW5ndWxhci5pc0RlZmluZWQoYXR0cnMubmdEaWFsb2dDbG9zZVByZXZpb3VzKSAmJiBuZ0RpYWxvZy5jbG9zZShhdHRycy5uZ0RpYWxvZ0Nsb3NlUHJldmlvdXMpO1xuXG4gICAgICAgICAgICAgICAgICAgIHZhciBkZWZhdWx0cyA9IG5nRGlhbG9nLmdldERlZmF1bHRzKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgbmdEaWFsb2cub3Blbih7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZTogYXR0cnMubmdEaWFsb2csXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IGF0dHJzLm5nRGlhbG9nQ2xhc3MgfHwgZGVmYXVsdHMuY2xhc3NOYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgYXBwZW5kQ2xhc3NOYW1lOiBhdHRycy5uZ0RpYWxvZ0FwcGVuZENsYXNzLFxuICAgICAgICAgICAgICAgICAgICAgICAgY29udHJvbGxlcjogYXR0cnMubmdEaWFsb2dDb250cm9sbGVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgY29udHJvbGxlckFzOiBhdHRycy5uZ0RpYWxvZ0NvbnRyb2xsZXJBcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGJpbmRUb0NvbnRyb2xsZXI6IGF0dHJzLm5nRGlhbG9nQmluZFRvQ29udHJvbGxlcixcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlOiBuZ0RpYWxvZ1Njb3BlLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogYXR0cnMubmdEaWFsb2dEYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2hvd0Nsb3NlOiBhdHRycy5uZ0RpYWxvZ1Nob3dDbG9zZSA9PT0gJ2ZhbHNlJyA/IGZhbHNlIDogKGF0dHJzLm5nRGlhbG9nU2hvd0Nsb3NlID09PSAndHJ1ZScgPyB0cnVlIDogZGVmYXVsdHMuc2hvd0Nsb3NlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsb3NlQnlEb2N1bWVudDogYXR0cnMubmdEaWFsb2dDbG9zZUJ5RG9jdW1lbnQgPT09ICdmYWxzZScgPyBmYWxzZSA6IChhdHRycy5uZ0RpYWxvZ0Nsb3NlQnlEb2N1bWVudCA9PT0gJ3RydWUnID8gdHJ1ZSA6IGRlZmF1bHRzLmNsb3NlQnlEb2N1bWVudCksXG4gICAgICAgICAgICAgICAgICAgICAgICBjbG9zZUJ5RXNjYXBlOiBhdHRycy5uZ0RpYWxvZ0Nsb3NlQnlFc2NhcGUgPT09ICdmYWxzZScgPyBmYWxzZSA6IChhdHRycy5uZ0RpYWxvZ0Nsb3NlQnlFc2NhcGUgPT09ICd0cnVlJyA/IHRydWUgOiBkZWZhdWx0cy5jbG9zZUJ5RXNjYXBlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJsYXk6IGF0dHJzLm5nRGlhbG9nT3ZlcmxheSA9PT0gJ2ZhbHNlJyA/IGZhbHNlIDogKGF0dHJzLm5nRGlhbG9nT3ZlcmxheSA9PT0gJ3RydWUnID8gdHJ1ZSA6IGRlZmF1bHRzLm92ZXJsYXkpLFxuICAgICAgICAgICAgICAgICAgICAgICAgcHJlQ2xvc2VDYWxsYmFjazogYXR0cnMubmdEaWFsb2dQcmVDbG9zZUNhbGxiYWNrIHx8IGRlZmF1bHRzLnByZUNsb3NlQ2FsbGJhY2ssXG4gICAgICAgICAgICAgICAgICAgICAgICBib2R5Q2xhc3NOYW1lOiBhdHRycy5uZ0RpYWxvZ0JvZHlDbGFzcyB8fCBkZWZhdWx0cy5ib2R5Q2xhc3NOYW1lXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH1dKTtcblxuICAgIHJldHVybiBtO1xufSkpO1xuIiwiLyoqXG4gKiBhbmd1bGFyLXVpLW5vdGlmaWNhdGlvbiAtIEFuZ3VsYXIuanMgc2VydmljZSBwcm92aWRpbmcgc2ltcGxlIG5vdGlmaWNhdGlvbnMgdXNpbmcgQm9vdHN0cmFwIDMgc3R5bGVzIHdpdGggY3NzIHRyYW5zaXRpb25zIGZvciBhbmltYXRpbmdcbiAqIEBhdXRob3IgQWxleF9DcmFja1xuICogQHZlcnNpb24gdjAuMi4wXG4gKiBAbGluayBodHRwczovL2dpdGh1Yi5jb20vYWxleGNyYWNrL2FuZ3VsYXItdWktbm90aWZpY2F0aW9uXG4gKiBAbGljZW5zZSBNSVRcbiAqL1xuYW5ndWxhci5tb2R1bGUoXCJ1aS1ub3RpZmljYXRpb25cIixbXSksYW5ndWxhci5tb2R1bGUoXCJ1aS1ub3RpZmljYXRpb25cIikucHJvdmlkZXIoXCJOb3RpZmljYXRpb25cIixmdW5jdGlvbigpe3RoaXMub3B0aW9ucz17ZGVsYXk6NWUzLHN0YXJ0VG9wOjEwLHN0YXJ0UmlnaHQ6MTAsdmVydGljYWxTcGFjaW5nOjEwLGhvcml6b250YWxTcGFjaW5nOjEwLHBvc2l0aW9uWDpcInJpZ2h0XCIscG9zaXRpb25ZOlwidG9wXCIscmVwbGFjZU1lc3NhZ2U6ITEsdGVtcGxhdGVVcmw6XCJhbmd1bGFyLXVpLW5vdGlmaWNhdGlvbi5odG1sXCIsb25DbG9zZTp2b2lkIDAsY2xvc2VPbkNsaWNrOiEwLG1heENvdW50OjB9LHRoaXMuc2V0T3B0aW9ucz1mdW5jdGlvbihlKXtpZighYW5ndWxhci5pc09iamVjdChlKSl0aHJvdyBuZXcgRXJyb3IoXCJPcHRpb25zIHNob3VsZCBiZSBhbiBvYmplY3QhXCIpO3RoaXMub3B0aW9ucz1hbmd1bGFyLmV4dGVuZCh7fSx0aGlzLm9wdGlvbnMsZSl9LHRoaXMuJGdldD1bXCIkdGltZW91dFwiLFwiJGh0dHBcIixcIiRjb21waWxlXCIsXCIkdGVtcGxhdGVDYWNoZVwiLFwiJHJvb3RTY29wZVwiLFwiJGluamVjdG9yXCIsXCIkc2NlXCIsXCIkcVwiLFwiJHdpbmRvd1wiLGZ1bmN0aW9uKGUsdCxuLGksbyxzLGEsbCxyKXt2YXIgYz10aGlzLm9wdGlvbnMscD1jLnN0YXJ0VG9wLGQ9Yy5zdGFydFJpZ2h0LHU9Yy52ZXJ0aWNhbFNwYWNpbmcsbT1jLmhvcml6b250YWxTcGFjaW5nLGY9Yy5kZWxheSxnPVtdLGg9ITEsQz1mdW5jdGlvbihzLEMpe3ZhciB5PWwuZGVmZXIoKTtyZXR1cm5cIm9iamVjdFwiIT10eXBlb2YgcyYmKHM9e21lc3NhZ2U6c30pLHMuc2NvcGU9cy5zY29wZT9zLnNjb3BlOm8scy50ZW1wbGF0ZT1zLnRlbXBsYXRlVXJsP3MudGVtcGxhdGVVcmw6Yy50ZW1wbGF0ZVVybCxzLmRlbGF5PWFuZ3VsYXIuaXNVbmRlZmluZWQocy5kZWxheSk/ZjpzLmRlbGF5LHMudHlwZT1DfHxjLnR5cGV8fFwiXCIscy5wb3NpdGlvblk9cy5wb3NpdGlvblk/cy5wb3NpdGlvblk6Yy5wb3NpdGlvblkscy5wb3NpdGlvblg9cy5wb3NpdGlvblg/cy5wb3NpdGlvblg6Yy5wb3NpdGlvblgscy5yZXBsYWNlTWVzc2FnZT1zLnJlcGxhY2VNZXNzYWdlP3MucmVwbGFjZU1lc3NhZ2U6Yy5yZXBsYWNlTWVzc2FnZSxzLm9uQ2xvc2U9cy5vbkNsb3NlP3Mub25DbG9zZTpjLm9uQ2xvc2Uscy5jbG9zZU9uQ2xpY2s9bnVsbCE9PXMuY2xvc2VPbkNsaWNrJiZ2b2lkIDAhPT1zLmNsb3NlT25DbGljaz9zLmNsb3NlT25DbGljazpjLmNsb3NlT25DbGljayx0LmdldChzLnRlbXBsYXRlLHtjYWNoZTppfSkuc3VjY2VzcyhmdW5jdGlvbih0KXtmdW5jdGlvbiBpKGUpe1tcIi13ZWJraXQtdHJhbnNpdGlvblwiLFwiLW8tdHJhbnNpdGlvblwiLFwidHJhbnNpdGlvblwiXS5mb3JFYWNoKGZ1bmN0aW9uKHQpe2YuY3NzKHQsZSl9KX12YXIgbz1zLnNjb3BlLiRuZXcoKTtvLm1lc3NhZ2U9YS50cnVzdEFzSHRtbChzLm1lc3NhZ2UpLG8udGl0bGU9YS50cnVzdEFzSHRtbChzLnRpdGxlKSxvLnQ9cy50eXBlLnN1YnN0cigwLDEpLG8uZGVsYXk9cy5kZWxheSxvLm9uQ2xvc2U9cy5vbkNsb3NlO3ZhciBsPWZ1bmN0aW9uKCl7Zm9yKHZhciBlPTAsdD0wLG49cCxpPWQsbz1bXSxhPWcubGVuZ3RoLTE7YT49MDthLS0pe3ZhciBsPWdbYV07aWYocy5yZXBsYWNlTWVzc2FnZSYmYTxnLmxlbmd0aC0xKWwuYWRkQ2xhc3MoXCJraWxsZWRcIik7ZWxzZXt2YXIgcj1wYXJzZUludChsWzBdLm9mZnNldEhlaWdodCksZj1wYXJzZUludChsWzBdLm9mZnNldFdpZHRoKSxoPW9bbC5fcG9zaXRpb25ZK2wuX3Bvc2l0aW9uWF07QytyPndpbmRvdy5pbm5lckhlaWdodCYmKGg9cCx0KyssZT0wKTt2YXIgQz1uPWg/MD09PWU/aDpoK3U6cCx5PWkrdCoobStmKTtsLmNzcyhsLl9wb3NpdGlvblksQytcInB4XCIpLFwiY2VudGVyXCI9PWwuX3Bvc2l0aW9uWD9sLmNzcyhcImxlZnRcIixwYXJzZUludCh3aW5kb3cuaW5uZXJXaWR0aC8yLWYvMikrXCJweFwiKTpsLmNzcyhsLl9wb3NpdGlvblgseStcInB4XCIpLG9bbC5fcG9zaXRpb25ZK2wuX3Bvc2l0aW9uWF09QytyLGMubWF4Q291bnQ+MCYmZy5sZW5ndGg+Yy5tYXhDb3VudCYmMD09PWEmJmwuc2NvcGUoKS5raWxsKCEwKSxlKyt9fX0sZj1uKHQpKG8pO2YuX3Bvc2l0aW9uWT1zLnBvc2l0aW9uWSxmLl9wb3NpdGlvblg9cy5wb3NpdGlvblgsZi5hZGRDbGFzcyhzLnR5cGUpO3ZhciBDPWZ1bmN0aW9uKGUpe2U9ZS5vcmlnaW5hbEV2ZW50fHxlLChcImNsaWNrXCI9PT1lLnR5cGV8fFwib3BhY2l0eVwiPT09ZS5wcm9wZXJ0eU5hbWUmJmUuZWxhcHNlZFRpbWU+PTEpJiYoby5vbkNsb3NlJiZvLiRhcHBseShvLm9uQ2xvc2UoZikpLGYucmVtb3ZlKCksZy5zcGxpY2UoZy5pbmRleE9mKGYpLDEpLG8uJGRlc3Ryb3koKSxsKCkpfTtzLmNsb3NlT25DbGljayYmKGYuYWRkQ2xhc3MoXCJjbGlja2FibGVcIiksZi5iaW5kKFwiY2xpY2tcIixDKSksZi5iaW5kKFwid2Via2l0VHJhbnNpdGlvbkVuZCBvVHJhbnNpdGlvbkVuZCBvdHJhbnNpdGlvbmVuZCB0cmFuc2l0aW9uZW5kIG1zVHJhbnNpdGlvbkVuZFwiLEMpLGFuZ3VsYXIuaXNOdW1iZXIocy5kZWxheSkmJmUoZnVuY3Rpb24oKXtmLmFkZENsYXNzKFwia2lsbGVkXCIpfSxzLmRlbGF5KSxpKFwibm9uZVwiKSxhbmd1bGFyLmVsZW1lbnQoZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJib2R5XCIpKS5hcHBlbmQoZik7dmFyIHY9LShwYXJzZUludChmWzBdLm9mZnNldEhlaWdodCkrNTApO2lmKGYuY3NzKGYuX3Bvc2l0aW9uWSx2K1wicHhcIiksZy5wdXNoKGYpLFwiY2VudGVyXCI9PXMucG9zaXRpb25YKXt2YXIgaz1wYXJzZUludChmWzBdLm9mZnNldFdpZHRoKTtmLmNzcyhcImxlZnRcIixwYXJzZUludCh3aW5kb3cuaW5uZXJXaWR0aC8yLWsvMikrXCJweFwiKX1lKGZ1bmN0aW9uKCl7aShcIlwiKX0pLG8uX3RlbXBsYXRlRWxlbWVudD1mLG8ua2lsbD1mdW5jdGlvbih0KXt0PyhvLm9uQ2xvc2UmJm8uJGFwcGx5KG8ub25DbG9zZShvLl90ZW1wbGF0ZUVsZW1lbnQpKSxnLnNwbGljZShnLmluZGV4T2Yoby5fdGVtcGxhdGVFbGVtZW50KSwxKSxvLl90ZW1wbGF0ZUVsZW1lbnQucmVtb3ZlKCksby4kZGVzdHJveSgpLGUobCkpOm8uX3RlbXBsYXRlRWxlbWVudC5hZGRDbGFzcyhcImtpbGxlZFwiKX0sZShsKSxofHwoYW5ndWxhci5lbGVtZW50KHIpLmJpbmQoXCJyZXNpemVcIixmdW5jdGlvbigpe2UobCl9KSxoPSEwKSx5LnJlc29sdmUobyl9KS5lcnJvcihmdW5jdGlvbihlKXt0aHJvdyBuZXcgRXJyb3IoXCJUZW1wbGF0ZSAoXCIrcy50ZW1wbGF0ZStcIikgY291bGQgbm90IGJlIGxvYWRlZC4gXCIrZSl9KSx5LnByb21pc2V9O3JldHVybiBDLnByaW1hcnk9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMoZSxcInByaW1hcnlcIil9LEMuZXJyb3I9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMoZSxcImVycm9yXCIpfSxDLnN1Y2Nlc3M9ZnVuY3Rpb24oZSl7cmV0dXJuIHRoaXMoZSxcInN1Y2Nlc3NcIil9LEMuaW5mbz1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcyhlLFwiaW5mb1wiKX0sQy53YXJuaW5nPWZ1bmN0aW9uKGUpe3JldHVybiB0aGlzKGUsXCJ3YXJuaW5nXCIpfSxDLmNsZWFyQWxsPWZ1bmN0aW9uKCl7YW5ndWxhci5mb3JFYWNoKGcsZnVuY3Rpb24oZSl7ZS5hZGRDbGFzcyhcImtpbGxlZFwiKX0pfSxDfV19KSxhbmd1bGFyLm1vZHVsZShcInVpLW5vdGlmaWNhdGlvblwiKS5ydW4oW1wiJHRlbXBsYXRlQ2FjaGVcIixmdW5jdGlvbihlKXtlLnB1dChcImFuZ3VsYXItdWktbm90aWZpY2F0aW9uLmh0bWxcIiwnPGRpdiBjbGFzcz1cInVpLW5vdGlmaWNhdGlvblwiPjxoMyBuZy1zaG93PVwidGl0bGVcIiBuZy1iaW5kLWh0bWw9XCJ0aXRsZVwiPjwvaDM+PGRpdiBjbGFzcz1cIm1lc3NhZ2VcIiBuZy1iaW5kLWh0bWw9XCJtZXNzYWdlXCI+PC9kaXY+PC9kaXY+Jyl9XSk7IiwiLypcbiBhbmd1bGFyLWZpbGUtdXBsb2FkIHYyLjMuNFxuIGh0dHBzOi8vZ2l0aHViLmNvbS9uZXJ2Z2gvYW5ndWxhci1maWxlLXVwbG9hZFxuKi9cblxuIWZ1bmN0aW9uKGUsdCl7XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHMmJlwib2JqZWN0XCI9PXR5cGVvZiBtb2R1bGU/bW9kdWxlLmV4cG9ydHM9dCgpOlwiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW10sdCk6XCJvYmplY3RcIj09dHlwZW9mIGV4cG9ydHM/ZXhwb3J0c1tcImFuZ3VsYXItZmlsZS11cGxvYWRcIl09dCgpOmVbXCJhbmd1bGFyLWZpbGUtdXBsb2FkXCJdPXQoKX0odGhpcyxmdW5jdGlvbigpe3JldHVybiBmdW5jdGlvbihlKXtmdW5jdGlvbiB0KG4pe2lmKG9bbl0pcmV0dXJuIG9bbl0uZXhwb3J0czt2YXIgcj1vW25dPXtleHBvcnRzOnt9LGlkOm4sbG9hZGVkOiExfTtyZXR1cm4gZVtuXS5jYWxsKHIuZXhwb3J0cyxyLHIuZXhwb3J0cyx0KSxyLmxvYWRlZD0hMCxyLmV4cG9ydHN9dmFyIG89e307cmV0dXJuIHQubT1lLHQuYz1vLHQucD1cIlwiLHQoMCl9KFtmdW5jdGlvbihlLHQsbyl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbihlKXtyZXR1cm4gZSYmZS5fX2VzTW9kdWxlP2U6e1wiZGVmYXVsdFwiOmV9fXZhciByPW8oMSksaT1uKHIpLHM9bygyKSxhPW4ocyksbD1vKDMpLHU9bihsKSxwPW8oNCksYz1uKHApLGY9byg1KSxkPW4oZiksaD1vKDYpLHk9bihoKSxtPW8oNyksXz1uKG0pLHY9byg4KSxnPW4odiksRj1vKDkpLGI9bihGKSxPPW8oMTApLEM9bihPKSxJPW8oMTEpLFU9bihJKSx3PW8oMTIpLEE9bih3KTthbmd1bGFyLm1vZHVsZShpW1wiZGVmYXVsdFwiXS5uYW1lLFtdKS52YWx1ZShcImZpbGVVcGxvYWRlck9wdGlvbnNcIixhW1wiZGVmYXVsdFwiXSkuZmFjdG9yeShcIkZpbGVVcGxvYWRlclwiLHVbXCJkZWZhdWx0XCJdKS5mYWN0b3J5KFwiRmlsZUxpa2VPYmplY3RcIixjW1wiZGVmYXVsdFwiXSkuZmFjdG9yeShcIkZpbGVJdGVtXCIsZFtcImRlZmF1bHRcIl0pLmZhY3RvcnkoXCJGaWxlRGlyZWN0aXZlXCIseVtcImRlZmF1bHRcIl0pLmZhY3RvcnkoXCJGaWxlU2VsZWN0XCIsX1tcImRlZmF1bHRcIl0pLmZhY3RvcnkoXCJGaWxlRHJvcFwiLGdbXCJkZWZhdWx0XCJdKS5mYWN0b3J5KFwiRmlsZU92ZXJcIixiW1wiZGVmYXVsdFwiXSkuZGlyZWN0aXZlKFwibnZGaWxlU2VsZWN0XCIsQ1tcImRlZmF1bHRcIl0pLmRpcmVjdGl2ZShcIm52RmlsZURyb3BcIixVW1wiZGVmYXVsdFwiXSkuZGlyZWN0aXZlKFwibnZGaWxlT3ZlclwiLEFbXCJkZWZhdWx0XCJdKS5ydW4oW1wiRmlsZVVwbG9hZGVyXCIsXCJGaWxlTGlrZU9iamVjdFwiLFwiRmlsZUl0ZW1cIixcIkZpbGVEaXJlY3RpdmVcIixcIkZpbGVTZWxlY3RcIixcIkZpbGVEcm9wXCIsXCJGaWxlT3ZlclwiLGZ1bmN0aW9uKGUsdCxvLG4scixpLHMpe2UuRmlsZUxpa2VPYmplY3Q9dCxlLkZpbGVJdGVtPW8sZS5GaWxlRGlyZWN0aXZlPW4sZS5GaWxlU2VsZWN0PXIsZS5GaWxlRHJvcD1pLGUuRmlsZU92ZXI9c31dKX0sZnVuY3Rpb24oZSx0KXtlLmV4cG9ydHM9e25hbWU6XCJhbmd1bGFyRmlsZVVwbG9hZFwifX0sZnVuY3Rpb24oZSx0KXtcInVzZSBzdHJpY3RcIjtPYmplY3QuZGVmaW5lUHJvcGVydHkodCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KSx0W1wiZGVmYXVsdFwiXT17dXJsOlwiL1wiLGFsaWFzOlwiZmlsZVwiLGhlYWRlcnM6e30scXVldWU6W10scHJvZ3Jlc3M6MCxhdXRvVXBsb2FkOiExLHJlbW92ZUFmdGVyVXBsb2FkOiExLG1ldGhvZDpcIlBPU1RcIixmaWx0ZXJzOltdLGZvcm1EYXRhOltdLHF1ZXVlTGltaXQ6TnVtYmVyLk1BWF9WQUxVRSx3aXRoQ3JlZGVudGlhbHM6ITEsZGlzYWJsZU11bHRpcGFydDohMX19LGZ1bmN0aW9uKGUsdCxvKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKGUpe3JldHVybiBlJiZlLl9fZXNNb2R1bGU/ZTp7XCJkZWZhdWx0XCI6ZX19ZnVuY3Rpb24gcihlLHQpe2lmKCEoZSBpbnN0YW5jZW9mIHQpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIil9ZnVuY3Rpb24gaShlLHQsbyxuLGkscyxhKXt2YXIgbT1uLkZpbGUsXz1uLkZvcm1EYXRhLHY9ZnVuY3Rpb24oKXtmdW5jdGlvbiBuKHQpe3IodGhpcyxuKTt2YXIgbz1sKGUpO3UodGhpcyxvLHQse2lzVXBsb2FkaW5nOiExLF9uZXh0SW5kZXg6MCxfZmFpbEZpbHRlckluZGV4Oi0xLF9kaXJlY3RpdmVzOntzZWxlY3Q6W10sZHJvcDpbXSxvdmVyOltdfX0pLHRoaXMuZmlsdGVycy51bnNoaWZ0KHtuYW1lOlwicXVldWVMaW1pdFwiLGZuOnRoaXMuX3F1ZXVlTGltaXRGaWx0ZXJ9KSx0aGlzLmZpbHRlcnMudW5zaGlmdCh7bmFtZTpcImZvbGRlclwiLGZuOnRoaXMuX2ZvbGRlckZpbHRlcn0pfXJldHVybiBuLnByb3RvdHlwZS5hZGRUb1F1ZXVlPWZ1bmN0aW9uKGUsdCxvKXt2YXIgbj10aGlzLHI9dGhpcy5pc0FycmF5TGlrZU9iamVjdChlKT9lOltlXSxpPXRoaXMuX2dldEZpbHRlcnMobyksbD10aGlzLnF1ZXVlLmxlbmd0aCx1PVtdO3AocixmdW5jdGlvbihlKXt2YXIgbz1uZXcgcyhlKTtpZihuLl9pc1ZhbGlkRmlsZShvLGksdCkpe3ZhciByPW5ldyBhKG4sZSx0KTt1LnB1c2gociksbi5xdWV1ZS5wdXNoKHIpLG4uX29uQWZ0ZXJBZGRpbmdGaWxlKHIpfWVsc2V7dmFyIGw9aVtuLl9mYWlsRmlsdGVySW5kZXhdO24uX29uV2hlbkFkZGluZ0ZpbGVGYWlsZWQobyxsLHQpfX0pLHRoaXMucXVldWUubGVuZ3RoIT09bCYmKHRoaXMuX29uQWZ0ZXJBZGRpbmdBbGwodSksdGhpcy5wcm9ncmVzcz10aGlzLl9nZXRUb3RhbFByb2dyZXNzKCkpLHRoaXMuX3JlbmRlcigpLHRoaXMuYXV0b1VwbG9hZCYmdGhpcy51cGxvYWRBbGwoKX0sbi5wcm90b3R5cGUucmVtb3ZlRnJvbVF1ZXVlPWZ1bmN0aW9uKGUpe3ZhciB0PXRoaXMuZ2V0SW5kZXhPZkl0ZW0oZSksbz10aGlzLnF1ZXVlW3RdO28uaXNVcGxvYWRpbmcmJm8uY2FuY2VsKCksdGhpcy5xdWV1ZS5zcGxpY2UodCwxKSxvLl9kZXN0cm95KCksdGhpcy5wcm9ncmVzcz10aGlzLl9nZXRUb3RhbFByb2dyZXNzKCl9LG4ucHJvdG90eXBlLmNsZWFyUXVldWU9ZnVuY3Rpb24oKXtmb3IoO3RoaXMucXVldWUubGVuZ3RoOyl0aGlzLnF1ZXVlWzBdLnJlbW92ZSgpO3RoaXMucHJvZ3Jlc3M9MH0sbi5wcm90b3R5cGUudXBsb2FkSXRlbT1mdW5jdGlvbihlKXt2YXIgdD10aGlzLmdldEluZGV4T2ZJdGVtKGUpLG89dGhpcy5xdWV1ZVt0XSxuPXRoaXMuaXNIVE1MNT9cIl94aHJUcmFuc3BvcnRcIjpcIl9pZnJhbWVUcmFuc3BvcnRcIjtvLl9wcmVwYXJlVG9VcGxvYWRpbmcoKSx0aGlzLmlzVXBsb2FkaW5nfHwodGhpcy5fb25CZWZvcmVVcGxvYWRJdGVtKG8pLG8uaXNDYW5jZWx8fChvLmlzVXBsb2FkaW5nPSEwLHRoaXMuaXNVcGxvYWRpbmc9ITAsdGhpc1tuXShvKSx0aGlzLl9yZW5kZXIoKSkpfSxuLnByb3RvdHlwZS5jYW5jZWxJdGVtPWZ1bmN0aW9uKGUpe3ZhciB0PXRoaXMsbz10aGlzLmdldEluZGV4T2ZJdGVtKGUpLG49dGhpcy5xdWV1ZVtvXSxyPXRoaXMuaXNIVE1MNT9cIl94aHJcIjpcIl9mb3JtXCI7biYmKG4uaXNDYW5jZWw9ITAsbi5pc1VwbG9hZGluZz9uW3JdLmFib3J0KCk6IWZ1bmN0aW9uKCl7dmFyIGU9W3ZvaWQgMCwwLHt9XSxvPWZ1bmN0aW9uKCl7dC5fb25DYW5jZWxJdGVtLmFwcGx5KHQsW25dLmNvbmNhdChlKSksdC5fb25Db21wbGV0ZUl0ZW0uYXBwbHkodCxbbl0uY29uY2F0KGUpKX07aShvKX0oKSl9LG4ucHJvdG90eXBlLnVwbG9hZEFsbD1mdW5jdGlvbigpe3ZhciBlPXRoaXMuZ2V0Tm90VXBsb2FkZWRJdGVtcygpLmZpbHRlcihmdW5jdGlvbihlKXtyZXR1cm4hZS5pc1VwbG9hZGluZ30pO2UubGVuZ3RoJiYocChlLGZ1bmN0aW9uKGUpe3JldHVybiBlLl9wcmVwYXJlVG9VcGxvYWRpbmcoKX0pLGVbMF0udXBsb2FkKCkpfSxuLnByb3RvdHlwZS5jYW5jZWxBbGw9ZnVuY3Rpb24oKXt2YXIgZT10aGlzLmdldE5vdFVwbG9hZGVkSXRlbXMoKTtwKGUsZnVuY3Rpb24oZSl7cmV0dXJuIGUuY2FuY2VsKCl9KX0sbi5wcm90b3R5cGUuaXNGaWxlPWZ1bmN0aW9uKGUpe3JldHVybiB0aGlzLmNvbnN0cnVjdG9yLmlzRmlsZShlKX0sbi5wcm90b3R5cGUuaXNGaWxlTGlrZU9iamVjdD1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci5pc0ZpbGVMaWtlT2JqZWN0KGUpfSxuLnByb3RvdHlwZS5pc0FycmF5TGlrZU9iamVjdD1mdW5jdGlvbihlKXtyZXR1cm4gdGhpcy5jb25zdHJ1Y3Rvci5pc0FycmF5TGlrZU9iamVjdChlKX0sbi5wcm90b3R5cGUuZ2V0SW5kZXhPZkl0ZW09ZnVuY3Rpb24oZSl7cmV0dXJuIGYoZSk/ZTp0aGlzLnF1ZXVlLmluZGV4T2YoZSl9LG4ucHJvdG90eXBlLmdldE5vdFVwbG9hZGVkSXRlbXM9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5xdWV1ZS5maWx0ZXIoZnVuY3Rpb24oZSl7cmV0dXJuIWUuaXNVcGxvYWRlZH0pfSxuLnByb3RvdHlwZS5nZXRSZWFkeUl0ZW1zPWZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMucXVldWUuZmlsdGVyKGZ1bmN0aW9uKGUpe3JldHVybiBlLmlzUmVhZHkmJiFlLmlzVXBsb2FkaW5nfSkuc29ydChmdW5jdGlvbihlLHQpe3JldHVybiBlLmluZGV4LXQuaW5kZXh9KX0sbi5wcm90b3R5cGUuZGVzdHJveT1mdW5jdGlvbigpe3ZhciBlPXRoaXM7cCh0aGlzLl9kaXJlY3RpdmVzLGZ1bmN0aW9uKHQpe3AoZS5fZGlyZWN0aXZlc1t0XSxmdW5jdGlvbihlKXtlLmRlc3Ryb3koKX0pfSl9LG4ucHJvdG90eXBlLm9uQWZ0ZXJBZGRpbmdBbGw9ZnVuY3Rpb24oZSl7fSxuLnByb3RvdHlwZS5vbkFmdGVyQWRkaW5nRmlsZT1mdW5jdGlvbihlKXt9LG4ucHJvdG90eXBlLm9uV2hlbkFkZGluZ0ZpbGVGYWlsZWQ9ZnVuY3Rpb24oZSx0LG8pe30sbi5wcm90b3R5cGUub25CZWZvcmVVcGxvYWRJdGVtPWZ1bmN0aW9uKGUpe30sbi5wcm90b3R5cGUub25Qcm9ncmVzc0l0ZW09ZnVuY3Rpb24oZSx0KXt9LG4ucHJvdG90eXBlLm9uUHJvZ3Jlc3NBbGw9ZnVuY3Rpb24oZSl7fSxuLnByb3RvdHlwZS5vblN1Y2Nlc3NJdGVtPWZ1bmN0aW9uKGUsdCxvLG4pe30sbi5wcm90b3R5cGUub25FcnJvckl0ZW09ZnVuY3Rpb24oZSx0LG8sbil7fSxuLnByb3RvdHlwZS5vbkNhbmNlbEl0ZW09ZnVuY3Rpb24oZSx0LG8sbil7fSxuLnByb3RvdHlwZS5vbkNvbXBsZXRlSXRlbT1mdW5jdGlvbihlLHQsbyxuKXt9LG4ucHJvdG90eXBlLm9uQ29tcGxldGVBbGw9ZnVuY3Rpb24oKXt9LG4ucHJvdG90eXBlLl9nZXRUb3RhbFByb2dyZXNzPWZ1bmN0aW9uKGUpe2lmKHRoaXMucmVtb3ZlQWZ0ZXJVcGxvYWQpcmV0dXJuIGV8fDA7dmFyIHQ9dGhpcy5nZXROb3RVcGxvYWRlZEl0ZW1zKCkubGVuZ3RoLG89dD90aGlzLnF1ZXVlLmxlbmd0aC10OnRoaXMucXVldWUubGVuZ3RoLG49MTAwL3RoaXMucXVldWUubGVuZ3RoLHI9KGV8fDApKm4vMTAwO3JldHVybiBNYXRoLnJvdW5kKG8qbityKX0sbi5wcm90b3R5cGUuX2dldEZpbHRlcnM9ZnVuY3Rpb24oZSl7aWYoIWUpcmV0dXJuIHRoaXMuZmlsdGVycztpZihoKGUpKXJldHVybiBlO3ZhciB0PWUubWF0Y2goL1teXFxzLF0rL2cpO3JldHVybiB0aGlzLmZpbHRlcnMuZmlsdGVyKGZ1bmN0aW9uKGUpe3JldHVybi0xIT09dC5pbmRleE9mKGUubmFtZSl9KX0sbi5wcm90b3R5cGUuX3JlbmRlcj1mdW5jdGlvbigpe3QuJCRwaGFzZXx8dC4kYXBwbHkoKX0sbi5wcm90b3R5cGUuX2ZvbGRlckZpbHRlcj1mdW5jdGlvbihlKXtyZXR1cm4hKCFlLnNpemUmJiFlLnR5cGUpfSxuLnByb3RvdHlwZS5fcXVldWVMaW1pdEZpbHRlcj1mdW5jdGlvbigpe3JldHVybiB0aGlzLnF1ZXVlLmxlbmd0aDx0aGlzLnF1ZXVlTGltaXR9LG4ucHJvdG90eXBlLl9pc1ZhbGlkRmlsZT1mdW5jdGlvbihlLHQsbyl7dmFyIG49dGhpcztyZXR1cm4gdGhpcy5fZmFpbEZpbHRlckluZGV4PS0xLHQubGVuZ3RoP3QuZXZlcnkoZnVuY3Rpb24odCl7cmV0dXJuIG4uX2ZhaWxGaWx0ZXJJbmRleCsrLHQuZm4uY2FsbChuLGUsbyl9KTohMH0sbi5wcm90b3R5cGUuX2lzU3VjY2Vzc0NvZGU9ZnVuY3Rpb24oZSl7cmV0dXJuIGU+PTIwMCYmMzAwPmV8fDMwND09PWV9LG4ucHJvdG90eXBlLl90cmFuc2Zvcm1SZXNwb25zZT1mdW5jdGlvbihlLHQpe3ZhciBuPXRoaXMuX2hlYWRlcnNHZXR0ZXIodCk7cmV0dXJuIHAoby5kZWZhdWx0cy50cmFuc2Zvcm1SZXNwb25zZSxmdW5jdGlvbih0KXtlPXQoZSxuKX0pLGV9LG4ucHJvdG90eXBlLl9wYXJzZUhlYWRlcnM9ZnVuY3Rpb24oZSl7dmFyIHQsbyxuLHI9e307cmV0dXJuIGU/KHAoZS5zcGxpdChcIlxcblwiKSxmdW5jdGlvbihlKXtuPWUuaW5kZXhPZihcIjpcIiksdD1lLnNsaWNlKDAsbikudHJpbSgpLnRvTG93ZXJDYXNlKCksbz1lLnNsaWNlKG4rMSkudHJpbSgpLHQmJihyW3RdPXJbdF0/clt0XStcIiwgXCIrbzpvKX0pLHIpOnJ9LG4ucHJvdG90eXBlLl9oZWFkZXJzR2V0dGVyPWZ1bmN0aW9uKGUpe3JldHVybiBmdW5jdGlvbih0KXtyZXR1cm4gdD9lW3QudG9Mb3dlckNhc2UoKV18fG51bGw6ZX19LG4ucHJvdG90eXBlLl94aHJUcmFuc3BvcnQ9ZnVuY3Rpb24oZSl7dmFyIHQsbz10aGlzLG49ZS5feGhyPW5ldyBYTUxIdHRwUmVxdWVzdDtpZihlLmRpc2FibGVNdWx0aXBhcnQ/dD1lLl9maWxlOih0PW5ldyBfLHAoZS5mb3JtRGF0YSxmdW5jdGlvbihlKXtwKGUsZnVuY3Rpb24oZSxvKXt0LmFwcGVuZChvLGUpfSl9KSx0LmFwcGVuZChlLmFsaWFzLGUuX2ZpbGUsZS5maWxlLm5hbWUpKSxcIm51bWJlclwiIT10eXBlb2YgZS5fZmlsZS5zaXplKXRocm93IG5ldyBUeXBlRXJyb3IoXCJUaGUgZmlsZSBzcGVjaWZpZWQgaXMgbm8gbG9uZ2VyIHZhbGlkXCIpO24udXBsb2FkLm9ucHJvZ3Jlc3M9ZnVuY3Rpb24odCl7dmFyIG49TWF0aC5yb3VuZCh0Lmxlbmd0aENvbXB1dGFibGU/MTAwKnQubG9hZGVkL3QudG90YWw6MCk7by5fb25Qcm9ncmVzc0l0ZW0oZSxuKX0sbi5vbmxvYWQ9ZnVuY3Rpb24oKXt2YXIgdD1vLl9wYXJzZUhlYWRlcnMobi5nZXRBbGxSZXNwb25zZUhlYWRlcnMoKSkscj1vLl90cmFuc2Zvcm1SZXNwb25zZShuLnJlc3BvbnNlLHQpLGk9by5faXNTdWNjZXNzQ29kZShuLnN0YXR1cyk/XCJTdWNjZXNzXCI6XCJFcnJvclwiLHM9XCJfb25cIitpK1wiSXRlbVwiO29bc10oZSxyLG4uc3RhdHVzLHQpLG8uX29uQ29tcGxldGVJdGVtKGUscixuLnN0YXR1cyx0KX0sbi5vbmVycm9yPWZ1bmN0aW9uKCl7dmFyIHQ9by5fcGFyc2VIZWFkZXJzKG4uZ2V0QWxsUmVzcG9uc2VIZWFkZXJzKCkpLHI9by5fdHJhbnNmb3JtUmVzcG9uc2Uobi5yZXNwb25zZSx0KTtvLl9vbkVycm9ySXRlbShlLHIsbi5zdGF0dXMsdCksby5fb25Db21wbGV0ZUl0ZW0oZSxyLG4uc3RhdHVzLHQpfSxuLm9uYWJvcnQ9ZnVuY3Rpb24oKXt2YXIgdD1vLl9wYXJzZUhlYWRlcnMobi5nZXRBbGxSZXNwb25zZUhlYWRlcnMoKSkscj1vLl90cmFuc2Zvcm1SZXNwb25zZShuLnJlc3BvbnNlLHQpO28uX29uQ2FuY2VsSXRlbShlLHIsbi5zdGF0dXMsdCksby5fb25Db21wbGV0ZUl0ZW0oZSxyLG4uc3RhdHVzLHQpfSxuLm9wZW4oZS5tZXRob2QsZS51cmwsITApLG4ud2l0aENyZWRlbnRpYWxzPWUud2l0aENyZWRlbnRpYWxzLHAoZS5oZWFkZXJzLGZ1bmN0aW9uKGUsdCl7bi5zZXRSZXF1ZXN0SGVhZGVyKHQsZSl9KSxuLnNlbmQodCl9LG4ucHJvdG90eXBlLl9pZnJhbWVUcmFuc3BvcnQ9ZnVuY3Rpb24oZSl7dmFyIHQ9dGhpcyxvPXkoJzxmb3JtIHN0eWxlPVwiZGlzcGxheTogbm9uZTtcIiAvPicpLG49eSgnPGlmcmFtZSBuYW1lPVwiaWZyYW1lVHJhbnNwb3J0JytEYXRlLm5vdygpKydcIj4nKSxyPWUuX2lucHV0O2UuX2Zvcm0mJmUuX2Zvcm0ucmVwbGFjZVdpdGgociksZS5fZm9ybT1vLHIucHJvcChcIm5hbWVcIixlLmFsaWFzKSxwKGUuZm9ybURhdGEsZnVuY3Rpb24oZSl7cChlLGZ1bmN0aW9uKGUsdCl7dmFyIG49eSgnPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBuYW1lPVwiJyt0KydcIiAvPicpO24udmFsKGUpLG8uYXBwZW5kKG4pfSl9KSxvLnByb3Aoe2FjdGlvbjplLnVybCxtZXRob2Q6XCJQT1NUXCIsdGFyZ2V0Om4ucHJvcChcIm5hbWVcIiksZW5jdHlwZTpcIm11bHRpcGFydC9mb3JtLWRhdGFcIixlbmNvZGluZzpcIm11bHRpcGFydC9mb3JtLWRhdGFcIn0pLG4uYmluZChcImxvYWRcIixmdW5jdGlvbigpe3ZhciBvPVwiXCIscj0yMDA7dHJ5e289blswXS5jb250ZW50RG9jdW1lbnQuYm9keS5pbm5lckhUTUx9Y2F0Y2goaSl7cj01MDB9dmFyIHM9e3Jlc3BvbnNlOm8sc3RhdHVzOnIsZHVtbXk6ITB9LGE9e30sbD10Ll90cmFuc2Zvcm1SZXNwb25zZShzLnJlc3BvbnNlLGEpO3QuX29uU3VjY2Vzc0l0ZW0oZSxsLHMuc3RhdHVzLGEpLHQuX29uQ29tcGxldGVJdGVtKGUsbCxzLnN0YXR1cyxhKX0pLG8uYWJvcnQ9ZnVuY3Rpb24oKXt2YXIgaSxzPXtzdGF0dXM6MCxkdW1teTohMH0sYT17fTtuLnVuYmluZChcImxvYWRcIikucHJvcChcInNyY1wiLFwiamF2YXNjcmlwdDpmYWxzZTtcIiksby5yZXBsYWNlV2l0aChyKSx0Ll9vbkNhbmNlbEl0ZW0oZSxpLHMuc3RhdHVzLGEpLHQuX29uQ29tcGxldGVJdGVtKGUsaSxzLnN0YXR1cyxhKX0sci5hZnRlcihvKSxvLmFwcGVuZChyKS5hcHBlbmQobiksb1swXS5zdWJtaXQoKX0sbi5wcm90b3R5cGUuX29uV2hlbkFkZGluZ0ZpbGVGYWlsZWQ9ZnVuY3Rpb24oZSx0LG8pe3RoaXMub25XaGVuQWRkaW5nRmlsZUZhaWxlZChlLHQsbyl9LG4ucHJvdG90eXBlLl9vbkFmdGVyQWRkaW5nRmlsZT1mdW5jdGlvbihlKXt0aGlzLm9uQWZ0ZXJBZGRpbmdGaWxlKGUpfSxuLnByb3RvdHlwZS5fb25BZnRlckFkZGluZ0FsbD1mdW5jdGlvbihlKXt0aGlzLm9uQWZ0ZXJBZGRpbmdBbGwoZSl9LG4ucHJvdG90eXBlLl9vbkJlZm9yZVVwbG9hZEl0ZW09ZnVuY3Rpb24oZSl7ZS5fb25CZWZvcmVVcGxvYWQoKSx0aGlzLm9uQmVmb3JlVXBsb2FkSXRlbShlKX0sbi5wcm90b3R5cGUuX29uUHJvZ3Jlc3NJdGVtPWZ1bmN0aW9uKGUsdCl7dmFyIG89dGhpcy5fZ2V0VG90YWxQcm9ncmVzcyh0KTt0aGlzLnByb2dyZXNzPW8sZS5fb25Qcm9ncmVzcyh0KSx0aGlzLm9uUHJvZ3Jlc3NJdGVtKGUsdCksdGhpcy5vblByb2dyZXNzQWxsKG8pLHRoaXMuX3JlbmRlcigpfSxuLnByb3RvdHlwZS5fb25TdWNjZXNzSXRlbT1mdW5jdGlvbihlLHQsbyxuKXtlLl9vblN1Y2Nlc3ModCxvLG4pLHRoaXMub25TdWNjZXNzSXRlbShlLHQsbyxuKX0sbi5wcm90b3R5cGUuX29uRXJyb3JJdGVtPWZ1bmN0aW9uKGUsdCxvLG4pe2UuX29uRXJyb3IodCxvLG4pLHRoaXMub25FcnJvckl0ZW0oZSx0LG8sbil9LG4ucHJvdG90eXBlLl9vbkNhbmNlbEl0ZW09ZnVuY3Rpb24oZSx0LG8sbil7ZS5fb25DYW5jZWwodCxvLG4pLHRoaXMub25DYW5jZWxJdGVtKGUsdCxvLG4pfSxuLnByb3RvdHlwZS5fb25Db21wbGV0ZUl0ZW09ZnVuY3Rpb24oZSx0LG8sbil7ZS5fb25Db21wbGV0ZSh0LG8sbiksdGhpcy5vbkNvbXBsZXRlSXRlbShlLHQsbyxuKTt2YXIgcj10aGlzLmdldFJlYWR5SXRlbXMoKVswXTtyZXR1cm4gdGhpcy5pc1VwbG9hZGluZz0hMSxkKHIpP3ZvaWQgci51cGxvYWQoKToodGhpcy5vbkNvbXBsZXRlQWxsKCksdGhpcy5wcm9ncmVzcz10aGlzLl9nZXRUb3RhbFByb2dyZXNzKCksdm9pZCB0aGlzLl9yZW5kZXIoKSl9LG4uaXNGaWxlPWZ1bmN0aW9uKGUpe3JldHVybiBtJiZlIGluc3RhbmNlb2YgbX0sbi5pc0ZpbGVMaWtlT2JqZWN0PWZ1bmN0aW9uKGUpe3JldHVybiBlIGluc3RhbmNlb2Ygc30sbi5pc0FycmF5TGlrZU9iamVjdD1mdW5jdGlvbihlKXtyZXR1cm4gYyhlKSYmXCJsZW5ndGhcImluIGV9LG4uaW5oZXJpdD1mdW5jdGlvbihlLHQpe2UucHJvdG90eXBlPU9iamVjdC5jcmVhdGUodC5wcm90b3R5cGUpLGUucHJvdG90eXBlLmNvbnN0cnVjdG9yPWUsZS5zdXBlcl89dH0sbn0oKTtyZXR1cm4gdi5wcm90b3R5cGUuaXNIVE1MNT0hKCFtfHwhXyksdi5pc0hUTUw1PXYucHJvdG90eXBlLmlzSFRNTDUsdn1PYmplY3QuZGVmaW5lUHJvcGVydHkodCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KSx0W1wiZGVmYXVsdFwiXT1pO3ZhciBzPW8oMSksYT0obihzKSxhbmd1bGFyKSxsPWEuY29weSx1PWEuZXh0ZW5kLHA9YS5mb3JFYWNoLGM9YS5pc09iamVjdCxmPWEuaXNOdW1iZXIsZD1hLmlzRGVmaW5lZCxoPWEuaXNBcnJheSx5PWEuZWxlbWVudDtpLiRpbmplY3Q9W1wiZmlsZVVwbG9hZGVyT3B0aW9uc1wiLFwiJHJvb3RTY29wZVwiLFwiJGh0dHBcIixcIiR3aW5kb3dcIixcIiR0aW1lb3V0XCIsXCJGaWxlTGlrZU9iamVjdFwiLFwiRmlsZUl0ZW1cIl19LGZ1bmN0aW9uKGUsdCxvKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKGUpe3JldHVybiBlJiZlLl9fZXNNb2R1bGU/ZTp7XCJkZWZhdWx0XCI6ZX19ZnVuY3Rpb24gcihlLHQpe2lmKCEoZSBpbnN0YW5jZW9mIHQpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIil9ZnVuY3Rpb24gaSgpe3JldHVybiBmdW5jdGlvbigpe2Z1bmN0aW9uIGUodCl7cih0aGlzLGUpO3ZhciBvPXUodCksbj1vP3QudmFsdWU6dCxpPXAobik/XCJGYWtlUGF0aFwiOlwiT2JqZWN0XCIscz1cIl9jcmVhdGVGcm9tXCIraTt0aGlzW3NdKG4pfXJldHVybiBlLnByb3RvdHlwZS5fY3JlYXRlRnJvbUZha2VQYXRoPWZ1bmN0aW9uKGUpe3RoaXMubGFzdE1vZGlmaWVkRGF0ZT1udWxsLHRoaXMuc2l6ZT1udWxsLHRoaXMudHlwZT1cImxpa2UvXCIrZS5zbGljZShlLmxhc3RJbmRleE9mKFwiLlwiKSsxKS50b0xvd2VyQ2FzZSgpLHRoaXMubmFtZT1lLnNsaWNlKGUubGFzdEluZGV4T2YoXCIvXCIpK2UubGFzdEluZGV4T2YoXCJcXFxcXCIpKzIpfSxlLnByb3RvdHlwZS5fY3JlYXRlRnJvbU9iamVjdD1mdW5jdGlvbihlKXt0aGlzLmxhc3RNb2RpZmllZERhdGU9bChlLmxhc3RNb2RpZmllZERhdGUpLHRoaXMuc2l6ZT1lLnNpemUsdGhpcy50eXBlPWUudHlwZSx0aGlzLm5hbWU9ZS5uYW1lfSxlfSgpfU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pLHRbXCJkZWZhdWx0XCJdPWk7dmFyIHM9bygxKSxhPShuKHMpLGFuZ3VsYXIpLGw9YS5jb3B5LHU9YS5pc0VsZW1lbnQscD1hLmlzU3RyaW5nfSxmdW5jdGlvbihlLHQsbyl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbihlKXtyZXR1cm4gZSYmZS5fX2VzTW9kdWxlP2U6e1wiZGVmYXVsdFwiOmV9fWZ1bmN0aW9uIHIoZSx0KXtpZighKGUgaW5zdGFuY2VvZiB0KSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpfWZ1bmN0aW9uIGkoZSx0KXtyZXR1cm4gZnVuY3Rpb24oKXtmdW5jdGlvbiBvKGUsbixpKXtyKHRoaXMsbyk7dmFyIHM9YyhuKSxhPXM/cChuKTpudWxsLGY9cz9udWxsOm47dSh0aGlzLHt1cmw6ZS51cmwsYWxpYXM6ZS5hbGlhcyxoZWFkZXJzOmwoZS5oZWFkZXJzKSxmb3JtRGF0YTpsKGUuZm9ybURhdGEpLHJlbW92ZUFmdGVyVXBsb2FkOmUucmVtb3ZlQWZ0ZXJVcGxvYWQsd2l0aENyZWRlbnRpYWxzOmUud2l0aENyZWRlbnRpYWxzLGRpc2FibGVNdWx0aXBhcnQ6ZS5kaXNhYmxlTXVsdGlwYXJ0LG1ldGhvZDplLm1ldGhvZH0saSx7dXBsb2FkZXI6ZSxmaWxlOm5ldyB0KG4pLGlzUmVhZHk6ITEsaXNVcGxvYWRpbmc6ITEsaXNVcGxvYWRlZDohMSxpc1N1Y2Nlc3M6ITEsaXNDYW5jZWw6ITEsaXNFcnJvcjohMSxwcm9ncmVzczowLGluZGV4Om51bGwsX2ZpbGU6ZixfaW5wdXQ6YX0pLGEmJnRoaXMuX3JlcGxhY2VOb2RlKGEpfXJldHVybiBvLnByb3RvdHlwZS51cGxvYWQ9ZnVuY3Rpb24oKXt0cnl7dGhpcy51cGxvYWRlci51cGxvYWRJdGVtKHRoaXMpfWNhdGNoKGUpe3RoaXMudXBsb2FkZXIuX29uQ29tcGxldGVJdGVtKHRoaXMsXCJcIiwwLFtdKSx0aGlzLnVwbG9hZGVyLl9vbkVycm9ySXRlbSh0aGlzLFwiXCIsMCxbXSl9fSxvLnByb3RvdHlwZS5jYW5jZWw9ZnVuY3Rpb24oKXt0aGlzLnVwbG9hZGVyLmNhbmNlbEl0ZW0odGhpcyl9LG8ucHJvdG90eXBlLnJlbW92ZT1mdW5jdGlvbigpe3RoaXMudXBsb2FkZXIucmVtb3ZlRnJvbVF1ZXVlKHRoaXMpfSxvLnByb3RvdHlwZS5vbkJlZm9yZVVwbG9hZD1mdW5jdGlvbigpe30sby5wcm90b3R5cGUub25Qcm9ncmVzcz1mdW5jdGlvbihlKXt9LG8ucHJvdG90eXBlLm9uU3VjY2Vzcz1mdW5jdGlvbihlLHQsbyl7fSxvLnByb3RvdHlwZS5vbkVycm9yPWZ1bmN0aW9uKGUsdCxvKXt9LG8ucHJvdG90eXBlLm9uQ2FuY2VsPWZ1bmN0aW9uKGUsdCxvKXt9LG8ucHJvdG90eXBlLm9uQ29tcGxldGU9ZnVuY3Rpb24oZSx0LG8pe30sby5wcm90b3R5cGUuX29uQmVmb3JlVXBsb2FkPWZ1bmN0aW9uKCl7dGhpcy5pc1JlYWR5PSEwLHRoaXMuaXNVcGxvYWRpbmc9ITEsdGhpcy5pc1VwbG9hZGVkPSExLHRoaXMuaXNTdWNjZXNzPSExLHRoaXMuaXNDYW5jZWw9ITEsdGhpcy5pc0Vycm9yPSExLHRoaXMucHJvZ3Jlc3M9MCx0aGlzLm9uQmVmb3JlVXBsb2FkKCl9LG8ucHJvdG90eXBlLl9vblByb2dyZXNzPWZ1bmN0aW9uKGUpe3RoaXMucHJvZ3Jlc3M9ZSx0aGlzLm9uUHJvZ3Jlc3MoZSl9LG8ucHJvdG90eXBlLl9vblN1Y2Nlc3M9ZnVuY3Rpb24oZSx0LG8pe3RoaXMuaXNSZWFkeT0hMSx0aGlzLmlzVXBsb2FkaW5nPSExLHRoaXMuaXNVcGxvYWRlZD0hMCx0aGlzLmlzU3VjY2Vzcz0hMCx0aGlzLmlzQ2FuY2VsPSExLHRoaXMuaXNFcnJvcj0hMSx0aGlzLnByb2dyZXNzPTEwMCx0aGlzLmluZGV4PW51bGwsdGhpcy5vblN1Y2Nlc3MoZSx0LG8pfSxvLnByb3RvdHlwZS5fb25FcnJvcj1mdW5jdGlvbihlLHQsbyl7dGhpcy5pc1JlYWR5PSExLHRoaXMuaXNVcGxvYWRpbmc9ITEsdGhpcy5pc1VwbG9hZGVkPSEwLHRoaXMuaXNTdWNjZXNzPSExLHRoaXMuaXNDYW5jZWw9ITEsdGhpcy5pc0Vycm9yPSEwLHRoaXMucHJvZ3Jlc3M9MCx0aGlzLmluZGV4PW51bGwsdGhpcy5vbkVycm9yKGUsdCxvKX0sby5wcm90b3R5cGUuX29uQ2FuY2VsPWZ1bmN0aW9uKGUsdCxvKXt0aGlzLmlzUmVhZHk9ITEsdGhpcy5pc1VwbG9hZGluZz0hMSx0aGlzLmlzVXBsb2FkZWQ9ITEsdGhpcy5pc1N1Y2Nlc3M9ITEsdGhpcy5pc0NhbmNlbD0hMCx0aGlzLmlzRXJyb3I9ITEsdGhpcy5wcm9ncmVzcz0wLHRoaXMuaW5kZXg9bnVsbCx0aGlzLm9uQ2FuY2VsKGUsdCxvKX0sby5wcm90b3R5cGUuX29uQ29tcGxldGU9ZnVuY3Rpb24oZSx0LG8pe3RoaXMub25Db21wbGV0ZShlLHQsbyksdGhpcy5yZW1vdmVBZnRlclVwbG9hZCYmdGhpcy5yZW1vdmUoKX0sby5wcm90b3R5cGUuX2Rlc3Ryb3k9ZnVuY3Rpb24oKXt0aGlzLl9pbnB1dCYmdGhpcy5faW5wdXQucmVtb3ZlKCksdGhpcy5fZm9ybSYmdGhpcy5fZm9ybS5yZW1vdmUoKSxkZWxldGUgdGhpcy5fZm9ybSxkZWxldGUgdGhpcy5faW5wdXR9LG8ucHJvdG90eXBlLl9wcmVwYXJlVG9VcGxvYWRpbmc9ZnVuY3Rpb24oKXt0aGlzLmluZGV4PXRoaXMuaW5kZXh8fCsrdGhpcy51cGxvYWRlci5fbmV4dEluZGV4LHRoaXMuaXNSZWFkeT0hMH0sby5wcm90b3R5cGUuX3JlcGxhY2VOb2RlPWZ1bmN0aW9uKHQpe3ZhciBvPWUodC5jbG9uZSgpKSh0LnNjb3BlKCkpO28ucHJvcChcInZhbHVlXCIsbnVsbCksdC5jc3MoXCJkaXNwbGF5XCIsXCJub25lXCIpLHQuYWZ0ZXIobyl9LG99KCl9T2JqZWN0LmRlZmluZVByb3BlcnR5KHQsXCJfX2VzTW9kdWxlXCIse3ZhbHVlOiEwfSksdFtcImRlZmF1bHRcIl09aTt2YXIgcz1vKDEpLGE9KG4ocyksYW5ndWxhciksbD1hLmNvcHksdT1hLmV4dGVuZCxwPWEuZWxlbWVudCxjPWEuaXNFbGVtZW50O2kuJGluamVjdD1bXCIkY29tcGlsZVwiLFwiRmlsZUxpa2VPYmplY3RcIl19LGZ1bmN0aW9uKGUsdCxvKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKGUpe3JldHVybiBlJiZlLl9fZXNNb2R1bGU/ZTp7XCJkZWZhdWx0XCI6ZX19ZnVuY3Rpb24gcihlLHQpe2lmKCEoZSBpbnN0YW5jZW9mIHQpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIil9ZnVuY3Rpb24gaSgpe3ZhciBlPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gZSh0KXtyKHRoaXMsZSksbCh0aGlzLHQpLHRoaXMudXBsb2FkZXIuX2RpcmVjdGl2ZXNbdGhpcy5wcm9wXS5wdXNoKHRoaXMpLHRoaXMuX3NhdmVMaW5rcygpLHRoaXMuYmluZCgpfXJldHVybiBlLnByb3RvdHlwZS5iaW5kPWZ1bmN0aW9uKCl7Zm9yKHZhciBlIGluIHRoaXMuZXZlbnRzKXt2YXIgdD10aGlzLmV2ZW50c1tlXTt0aGlzLmVsZW1lbnQuYmluZChlLHRoaXNbdF0pfX0sZS5wcm90b3R5cGUudW5iaW5kPWZ1bmN0aW9uKCl7Zm9yKHZhciBlIGluIHRoaXMuZXZlbnRzKXRoaXMuZWxlbWVudC51bmJpbmQoZSx0aGlzLmV2ZW50c1tlXSl9LGUucHJvdG90eXBlLmRlc3Ryb3k9ZnVuY3Rpb24oKXt2YXIgZT10aGlzLnVwbG9hZGVyLl9kaXJlY3RpdmVzW3RoaXMucHJvcF0uaW5kZXhPZih0aGlzKTt0aGlzLnVwbG9hZGVyLl9kaXJlY3RpdmVzW3RoaXMucHJvcF0uc3BsaWNlKGUsMSksdGhpcy51bmJpbmQoKX0sZS5wcm90b3R5cGUuX3NhdmVMaW5rcz1mdW5jdGlvbigpe2Zvcih2YXIgZSBpbiB0aGlzLmV2ZW50cyl7dmFyIHQ9dGhpcy5ldmVudHNbZV07dGhpc1t0XT10aGlzW3RdLmJpbmQodGhpcyl9fSxlfSgpO3JldHVybiBlLnByb3RvdHlwZS5ldmVudHM9e30sZX1PYmplY3QuZGVmaW5lUHJvcGVydHkodCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KSx0W1wiZGVmYXVsdFwiXT1pO3ZhciBzPW8oMSksYT0obihzKSxhbmd1bGFyKSxsPWEuZXh0ZW5kfSxmdW5jdGlvbihlLHQsbyl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbihlKXtyZXR1cm4gZSYmZS5fX2VzTW9kdWxlP2U6e1wiZGVmYXVsdFwiOmV9fWZ1bmN0aW9uIHIoZSx0KXtpZighKGUgaW5zdGFuY2VvZiB0KSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpfWZ1bmN0aW9uIGkoZSx0KXtpZighZSl0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7cmV0dXJuIXR8fFwib2JqZWN0XCIhPXR5cGVvZiB0JiZcImZ1bmN0aW9uXCIhPXR5cGVvZiB0P2U6dH1mdW5jdGlvbiBzKGUsdCl7aWYoXCJmdW5jdGlvblwiIT10eXBlb2YgdCYmbnVsbCE9PXQpdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIrdHlwZW9mIHQpO2UucHJvdG90eXBlPU9iamVjdC5jcmVhdGUodCYmdC5wcm90b3R5cGUse2NvbnN0cnVjdG9yOnt2YWx1ZTplLGVudW1lcmFibGU6ITEsd3JpdGFibGU6ITAsY29uZmlndXJhYmxlOiEwfX0pLHQmJihPYmplY3Quc2V0UHJvdG90eXBlT2Y/T2JqZWN0LnNldFByb3RvdHlwZU9mKGUsdCk6ZS5fX3Byb3RvX189dCl9ZnVuY3Rpb24gYShlLHQpe3JldHVybiBmdW5jdGlvbih0KXtmdW5jdGlvbiBvKGUpe3IodGhpcyxvKTt2YXIgbj1wKGUse2V2ZW50czp7JGRlc3Ryb3k6XCJkZXN0cm95XCIsY2hhbmdlOlwib25DaGFuZ2VcIn0scHJvcDpcInNlbGVjdFwifSkscz1pKHRoaXMsdC5jYWxsKHRoaXMsbikpO3JldHVybiBzLnVwbG9hZGVyLmlzSFRNTDV8fHMuZWxlbWVudC5yZW1vdmVBdHRyKFwibXVsdGlwbGVcIikscy5lbGVtZW50LnByb3AoXCJ2YWx1ZVwiLG51bGwpLHN9cmV0dXJuIHMobyx0KSxvLnByb3RvdHlwZS5nZXRPcHRpb25zPWZ1bmN0aW9uKCl7fSxvLnByb3RvdHlwZS5nZXRGaWx0ZXJzPWZ1bmN0aW9uKCl7fSxvLnByb3RvdHlwZS5pc0VtcHR5QWZ0ZXJTZWxlY3Rpb249ZnVuY3Rpb24oKXtyZXR1cm4hIXRoaXMuZWxlbWVudC5hdHRyKFwibXVsdGlwbGVcIil9LG8ucHJvdG90eXBlLm9uQ2hhbmdlPWZ1bmN0aW9uKCl7dmFyIHQ9dGhpcy51cGxvYWRlci5pc0hUTUw1P3RoaXMuZWxlbWVudFswXS5maWxlczp0aGlzLmVsZW1lbnRbMF0sbz10aGlzLmdldE9wdGlvbnMoKSxuPXRoaXMuZ2V0RmlsdGVycygpO3RoaXMudXBsb2FkZXIuaXNIVE1MNXx8dGhpcy5kZXN0cm95KCksdGhpcy51cGxvYWRlci5hZGRUb1F1ZXVlKHQsbyxuKSx0aGlzLmlzRW1wdHlBZnRlclNlbGVjdGlvbigpJiYodGhpcy5lbGVtZW50LnByb3AoXCJ2YWx1ZVwiLG51bGwpLHRoaXMuZWxlbWVudC5yZXBsYWNlV2l0aChlKHRoaXMuZWxlbWVudC5jbG9uZSgpKSh0aGlzLnNjb3BlKSkpfSxvfSh0KX1PYmplY3QuZGVmaW5lUHJvcGVydHkodCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KSx0W1wiZGVmYXVsdFwiXT1hO3ZhciBsPW8oMSksdT0obihsKSxhbmd1bGFyKSxwPXUuZXh0ZW5kO2EuJGluamVjdD1bXCIkY29tcGlsZVwiLFwiRmlsZURpcmVjdGl2ZVwiXX0sZnVuY3Rpb24oZSx0LG8pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIG4oZSl7cmV0dXJuIGUmJmUuX19lc01vZHVsZT9lOntcImRlZmF1bHRcIjplfX1mdW5jdGlvbiByKGUsdCl7aWYoIShlIGluc3RhbmNlb2YgdCkpdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKX1mdW5jdGlvbiBpKGUsdCl7aWYoIWUpdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpO3JldHVybiF0fHxcIm9iamVjdFwiIT10eXBlb2YgdCYmXCJmdW5jdGlvblwiIT10eXBlb2YgdD9lOnR9ZnVuY3Rpb24gcyhlLHQpe2lmKFwiZnVuY3Rpb25cIiE9dHlwZW9mIHQmJm51bGwhPT10KXRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiK3R5cGVvZiB0KTtlLnByb3RvdHlwZT1PYmplY3QuY3JlYXRlKHQmJnQucHJvdG90eXBlLHtjb25zdHJ1Y3Rvcjp7dmFsdWU6ZSxlbnVtZXJhYmxlOiExLHdyaXRhYmxlOiEwLGNvbmZpZ3VyYWJsZTohMH19KSx0JiYoT2JqZWN0LnNldFByb3RvdHlwZU9mP09iamVjdC5zZXRQcm90b3R5cGVPZihlLHQpOmUuX19wcm90b19fPXQpfWZ1bmN0aW9uIGEoZSl7cmV0dXJuIGZ1bmN0aW9uKGUpe2Z1bmN0aW9uIHQobyl7cih0aGlzLHQpO3ZhciBuPXAobyx7ZXZlbnRzOnskZGVzdHJveTpcImRlc3Ryb3lcIixkcm9wOlwib25Ecm9wXCIsZHJhZ292ZXI6XCJvbkRyYWdPdmVyXCIsZHJhZ2xlYXZlOlwib25EcmFnTGVhdmVcIn0scHJvcDpcImRyb3BcIn0pO3JldHVybiBpKHRoaXMsZS5jYWxsKHRoaXMsbikpfXJldHVybiBzKHQsZSksdC5wcm90b3R5cGUuZ2V0T3B0aW9ucz1mdW5jdGlvbigpe30sdC5wcm90b3R5cGUuZ2V0RmlsdGVycz1mdW5jdGlvbigpe30sdC5wcm90b3R5cGUub25Ecm9wPWZ1bmN0aW9uKGUpe3ZhciB0PXRoaXMuX2dldFRyYW5zZmVyKGUpO2lmKHQpe3ZhciBvPXRoaXMuZ2V0T3B0aW9ucygpLG49dGhpcy5nZXRGaWx0ZXJzKCk7dGhpcy5fcHJldmVudEFuZFN0b3AoZSksYyh0aGlzLnVwbG9hZGVyLl9kaXJlY3RpdmVzLm92ZXIsdGhpcy5fcmVtb3ZlT3ZlckNsYXNzLHRoaXMpLHRoaXMudXBsb2FkZXIuYWRkVG9RdWV1ZSh0LmZpbGVzLG8sbil9fSx0LnByb3RvdHlwZS5vbkRyYWdPdmVyPWZ1bmN0aW9uKGUpe3ZhciB0PXRoaXMuX2dldFRyYW5zZmVyKGUpO3RoaXMuX2hhdmVGaWxlcyh0LnR5cGVzKSYmKHQuZHJvcEVmZmVjdD1cImNvcHlcIix0aGlzLl9wcmV2ZW50QW5kU3RvcChlKSxjKHRoaXMudXBsb2FkZXIuX2RpcmVjdGl2ZXMub3Zlcix0aGlzLl9hZGRPdmVyQ2xhc3MsdGhpcykpfSx0LnByb3RvdHlwZS5vbkRyYWdMZWF2ZT1mdW5jdGlvbihlKXtlLmN1cnJlbnRUYXJnZXQhPT10aGlzLmVsZW1lbnRbMF0mJih0aGlzLl9wcmV2ZW50QW5kU3RvcChlKSxjKHRoaXMudXBsb2FkZXIuX2RpcmVjdGl2ZXMub3Zlcix0aGlzLl9yZW1vdmVPdmVyQ2xhc3MsdGhpcykpfSx0LnByb3RvdHlwZS5fZ2V0VHJhbnNmZXI9ZnVuY3Rpb24oZSl7cmV0dXJuIGUuZGF0YVRyYW5zZmVyP2UuZGF0YVRyYW5zZmVyOmUub3JpZ2luYWxFdmVudC5kYXRhVHJhbnNmZXJ9LHQucHJvdG90eXBlLl9wcmV2ZW50QW5kU3RvcD1mdW5jdGlvbihlKXtlLnByZXZlbnREZWZhdWx0KCksZS5zdG9wUHJvcGFnYXRpb24oKX0sdC5wcm90b3R5cGUuX2hhdmVGaWxlcz1mdW5jdGlvbihlKXtyZXR1cm4gZT9lLmluZGV4T2Y/LTEhPT1lLmluZGV4T2YoXCJGaWxlc1wiKTplLmNvbnRhaW5zP2UuY29udGFpbnMoXCJGaWxlc1wiKTohMTohMX0sdC5wcm90b3R5cGUuX2FkZE92ZXJDbGFzcz1mdW5jdGlvbihlKXtlLmFkZE92ZXJDbGFzcygpfSx0LnByb3RvdHlwZS5fcmVtb3ZlT3ZlckNsYXNzPWZ1bmN0aW9uKGUpe2UucmVtb3ZlT3ZlckNsYXNzKCl9LHR9KGUpfU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pLHRbXCJkZWZhdWx0XCJdPWE7dmFyIGw9bygxKSx1PShuKGwpLGFuZ3VsYXIpLHA9dS5leHRlbmQsYz11LmZvckVhY2g7YS4kaW5qZWN0PVtcIkZpbGVEaXJlY3RpdmVcIl19LGZ1bmN0aW9uKGUsdCxvKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKGUpe3JldHVybiBlJiZlLl9fZXNNb2R1bGU/ZTp7XCJkZWZhdWx0XCI6ZX19ZnVuY3Rpb24gcihlLHQpe2lmKCEoZSBpbnN0YW5jZW9mIHQpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIil9ZnVuY3Rpb24gaShlLHQpe2lmKCFlKXRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTtyZXR1cm4hdHx8XCJvYmplY3RcIiE9dHlwZW9mIHQmJlwiZnVuY3Rpb25cIiE9dHlwZW9mIHQ/ZTp0fWZ1bmN0aW9uIHMoZSx0KXtpZihcImZ1bmN0aW9uXCIhPXR5cGVvZiB0JiZudWxsIT09dCl0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIit0eXBlb2YgdCk7ZS5wcm90b3R5cGU9T2JqZWN0LmNyZWF0ZSh0JiZ0LnByb3RvdHlwZSx7Y29uc3RydWN0b3I6e3ZhbHVlOmUsZW51bWVyYWJsZTohMSx3cml0YWJsZTohMCxjb25maWd1cmFibGU6ITB9fSksdCYmKE9iamVjdC5zZXRQcm90b3R5cGVPZj9PYmplY3Quc2V0UHJvdG90eXBlT2YoZSx0KTplLl9fcHJvdG9fXz10KX1mdW5jdGlvbiBhKGUpe3JldHVybiBmdW5jdGlvbihlKXtmdW5jdGlvbiB0KG8pe3IodGhpcyx0KTt2YXIgbj1wKG8se2V2ZW50czp7JGRlc3Ryb3k6XCJkZXN0cm95XCJ9LHByb3A6XCJvdmVyXCIsb3ZlckNsYXNzOlwibnYtZmlsZS1vdmVyXCJ9KTtyZXR1cm4gaSh0aGlzLGUuY2FsbCh0aGlzLG4pKX1yZXR1cm4gcyh0LGUpLHQucHJvdG90eXBlLmFkZE92ZXJDbGFzcz1mdW5jdGlvbigpe3RoaXMuZWxlbWVudC5hZGRDbGFzcyh0aGlzLmdldE92ZXJDbGFzcygpKX0sdC5wcm90b3R5cGUucmVtb3ZlT3ZlckNsYXNzPWZ1bmN0aW9uKCl7dGhpcy5lbGVtZW50LnJlbW92ZUNsYXNzKHRoaXMuZ2V0T3ZlckNsYXNzKCkpfSx0LnByb3RvdHlwZS5nZXRPdmVyQ2xhc3M9ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5vdmVyQ2xhc3N9LHR9KGUpfU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pLHRbXCJkZWZhdWx0XCJdPWE7dmFyIGw9bygxKSx1PShuKGwpLGFuZ3VsYXIpLHA9dS5leHRlbmQ7YS4kaW5qZWN0PVtcIkZpbGVEaXJlY3RpdmVcIl19LGZ1bmN0aW9uKGUsdCxvKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKGUpe3JldHVybiBlJiZlLl9fZXNNb2R1bGU/ZTp7XCJkZWZhdWx0XCI6ZX19ZnVuY3Rpb24gcihlLHQsbyl7cmV0dXJue2xpbms6ZnVuY3Rpb24obixyLGkpe3ZhciBzPW4uJGV2YWwoaS51cGxvYWRlcik7aWYoIShzIGluc3RhbmNlb2YgdCkpdGhyb3cgbmV3IFR5cGVFcnJvcignXCJVcGxvYWRlclwiIG11c3QgYmUgYW4gaW5zdGFuY2Ugb2YgRmlsZVVwbG9hZGVyJyk7dmFyIGE9bmV3IG8oe3VwbG9hZGVyOnMsZWxlbWVudDpyLHNjb3BlOm59KTthLmdldE9wdGlvbnM9ZShpLm9wdGlvbnMpLmJpbmQoYSxuKSxhLmdldEZpbHRlcnM9ZnVuY3Rpb24oKXtyZXR1cm4gaS5maWx0ZXJzfX19fU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pLHRbXCJkZWZhdWx0XCJdPXI7dmFyIGk9bygxKTtuKGkpO3IuJGluamVjdD1bXCIkcGFyc2VcIixcIkZpbGVVcGxvYWRlclwiLFwiRmlsZVNlbGVjdFwiXX0sZnVuY3Rpb24oZSx0LG8pe1widXNlIHN0cmljdFwiO2Z1bmN0aW9uIG4oZSl7cmV0dXJuIGUmJmUuX19lc01vZHVsZT9lOntcImRlZmF1bHRcIjplfX1mdW5jdGlvbiByKGUsdCxvKXtyZXR1cm57bGluazpmdW5jdGlvbihuLHIsaSl7dmFyIHM9bi4kZXZhbChpLnVwbG9hZGVyKTtpZighKHMgaW5zdGFuY2VvZiB0KSl0aHJvdyBuZXcgVHlwZUVycm9yKCdcIlVwbG9hZGVyXCIgbXVzdCBiZSBhbiBpbnN0YW5jZSBvZiBGaWxlVXBsb2FkZXInKTtpZihzLmlzSFRNTDUpe3ZhciBhPW5ldyBvKHt1cGxvYWRlcjpzLGVsZW1lbnQ6cn0pO2EuZ2V0T3B0aW9ucz1lKGkub3B0aW9ucykuYmluZChhLG4pLGEuZ2V0RmlsdGVycz1mdW5jdGlvbigpe3JldHVybiBpLmZpbHRlcnN9fX19fU9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0LFwiX19lc01vZHVsZVwiLHt2YWx1ZTohMH0pLHRbXCJkZWZhdWx0XCJdPXI7dmFyIGk9bygxKTtuKGkpO3IuJGluamVjdD1bXCIkcGFyc2VcIixcIkZpbGVVcGxvYWRlclwiLFwiRmlsZURyb3BcIl19LGZ1bmN0aW9uKGUsdCxvKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBuKGUpe3JldHVybiBlJiZlLl9fZXNNb2R1bGU/ZTp7XCJkZWZhdWx0XCI6ZX19ZnVuY3Rpb24gcihlLHQpe3JldHVybntsaW5rOmZ1bmN0aW9uKG8sbixyKXt2YXIgaT1vLiRldmFsKHIudXBsb2FkZXIpO2lmKCEoaSBpbnN0YW5jZW9mIGUpKXRocm93IG5ldyBUeXBlRXJyb3IoJ1wiVXBsb2FkZXJcIiBtdXN0IGJlIGFuIGluc3RhbmNlIG9mIEZpbGVVcGxvYWRlcicpO3ZhciBzPW5ldyB0KHt1cGxvYWRlcjppLGVsZW1lbnQ6bn0pO3MuZ2V0T3ZlckNsYXNzPWZ1bmN0aW9uKCl7cmV0dXJuIHIub3ZlckNsYXNzfHxzLm92ZXJDbGFzc319fX1PYmplY3QuZGVmaW5lUHJvcGVydHkodCxcIl9fZXNNb2R1bGVcIix7dmFsdWU6ITB9KSx0W1wiZGVmYXVsdFwiXT1yO3ZhciBpPW8oMSk7bihpKTtyLiRpbmplY3Q9W1wiRmlsZVVwbG9hZGVyXCIsXCJGaWxlT3ZlclwiXX1dKX0pO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YW5ndWxhci1maWxlLXVwbG9hZC5taW4uanMubWFwIiwiKGZ1bmN0aW9uKCkge1xuXG5cdHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgncHJvZHVjdFBhZ2UnLCBbJ3VpLW5vdGlmaWNhdGlvbicsICdhbmd1bGFyRmlsZVVwbG9hZCddKTtcblxuICAgIGFwcC5jb250cm9sbGVyKCdQcm9kdWN0UGFnZUNvbnRyb2xsZXInLCBbJ25nRGlhbG9nJywgJ05vdGlmaWNhdGlvbicsICdwcm9kdWN0c0xpc3QnLCAnRmlsZVVwbG9hZGVyJywgJyRyb290U2NvcGUnLCBmdW5jdGlvbihuZ0RpYWxvZywgTm90aWZpY2F0aW9uLCBwcm9kdWN0c0xpc3QsIEZpbGVVcGxvYWRlciwgJHJvb3RTY29wZSkge1xuXG5cdFx0JHJvb3RTY29wZS4kb24oJ25nRGlhbG9nLm9wZW5lZCcsIGZ1bmN0aW9uIChlLCAkZGlhbG9nKSB7XG4gICAgXHRcdCQoJ3NlbGVjdCcpLm1hdGVyaWFsX3NlbGVjdCgpO1xuXHRcdFx0TWF0ZXJpYWxpemUudXBkYXRlVGV4dEZpZWxkcygpO1xuXHRcdH0pO1xuXG5cdFx0dGhpcy51cGxvYWRlciA9IG5ldyBGaWxlVXBsb2FkZXIoeyB1cmw6ICcvY29ubmVjdHMvdXBsb2FkLnBocCcgfSk7XG5cbiAgICAgICAgdGhpcy51cGxvYWRlci5vblN1Y2Nlc3NJdGVtID0gZnVuY3Rpb24oZmlsZUl0ZW0sIHJlc3BvbnNlLCBzdGF0dXMsIGhlYWRlcnMpIHtcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbygnb25TdWNjZXNzSXRlbScsIGZpbGVJdGVtLCByZXNwb25zZSwgc3RhdHVzLCBoZWFkZXJzKTtcbiAgICAgICAgfTtcblxuXHRcdHRoaXMucmVtb3ZlID0gZnVuY3Rpb24oZXZlbnQsIHByb2R1Y3QsIHZpZGVvSW5kZXgpIHtcblx0XHRcdHByb2R1Y3RzTGlzdFtwcm9kdWN0LmluZGV4XS52aWRlby5zcGxpY2UodmlkZW9JbmRleCwgMSlcblx0XHRcdC8vIGRlbGV0ZSBwcm9kdWN0c0xpc3RbcHJvZHVjdC5pbmRleF0udmlkZW9bdmlkZW9JbmRleF07XG5cdFx0XHQvLyBldmVudC5zcmNFbGVtZW50LnBhcmVudE5vZGUuc3R5bGUuZGlzcGxheSA9ICdub25lJztcblx0XHRcdC8vIHByb2R1Y3RzTGlzdFtwcm9kdWN0LmluZGV4XS52aWRlby5sZW5ndGggPSBwcm9kdWN0c0xpc3RbcHJvZHVjdC5pbmRleF0udmlkZW8ubGVuZ3RoIC0gMTtcblx0XHRcdC8vIGNvbnNvbGUubG9nKGV2ZW50LCBwcm9kdWN0LCB2aWRlb0luZGV4KTtcblx0XHRcdGNvbnNvbGUubG9nKHByb2R1Y3RzTGlzdCk7XG5cdFx0fVxuXG5cdFx0dGhpcy5kZWxldGVWaWRlbyA9IGZ1bmN0aW9uKGluZGV4LCBwcm9kdWN0SW5kZXgpIHtcblx0XHRcdGNvbnNvbGUubG9nKGluZGV4LCBwcm9kdWN0SW5kZXgpO1xuXHRcdH1cblxuXHRcdHRoaXMuc2F2ZVByb2R1Y3QgPSBmdW5jdGlvbihwcm9kdWN0LCBxdWUpIHtcblxuXHRcdFx0aWYgKHF1ZS5sZW5ndGggPT0gMCkge1xuXG5cdFx0XHRcdGNvbnNvbGUubG9nKCdJbWFnZSBub3QgdXBsb2FkJyk7XG5cdFx0XHRcdHByb2R1Y3RzTGlzdFtwcm9kdWN0LmluZGV4XS5pbWFnZSA9ICd2aWRlbyc7XG5cblx0XHRcdH1cblxuXHRcdFx0dGhpcy51cGxvYWRlci51cGxvYWRBbGwoKTtcblxuXHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBxdWUubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0Y29uc29sZS5sb2cocXVlKTtcblx0XHRcdFx0cHJvZHVjdHNMaXN0W3Byb2R1Y3QuaW5kZXhdLmltYWdlID0gcXVlWzBdLmZpbGUubmFtZTtcblx0XHRcdH07XG5cblxuXHRcdFx0Ly8gY29uc29sZS5sb2cocHJvZHVjdC5pbmRleCwgcXVlWzBdLmZpbGUubmFtZSk7XG5cdFx0ICAgIE5vdGlmaWNhdGlvbi5zdWNjZXNzKCfQotC+0LLQsNGAIFwiJyArIHByb2R1Y3QucHJvZHVjdC5uYW1lICsnXCIg0LjQt9C80LXQvdC10L0nKTtcblx0XHR9O1xuXG4gICAgfV0pO1xuXG59KSgpO1xuIiwiKGZ1bmN0aW9uKCkge1xuXG5cdHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgncHJvZHVjdExpc3QnLCBbJ25nRGlhbG9nJywgJ3VpLW5vdGlmaWNhdGlvbiddKTtcblxuICAgIGFwcC5jb250cm9sbGVyKCdQcm9kdWN0Q29udHJvbGxlcicsIFsnJGh0dHAnLCAnbmdEaWFsb2cnLCAnTm90aWZpY2F0aW9uJywgJ3Byb2R1Y3RzTGlzdCcsIGZ1bmN0aW9uKCRodHRwLCBuZ0RpYWxvZywgTm90aWZpY2F0aW9uLCBwcm9kdWN0c0xpc3QpIHtcblxuXHRcdHZhciBzdG9yZSA9IHRoaXM7XG5cdFx0c3RvcmUucHJvZHVjdHMgPSBwcm9kdWN0c0xpc3Q7XG5cblx0XHQvLyAkaHR0cC5wb3N0KCcvY29ubmVjdHMvY29ubmVjdHMucGhwJywge1xuXHRcdC8vIFx0dHlwZTogJ3JlYWQnLFxuXHRcdC8vIFx0aXRlbTogJ3Byb2R1Y3QnXG5cdFx0Ly8gXHR9KS5zdWNjZXNzKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cdFx0Ly8gXHRzdG9yZS5wcm9kdWN0cyA9IHJlc3BvbnNlO1xuXHRcdC8vIFx0Y29uc29sZS5sb2cocmVzcG9uc2UpO1xuXHRcdC8vIH0pO1xuXG5cdFx0Ly8gdmFyIGhhbmRsZVN1Y2Nlc3MgPSBmdW5jdGlvbihkYXRhLCBzdGF0dXMpIHtcblx0XHQvLyBcdHN0b3JlLnByb2R1Y3RzID0gZGF0YTtcbiAgLy8gIFx0XHR9O1xuXHRcdC8vXG5cdFx0Ly8gcHJvZHVjdHNMaXN0LmdldFNlc3Npb25zKCkuc3VjY2VzcyhoYW5kbGVTdWNjZXNzKTtcblxuICAgICAgICB0aGlzLnJlbW92ZVByb2R1Y3QgPSBmdW5jdGlvbihpbmRleCkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgncHJvZHVjdExpc3RfX2l0ZW0nKVtpbmRleF0uc3R5bGUuZGlzcGxheT0nbm9uZSc7XG4gICAgICAgICAgICBkZWxldGUgcHJvZHVjdHNMaXN0W2luZGV4XTtcblx0XHRcdGNvbnNvbGUubG9nKHByb2R1Y3RzTGlzdCk7XG4gICAgICAgIH1cblxuXHRcdHRoaXMub3BlbkVkaXRQYWdlID0gZnVuY3Rpb24oaW5kZXgpIHtcblxuXHRcdFx0Y29uc29sZS5sb2coaW5kZXgpO1xuXG5cdFx0XHRuZ0RpYWxvZy5vcGVuKHtcblx0XHRcdFx0dGVtcGxhdGU6ICdwcm9kdWN0RWRpdFBhZ2UnLFxuXHRcdFx0XHRjbGFzc05hbWU6ICduZ2RpYWxvZy10aGVtZS1wbGFpbicsXG4gICAgICAgICAgICAgICAgZGF0YToge1xuXHRcdFx0XHRcdHByb2R1Y3Q6IHN0b3JlLnByb2R1Y3RzW2luZGV4XSxcblx0XHRcdFx0XHRpbmRleDogaW5kZXhcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cblx0XHR9XG5cblx0XHR0aGlzLmFkZFByb2R1Y3QgPSBmdW5jdGlvbigpIHtcblx0XHRcdHN0b3JlLnByb2R1Y3RzW3N0b3JlLnByb2R1Y3RzLmxlbmd0aF0gPSB7XG5cdFx0XHRcdG5hbWU6IFwi0J3QvtCy0YvQuSDRgtC+0LLQsNGAXCIsXG5cdFx0ICAgICAgICBkZXNjcmlwdGlvbjogXCLQntC/0LjRgdCw0L3QuNC1INGC0L7QstCw0YDQsC5cIixcblx0XHQgICAgICAgIHByaWNlOiBcIjEwMFwiLFxuXHRcdCAgICAgICAgaW1hZ2U6IFwiaW1nL3NlcnZlci5zdmdcIixcblx0XHQgICAgICAgIHZpZGVvOiBcIlwiXG5cdFx0XHR9XG5cblx0XHRcdGNvbnNvbGUubG9nKHN0b3JlLnByb2R1Y3RzKTtcblx0XHR9XG5cblx0XHR0aGlzLnNhdmUgPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0Ly8gcHJvZHVjdC5wcm9kdWN0LnZpZGVvID0gcHJvZHVjdC5wcm9kdWN0LnZpZGVvLmpvaW4oJywnKTtcblxuXHRcdFx0dmFyIGNoYW5nZWRMaXN0ID0ge307XG5cblx0XHRcdGZvciAodmFyIGkgPSAxOyBpIDwgc3RvcmUucHJvZHVjdHMubGVuZ3RoICsgMTsgaSsrKSB7XG5cdFx0XHRcdGNoYW5nZWRMaXN0LnR5cGUgPSAnd3JpdGUnO1xuXHRcdFx0XHRjaGFuZ2VkTGlzdC5pdGVtID0gJ3Byb2R1Y3QnO1xuXHRcdFx0XHRjaGFuZ2VkTGlzdFsncHJvZHVjdCcgKyBpXSA9IHN0b3JlLnByb2R1Y3RzW2kgLSAxXTtcblxuXHRcdFx0XHRpZiAoY2hhbmdlZExpc3RbJ3Byb2R1Y3QnICsgaV0gPT09IHRydWUpIHtcblxuXHRcdFx0XHRcdGNoYW5nZWRMaXN0Wydwcm9kdWN0JyArIGldLnZpZGVvID0gY2hhbmdlZExpc3RbJ3Byb2R1Y3QnICsgaV0udmlkZW8uam9pbignLCcpO1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGNoYW5nZWRMaXN0Wydwcm9kdWN0JyArIGldKTtcblxuXHRcdFx0XHR9XG5cblx0XHRcdFx0ZGVsZXRlIGNoYW5nZWRMaXN0LmlkO1xuXHRcdFx0fVxuXG5cdFx0XHRjb25zb2xlLmxvZyhjaGFuZ2VkTGlzdCk7XG5cblx0XHRcdCRodHRwLnBvc3QoJy9jb25uZWN0cy9jb25uZWN0cy5waHAnLCBjaGFuZ2VkTGlzdCkuc3VjY2VzcyhmdW5jdGlvbihyZXNwb25zZSkge1xuXG5cdFx0XHRcdE5vdGlmaWNhdGlvbi5zdWNjZXNzKHJlc3BvbnNlKTtcblxuXHRcdFx0XHQvLyBsb2NhdGlvbi5yZWxvYWQoKTtcblxuXHRcdFx0IH0pO1xuXG5cdFx0fVxuXG4gICAgfV0pO1xuXG59KSgpO1xuIiwiKGZ1bmN0aW9uKCkge1xuXG5cdHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnc3RvcmVJbmZvJywgWyd1aS1ub3RpZmljYXRpb24nLCAnYW5ndWxhckZpbGVVcGxvYWQnXSk7XG5cbiAgICBhcHAuY29udHJvbGxlcignU3RvcmVJbmZvQ29udHJvbGxlcicsIFsnbmdEaWFsb2cnLCAnTm90aWZpY2F0aW9uJywgJyRodHRwJywgJ0ZpbGVVcGxvYWRlcicsIGZ1bmN0aW9uKG5nRGlhbG9nLCBOb3RpZmljYXRpb24sICRodHRwLCBGaWxlVXBsb2FkZXIpIHtcblxuXHRcdHRoaXMudXBsb2FkZXJPbmUgPSBuZXcgRmlsZVVwbG9hZGVyKHsgdXJsOiAnL2Nvbm5lY3RzL3VwbG9hZC5waHAnIH0pO1xuXHRcdHRoaXMudXBsb2FkZXJUd28gPSBuZXcgRmlsZVVwbG9hZGVyKHsgdXJsOiAnL2Nvbm5lY3RzL3VwbG9hZC5waHAnIH0pO1xuXG5cdFx0dGhpcy51cGxvYWRlck9uZS5vblN1Y2Nlc3NJdGVtID0gZnVuY3Rpb24oZmlsZUl0ZW0sIHJlc3BvbnNlLCBzdGF0dXMsIGhlYWRlcnMpIHtcblx0XHRcdGNvbnNvbGUuaW5mbygnb25TdWNjZXNzSXRlbScsIGZpbGVJdGVtLCByZXNwb25zZSwgc3RhdHVzLCBoZWFkZXJzKTtcblx0XHR9O1xuXG5cdFx0dGhpcy51cGxvYWRlclR3by5vblN1Y2Nlc3NJdGVtID0gZnVuY3Rpb24oZmlsZUl0ZW0sIHJlc3BvbnNlLCBzdGF0dXMsIGhlYWRlcnMpIHtcblx0XHRcdGNvbnNvbGUuaW5mbygnb25TdWNjZXNzSXRlbScsIGZpbGVJdGVtLCByZXNwb25zZSwgc3RhdHVzLCBoZWFkZXJzKTtcblx0XHR9O1xuXG4gICAgICAgIHZhciBpbmZvID0gZnVuY3Rpb24oKSB7XG5cblx0XHRcdHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuXHRcdFx0dmFyIGJvZHkgPSAndHlwZT1yZWFkJml0ZW09Y29uZmlncyc7XG5cblx0XHRcdHhoci5vcGVuKFwiUE9TVFwiLCAnL2Nvbm5lY3RzL2Nvbm5lY3RzLnBocCcsIGZhbHNlKVxuXHRcdFx0eGhyLnNldFJlcXVlc3RIZWFkZXIoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKVxuXG5cdFx0XHR4aHIuc2VuZChib2R5KTtcblxuXHRcdFx0dmFyIGluZm8gPSBKU09OLnBhcnNlKHhoci5yZXNwb25zZVRleHQpO1xuXG5cdFx0XHRyZXR1cm4gaW5mbztcblxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5pbmZvID0gaW5mbygpO1xuXG5cdFx0dGhpcy5zYXZlID0gZnVuY3Rpb24oaW5mbywgaW1nMSwgaW1nMikge1xuXG5cdFx0XHRjb25zb2xlLmxvZyhpbWcxLmxlbmd0aCwgaW1nMi5sZW5ndGgpO1xuXG5cdFx0XHRpZiAoaW1nMS5sZW5ndGggIT0gMCAmJiBpbWcyLmxlbmd0aCAhPSAwKSB7XG5cblx0XHRcdFx0JGh0dHAucG9zdCgnL2Nvbm5lY3RzL2Nvbm5lY3RzLnBocCcsIHtcblx0XHRcdFx0XHR0eXBlOiAnd3JpdGUnLFxuXHRcdFx0XHRcdGl0ZW06ICdjb25maWdzJyxcblx0XHRcdFx0XHRjb25maWdzMToge1xuXHRcdFx0XHRcdFx0bmFtZTogJ3RleHQxJyxcblx0XHRcdFx0XHRcdGNvbnRlbnQ6IGluZm9bMF0uY29udGVudFxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0Y29uZmlnczI6IHtcblx0XHRcdFx0XHRcdG5hbWU6ICd0ZXh0MicsXG5cdFx0XHRcdFx0XHRjb250ZW50OiBpbmZvWzFdLmNvbnRlbnRcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdGNvbmZpZ3MzOiB7XG5cdFx0XHRcdFx0XHRuYW1lOiAndGV4dDMnLFxuXHRcdFx0XHRcdFx0Y29udGVudDogaW5mb1syXS5jb250ZW50XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRjb25maWdzNDoge1xuXHRcdFx0XHRcdFx0bmFtZTogJ3RleHQ0Jyxcblx0XHRcdFx0XHRcdGNvbnRlbnQ6IGluZm9bM10uY29udGVudFxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0Y29uZmlnczU6IHtcblx0XHRcdFx0XHRcdG5hbWU6ICd0ZXh0NScsXG5cdFx0XHRcdFx0XHRjb250ZW50OiBpbmZvWzRdLmNvbnRlbnRcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdGNvbmZpZ3M2OiB7XG5cdFx0XHRcdFx0XHRuYW1lOiAndGV4dDYnLFxuXHRcdFx0XHRcdFx0Y29udGVudDogaW5mb1s1XS5jb250ZW50XG5cdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRjb25maWdzNzoge1xuXHRcdFx0XHRcdFx0bmFtZTogJ3RleHQ3Jyxcblx0XHRcdFx0XHRcdGNvbnRlbnQ6IGluZm9bNl0uY29udGVudFxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0Y29uZmlnczg6IHtcblx0XHRcdFx0XHRcdG5hbWU6ICd0ZXh0OCcsXG5cdFx0XHRcdFx0XHRjb250ZW50OiBpbmZvWzddLmNvbnRlbnRcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdGNvbmZpZ3M5OiB7XG5cdFx0XHRcdFx0XHRuYW1lOiAnaW1hZ2VQYXRoMScsXG5cdFx0XHRcdFx0XHRjb250ZW50OiBpbWcxWzBdLmZpbGUubmFtZVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0Y29uZmlnczEwOiB7XG5cdFx0XHRcdFx0XHRuYW1lOiAnaW1hZ2VQYXRoMicsXG5cdFx0XHRcdFx0XHRjb250ZW50OiBpbWcyWzBdLmZpbGUubmFtZVxuXHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0Y29uZmlnczExOiB7XG5cdFx0XHRcdFx0XHRuYW1lOiAnaGVyb1ZpZGVvJyxcblx0XHRcdFx0XHRcdGNvbnRlbnQ6IGluZm9bMTBdLmNvbnRlbnQgfHwgdW5kZWZpbmVkXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KS5zdWNjZXNzKGZ1bmN0aW9uIChlKSB7XG5cdFx0XHRcdFx0Tm90aWZpY2F0aW9uLnN1Y2Nlc3MoJ9Ch0L7RhdGA0LDQvdC10L3QvicpO1xuXHRcdFx0XHR9KTtcblxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Tm90aWZpY2F0aW9uLmVycm9yKCfQndC1INCy0YHQtSDQv9C+0LvRjyDQt9Cw0L/QvtC70L3QtdC90YsnKTtcblx0XHRcdH1cblxuXG5cdFx0fVxuXG4gICAgfV0pO1xuXG59KSgpO1xuIiwiKGZ1bmN0aW9uKCkge1xuXG5cdHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnbWV0YUluZm8nLCBbJ3VpLW5vdGlmaWNhdGlvbiddKTtcblxuICAgIGFwcC5jb250cm9sbGVyKCdNZXRhSW5mb0NvbnRyb2xsZXInLCBbJ25nRGlhbG9nJywgJ05vdGlmaWNhdGlvbicsICckaHR0cCcsIGZ1bmN0aW9uKG5nRGlhbG9nLCBOb3RpZmljYXRpb24sICRodHRwKSB7XG5cblx0XHR2YXIgbWV0YUluZm8gPSBmdW5jdGlvbigpIHtcblxuXHRcdFx0dmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG5cdFx0XHR2YXIgYm9keSA9ICd0eXBlPXJlYWQmaXRlbT1tZXRhJztcblxuXHRcdFx0eGhyLm9wZW4oXCJQT1NUXCIsICcvY29ubmVjdHMvY29ubmVjdHMucGhwJywgZmFsc2UpXG5cdFx0XHR4aHIuc2V0UmVxdWVzdEhlYWRlcignQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCcpXG5cblx0XHRcdHhoci5zZW5kKGJvZHkpO1xuXG5cdFx0XHR2YXIgbWV0YSA9IEpTT04ucGFyc2UoeGhyLnJlc3BvbnNlVGV4dCk7XG5cblx0XHRcdHJldHVybiBtZXRhO1xuXG5cdFx0fTtcblxuXHRcdHRoaXMubWV0YUluZm8gPSBtZXRhSW5mbygpO1xuXG5cdFx0dGhpcy5zYXZlID0gZnVuY3Rpb24oaW5mbykge1xuXG5cdFx0XHRjb25zb2xlLmxvZyhpbmZvKTtcblxuXHRcdFx0JGh0dHAucG9zdCgnL2Nvbm5lY3RzL2Nvbm5lY3RzLnBocCcsIHtcblxuICAgICAgICAgICAgICAgIHR5cGU6ICd3cml0ZScsXG4gICAgICAgICAgICAgICAgaXRlbTogJ21ldGEnLFxuXHRcdFx0XHR0aXRsZTogZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3RpdGxlJykudmFsdWUsXG5cdFx0XHRcdGRlc2NyaXB0aW9uOiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbWV0YURlc2NyaXB0aW9uJykudmFsdWUsXG5cdFx0XHRcdGtleXdvcmRzOiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbWV0YVRhZ3MnKS52YWx1ZVxuXG5cdFx0XHRcdH0pLnN1Y2Nlc3MoZnVuY3Rpb24ocmVzcG9uc2UpIHtcblxuXHRcdFx0XHROb3RpZmljYXRpb24uc3VjY2Vzcygn0KHQvtGF0YDQsNC90LXQvdC+Jyk7XG5cbiAgICAgICAgICAgIH0pO1xuXG5cdFx0fTtcblxuICAgIH1dKTtcblxufSkoKTtcbiIsIihmdW5jdGlvbigpIHtcblxuXHR2YXIgYXBwID0gYW5ndWxhci5tb2R1bGUoJ2FkbWluTG9naW4nLCBbXSk7XG5cbiAgICBhcHAuY29udHJvbGxlcignQWRtaW5Mb2dpbkNvbnRyb2xsZXInLCBbJyRodHRwJywgJ05vdGlmaWNhdGlvbicsIGZ1bmN0aW9uKCRodHRwLCBOb3RpZmljYXRpb24pIHtcblxuICAgICAgICB0aGlzLnN1Ym1pdCA9IGZ1bmN0aW9uKCkge1xuXG5cbiAgICAgICAgICAgICRodHRwLnBvc3QoJy9jb25uZWN0cy9jb25uZWN0cy5waHAnLCB7XG4gICAgICAgICAgICAgICAgdHlwZTogJ2FkbWxvZ2luJyxcbiAgICAgICAgICAgICAgICBsb2dpbjogZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2FkbWluTG9naW4nKS52YWx1ZSxcbiAgICAgICAgICAgICAgICBwYXNzOiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYWRtaW5QYXNzJykudmFsdWVcbiAgICAgICAgICAgICAgICB9KS5zdWNjZXNzKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG5cbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UucmVwbGFjZSgvXFxuJC9tLCAnJykgPT0gJ29rJykge1xuICAgICAgICAgICAgICAgICAgICBsb2NhdGlvbi5yZWxvYWQoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKHJlc3BvbnNlLCAnd3JvbmcnKTtcblx0XHRcdFx0XHRjb25zb2xlLmxvZyhyZXNwb25zZSA9PSAnb2smIzAxMCcsIHJlc3BvbnNlID09ICdvaycpO1xuXHRcdFx0XHRcdE5vdGlmaWNhdGlvbi5lcnJvcih7bWVzc2FnZTogcmVzcG9uc2UsIGRlbGF5OiAxMDAwfSk7XG4gICAgICAgICAgICAgICAgICAgIGxvY2F0aW9uLnJlbG9hZCgpO1xuXHRcdFx0XHR9XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB9XG5cblxuICAgIH1dKTtcblxuXG59KSgpO1xuIiwiKGZ1bmN0aW9uKCkge1xuXG5cdHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgncHJvZHVjdENvdmVyJywgW10pO1xuXG4gICAgYXBwLmNvbnRyb2xsZXIoJ3Byb2R1Y3RDb3ZlckNvbnRyb2xsZXInLCBbJyRzY2UnLCAnZ2V0WW90dWJlVmlkZW9JRCcsIGZ1bmN0aW9uKCRzY2UsIGdldFlvdHViZVZpZGVvSUQpIHtcblxuICAgICAgICB0aGlzLmlzVmlkZW8gPSBmYWxzZTtcbiAgICAgICAgdGhpcy5pc0ltYWdlID0gdHJ1ZTtcblxuICAgICAgICB0aGlzLmNoYW5nZVR5cGUgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgIHN3aXRjaCAodGhpcy5wcm9kdWN0Q292ZXJUeXBlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnaW1hZ2UnOlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzSW1hZ2UgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzVmlkZW8gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAndmlkZW8nOlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzVmlkZW8gPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzSW1hZ2UgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9O1xuXG4gICAgICAgIHRoaXMuaXNWaWRlb0NvdmVyID0gZnVuY3Rpb24gKHByb2R1Y3QpIHtcblxuICAgICAgICAgICAgaWYgKHByb2R1Y3QgPT0gJ3ZpZGVvJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgdGhpcy5zZXRWaWRlbyA9IGZ1bmN0aW9uKHVybCkge1xuXG5cdFx0XHRyZXR1cm4gZ2V0WW90dWJlVmlkZW9JRC5JRCh1cmwpO1xuXG4gICAgICAgIH07XG5cblx0XHR0aGlzLnRlc3QgPSAnd29yayc7XG5cblxuICAgIH1dKTtcblxufSkoKTtcbiIsIihmdW5jdGlvbigpIHtcblxuICAgIHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnbWFpbEhpc3RvcnknLCBbXSk7XG5cbiAgICBhcHAuY29udHJvbGxlcignTWFpbEhpc3RvcnlDb250cm9sbGVyJywgWyckaHR0cCcsIGZ1bmN0aW9uKCRodHRwKSB7XG5cbiAgICAgICAgdmFyIG1haWxzID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuICAgICAgICAgICAgdmFyIGJvZHkgPSAndHlwZT1yZWFkJml0ZW09bWFpbCc7XG5cbiAgICAgICAgICAgIHhoci5vcGVuKFwiUE9TVFwiLCAnL2Nvbm5lY3RzL2Nvbm5lY3RzLnBocCcsIGZhbHNlKVxuICAgICAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKVxuXG4gICAgICAgICAgICB4aHIuc2VuZChib2R5KTtcblxuICAgICAgICAgICAgdmFyIGluZm8gPSBKU09OLnBhcnNlKHhoci5yZXNwb25zZVRleHQpO1xuXG4gICAgICAgICAgICByZXR1cm4gaW5mbztcblxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5tYWlscyA9IG1haWxzKCk7XG5cbiAgICAgICAgJGh0dHAucG9zdCgnL2Nvbm5lY3RzL2Nvbm5lY3RzLnBocCcsIHtcbiAgICAgICAgICAgIHR5cGU6ICdyZWFkJyxcbiAgICAgICAgICAgIGl0ZW06ICdtYWlsJ1xuICAgICAgICB9KS5zdWNjZXNzKGZ1bmN0aW9uIChlKSB7XG5cbiAgICAgICAgfSk7XG5cbiAgICB9XSk7XG5cblxufSkoKTtcbiIsIi8vPSByZXF1aXJlIG5hdmlnYXRpb24vbmF2aWdhdGlvbi5qc1xuLy89IHJlcXVpcmUgLi4vbGlicy9uZ0RpYWxvZy5qc1xuLy89IHJlcXVpcmUgLi4vbGlicy9hbmd1bGFyLXVpLW5vdGlmaWNhdGlvbi5taW4uanNcbi8vPSByZXF1aXJlIC4uL2xpYnMvYW5ndWxhci1maWxlLXVwbG9hZC5taW4uanNcbi8vPSByZXF1aXJlIHByb2R1Y3RQYWdlL3Byb2R1Y3RQYWdlLmpzXG4vLz0gcmVxdWlyZSBwcm9kdWN0TGlzdC9wcm9kdWN0TGlzdC5qc1xuLy89IHJlcXVpcmUgc3RvcmVJbmZvL3N0b3JlSW5mby5qc1xuLy89IHJlcXVpcmUgbWV0YUluZm8vbWV0YUluZm8uanNcbi8vPSByZXF1aXJlIGFkbWluTG9naW4vYWRtaW5Mb2dpbi5qc1xuLy89IHJlcXVpcmUgcHJvZHVjdENvdmVyL3Byb2R1Y3RDb3Zlci5qc1xuLy89IHJlcXVpcmUgbWFpbEhpc3RvcnkvbWFpbEhpc3RvcnkuanNcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgTWF0ZXJpYWxpemUudXBkYXRlVGV4dEZpZWxkcygpO1xuIH0pO1xuXG4oZnVuY3Rpb24oKSB7XG5cblx0dmFyIGFwcCA9IGFuZ3VsYXIubW9kdWxlKCdzdG9yZScsIFsnbmF2aWdhdGlvbicsICdwcm9kdWN0UGFnZScsICdwcm9kdWN0TGlzdCcsICdzdG9yZUluZm8nLCAnbWV0YUluZm8nLCAnYWRtaW5Mb2dpbicsICdwcm9kdWN0Q292ZXInLCAnbWFpbEhpc3RvcnknXSwgZnVuY3Rpb24oJGh0dHBQcm92aWRlcikge1xuXG5cdFx0JGh0dHBQcm92aWRlci5kZWZhdWx0cy5oZWFkZXJzLnBvc3RbJ0NvbnRlbnQtVHlwZSddID0gJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZDtjaGFyc2V0PXV0Zi04JztcblxuXHRcdCRodHRwUHJvdmlkZXIuZGVmYXVsdHMudHJhbnNmb3JtUmVxdWVzdCA9IFtmdW5jdGlvbihkYXRhKSB7XG5cblx0XHQgICB2YXIgcGFyYW0gPSBmdW5jdGlvbihvYmopIHtcblx0XHRcdCB2YXIgcXVlcnkgPSAnJztcblx0XHRcdCB2YXIgbmFtZSwgdmFsdWUsIGZ1bGxTdWJOYW1lLCBzdWJWYWx1ZSwgaW5uZXJPYmosIGk7XG5cblx0XHRcdCBmb3IobmFtZSBpbiBvYmopXG5cdFx0XHQge1xuXHRcdFx0ICAgdmFsdWUgPSBvYmpbbmFtZV07XG5cblx0XHRcdCAgIGlmKHZhbHVlIGluc3RhbmNlb2YgQXJyYXkpXG5cdFx0XHQgICB7XG5cdFx0XHRcdCBmb3IoaT0wOyBpPHZhbHVlLmxlbmd0aDsgKytpKVxuXHRcdFx0XHQge1xuXHRcdFx0XHQgICBzdWJWYWx1ZSA9IHZhbHVlW2ldO1xuXHRcdFx0XHQgICBmdWxsU3ViTmFtZSA9IG5hbWUgKyAnWycgKyBpICsgJ10nO1xuXHRcdFx0XHQgICBpbm5lck9iaiA9IHt9O1xuXHRcdFx0XHQgICBpbm5lck9ialtmdWxsU3ViTmFtZV0gPSBzdWJWYWx1ZTtcblx0XHRcdFx0ICAgcXVlcnkgKz0gcGFyYW0oaW5uZXJPYmopICsgJyYnO1xuXHRcdFx0XHQgfVxuXHRcdFx0ICAgfVxuXHRcdFx0ICAgZWxzZSBpZih2YWx1ZSBpbnN0YW5jZW9mIE9iamVjdClcblx0XHRcdCAgIHtcblx0XHRcdFx0IGZvcihzdWJOYW1lIGluIHZhbHVlKVxuXHRcdFx0XHQge1xuXHRcdFx0XHQgICBzdWJWYWx1ZSA9IHZhbHVlW3N1Yk5hbWVdO1xuXHRcdFx0XHQgICBmdWxsU3ViTmFtZSA9IG5hbWUgKyAnWycgKyBzdWJOYW1lICsgJ10nO1xuXHRcdFx0XHQgICBpbm5lck9iaiA9IHt9O1xuXHRcdFx0XHQgICBpbm5lck9ialtmdWxsU3ViTmFtZV0gPSBzdWJWYWx1ZTtcblx0XHRcdFx0ICAgcXVlcnkgKz0gcGFyYW0oaW5uZXJPYmopICsgJyYnO1xuXHRcdFx0XHQgfVxuXHRcdFx0ICAgfVxuXHRcdFx0ICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkICYmIHZhbHVlICE9PSBudWxsKVxuXHRcdFx0ICAge1xuXHRcdFx0XHQgcXVlcnkgKz0gZW5jb2RlVVJJQ29tcG9uZW50KG5hbWUpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlKSArICcmJztcblx0XHRcdCAgIH1cblx0XHRcdCB9XG5cblx0XHRcdCByZXR1cm4gcXVlcnkubGVuZ3RoID8gcXVlcnkuc3Vic3RyKDAsIHF1ZXJ5Lmxlbmd0aCAtIDEpIDogcXVlcnk7XG5cdFx0ICAgfTtcblxuXHRcdCAgIHJldHVybiBhbmd1bGFyLmlzT2JqZWN0KGRhdGEpICYmIFN0cmluZyhkYXRhKSAhPT0gJ1tvYmplY3QgRmlsZV0nID8gcGFyYW0oZGF0YSkgOiBkYXRhO1xuXHRcdCB9XTtcblxuXHR9KTtcblxuXHRhcHAuc2VydmljZSgncHJvZHVjdHNMaXN0JywgWyckaHR0cCcsIGZ1bmN0aW9uKCRodHRwKSB7XG5cblx0XHR2YXIgcHJvZHVjdHMgPSBbXTtcblxuXHRcdHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuXHRcdHZhciBib2R5ID0gJ3R5cGU9cmVhZCZpdGVtPXByb2R1Y3QnO1xuXG5cdFx0eGhyLm9wZW4oXCJQT1NUXCIsICcvY29ubmVjdHMvY29ubmVjdHMucGhwJywgZmFsc2UpXG5cdFx0eGhyLnNldFJlcXVlc3RIZWFkZXIoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKVxuXG5cdFx0eGhyLnNlbmQoYm9keSk7XG5cblx0XHRwcm9kdWN0cyA9IEpTT04ucGFyc2UoeGhyLnJlc3BvbnNlVGV4dCk7XG5cblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHByb2R1Y3RzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRwcm9kdWN0c1tpXS52aWRlbyA9IHByb2R1Y3RzW2ldLnZpZGVvLnNwbGl0KCcsJyk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHByb2R1Y3RzO1xuXG4gICAgfV0pO1xuXG4gICAgYXBwLnNlcnZpY2UoJ2dldFlvdHViZVZpZGVvSUQnLCBbJyRzY2UnLCBmdW5jdGlvbigkc2NlKSB7XG5cblx0XHR0aGlzLklEID0gZnVuY3Rpb24odXJsKSB7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgdXJsID09PSAnc3RyaW5nJyAmJiB1cmwgIT0gJycpIHtcblxuXHRcdFx0XHR2YXIgdmlkZW9JRCA9IHVybC5tYXRjaCgvPShbXFx3LV0rKS8pWzFdO1xuXHQgICAgICAgICAgICB2YXIgdmlkZW9VcmwgPSAkc2NlLnRydXN0QXNSZXNvdXJjZVVybCgnaHR0cHM6Ly93d3cueW91dHViZS5jb20vZW1iZWQvJyArIHZpZGVvSUQpO1xuXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codmlkZW9VcmwpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZpZGVvVXJsO1xuXG5cdFx0XHR9IGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUuZXJyb3IoJ2JhZCB1cmwgJyArIHVybCk7XG4gICAgICAgICAgICB9XG5cblx0XHR9XG5cbiAgICB9XSk7XG5cblx0YXBwLmRpcmVjdGl2ZSgnYmFja0ltZycsIGZ1bmN0aW9uKCl7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbihzY29wZSwgZWxlbWVudCwgYXR0cnMpe1xuICAgICAgICAgICAgdmFyIHVybCA9IGF0dHJzLmJhY2tJbWc7XG5cdFx0XHR2YXIgc2l6ZSA9IGF0dHJzLmJhY2tTaXplO1xuXG4gICAgICAgICAgICBlbGVtZW50LmNzcyh7XG4gICAgICAgICAgICAgICAgJ2JhY2tncm91bmQnOiAndXJsKCcgKyB1cmwgKycpIG5vLXJlcGVhdCBjZW50ZXInLFxuICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kLXNpemUnIDogc2l6ZSB8fCAnY292ZXInXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICB9KTtcblxuXHRhcHAuZGlyZWN0aXZlKCdlbGFzdGljJywgWyckdGltZW91dCcsIGZ1bmN0aW9uKCR0aW1lb3V0KSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0EnLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24oJHNjb3BlLCBlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmluaXRpYWxIZWlnaHQgPSAkc2NvcGUuaW5pdGlhbEhlaWdodCB8fCBlbGVtZW50WzBdLnN0eWxlLmhlaWdodDtcbiAgICAgICAgICAgICAgICB2YXIgcmVzaXplID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRbMF0uc3R5bGUuaGVpZ2h0ID0gJHNjb3BlLmluaXRpYWxIZWlnaHQ7XG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRbMF0uc3R5bGUuaGVpZ2h0ID0gXCJcIiArIGVsZW1lbnRbMF0uc2Nyb2xsSGVpZ2h0ICsgXCJweFwiO1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgZWxlbWVudC5vbihcImlucHV0IGNoYW5nZVwiLCByZXNpemUpO1xuICAgICAgICAgICAgICAgICR0aW1lb3V0KHJlc2l6ZSwgMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cdFx0fVxuXHRdKTtcblxuXHRhcHAuZGlyZWN0aXZlKCduZ1RodW1iJywgWyckd2luZG93JywgZnVuY3Rpb24oJHdpbmRvdykge1xuXHQgICAgICAgIHZhciBoZWxwZXIgPSB7XG5cdCAgICAgICAgICAgIHN1cHBvcnQ6ICEhKCR3aW5kb3cuRmlsZVJlYWRlciAmJiAkd2luZG93LkNhbnZhc1JlbmRlcmluZ0NvbnRleHQyRCksXG5cdCAgICAgICAgICAgIGlzRmlsZTogZnVuY3Rpb24oaXRlbSkge1xuXHQgICAgICAgICAgICAgICAgcmV0dXJuIGFuZ3VsYXIuaXNPYmplY3QoaXRlbSkgJiYgaXRlbSBpbnN0YW5jZW9mICR3aW5kb3cuRmlsZTtcblx0ICAgICAgICAgICAgfSxcblx0ICAgICAgICAgICAgaXNJbWFnZTogZnVuY3Rpb24oZmlsZSkge1xuXHQgICAgICAgICAgICAgICAgdmFyIHR5cGUgPSAgJ3wnICsgZmlsZS50eXBlLnNsaWNlKGZpbGUudHlwZS5sYXN0SW5kZXhPZignLycpICsgMSkgKyAnfCc7XG5cdCAgICAgICAgICAgICAgICByZXR1cm4gJ3xqcGd8cG5nfGpwZWd8Ym1wfGdpZnwnLmluZGV4T2YodHlwZSkgIT09IC0xO1xuXHQgICAgICAgICAgICB9XG5cdCAgICAgICAgfTtcblxuXHQgICAgICAgIHJldHVybiB7XG5cdCAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXG5cdCAgICAgICAgICAgIHRlbXBsYXRlOiAnPGNhbnZhcy8+Jyxcblx0ICAgICAgICAgICAgbGluazogZnVuY3Rpb24oc2NvcGUsIGVsZW1lbnQsIGF0dHJpYnV0ZXMpIHtcblx0ICAgICAgICAgICAgICAgIGlmICghaGVscGVyLnN1cHBvcnQpIHJldHVybjtcblxuXHQgICAgICAgICAgICAgICAgdmFyIHBhcmFtcyA9IHNjb3BlLiRldmFsKGF0dHJpYnV0ZXMubmdUaHVtYik7XG5cblx0ICAgICAgICAgICAgICAgIGlmICghaGVscGVyLmlzRmlsZShwYXJhbXMuZmlsZSkpIHJldHVybjtcblx0ICAgICAgICAgICAgICAgIGlmICghaGVscGVyLmlzSW1hZ2UocGFyYW1zLmZpbGUpKSByZXR1cm47XG5cblx0ICAgICAgICAgICAgICAgIHZhciBjYW52YXMgPSBlbGVtZW50LmZpbmQoJ2NhbnZhcycpO1xuXHQgICAgICAgICAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG5cblx0ICAgICAgICAgICAgICAgIHJlYWRlci5vbmxvYWQgPSBvbkxvYWRGaWxlO1xuXHQgICAgICAgICAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwocGFyYW1zLmZpbGUpO1xuXG5cdCAgICAgICAgICAgICAgICBmdW5jdGlvbiBvbkxvYWRGaWxlKGV2ZW50KSB7XG5cdCAgICAgICAgICAgICAgICAgICAgdmFyIGltZyA9IG5ldyBJbWFnZSgpO1xuXHQgICAgICAgICAgICAgICAgICAgIGltZy5vbmxvYWQgPSBvbkxvYWRJbWFnZTtcblx0ICAgICAgICAgICAgICAgICAgICBpbWcuc3JjID0gZXZlbnQudGFyZ2V0LnJlc3VsdDtcblx0ICAgICAgICAgICAgICAgIH1cblxuXHQgICAgICAgICAgICAgICAgZnVuY3Rpb24gb25Mb2FkSW1hZ2UoKSB7XG5cdCAgICAgICAgICAgICAgICAgICAgdmFyIHdpZHRoID0gcGFyYW1zLndpZHRoIHx8IHRoaXMud2lkdGggLyB0aGlzLmhlaWdodCAqIHBhcmFtcy5oZWlnaHQ7XG5cdCAgICAgICAgICAgICAgICAgICAgdmFyIGhlaWdodCA9IHBhcmFtcy5oZWlnaHQgfHwgdGhpcy5oZWlnaHQgLyB0aGlzLndpZHRoICogcGFyYW1zLndpZHRoO1xuXHQgICAgICAgICAgICAgICAgICAgIGNhbnZhcy5hdHRyKHsgd2lkdGg6IHdpZHRoLCBoZWlnaHQ6IGhlaWdodCB9KTtcblx0ICAgICAgICAgICAgICAgICAgICBjYW52YXNbMF0uZ2V0Q29udGV4dCgnMmQnKS5kcmF3SW1hZ2UodGhpcywgMCwgMCwgd2lkdGgsIGhlaWdodCk7XG5cdCAgICAgICAgICAgICAgICB9XG5cdCAgICAgICAgICAgIH1cblx0ICAgICAgICB9O1xuXHQgICAgfV0pO1xuXG59KSgpO1xuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
