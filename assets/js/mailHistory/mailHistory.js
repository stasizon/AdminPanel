(function() {

    var app = angular.module('mailHistory', []);

    app.controller('MailHistoryController', ['$http', function($http) {

        var mails = function() {

            var xhr = new XMLHttpRequest();

            var body = 'type=read&item=mail';

            xhr.open("POST", '/connects/connects.php', false)
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

            xhr.send(body);

            var info = JSON.parse(xhr.responseText);

            return info;

        }

        this.mails = mails();

        $http.post('/connects/connects.php', {
            type: 'read',
            item: 'mail'
        }).success(function (e) {

        });

    }]);


})();
