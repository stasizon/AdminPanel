(function() {

	var app = angular.module('productPage', ['ui-notification', 'angularFileUpload']);

    app.controller('ProductPageController', ['ngDialog', 'Notification', 'productsList', 'FileUploader', '$rootScope', function(ngDialog, Notification, productsList, FileUploader, $rootScope) {

		$rootScope.$on('ngDialog.opened', function (e, $dialog) {
    		$('select').material_select();
			Materialize.updateTextFields();
		});

		this.uploader = new FileUploader({ url: '/connects/upload.php' });

        this.uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };

		this.remove = function(event, product, videoIndex) {
			productsList[product.index].video.splice(videoIndex, 1)
			// delete productsList[product.index].video[videoIndex];
			// event.srcElement.parentNode.style.display = 'none';
			// productsList[product.index].video.length = productsList[product.index].video.length - 1;
			// console.log(event, product, videoIndex);
			console.log(productsList);
		}

		this.deleteVideo = function(index, productIndex) {
			console.log(index, productIndex);
		}

		this.saveProduct = function(product, que) {

			if (que.length == 0) {

				console.log('Image not upload');
				productsList[product.index].image = 'video';

			}

			this.uploader.uploadAll();

			for (var i = 0; i < que.length; i++) {
				console.log(que);
				productsList[product.index].image = que[0].file.name;
			};


			// console.log(product.index, que[0].file.name);
		    Notification.success('Товар "' + product.product.name +'" изменен');
		};

    }]);

})();
