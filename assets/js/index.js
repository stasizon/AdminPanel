//= require navigation/navigation.js
//= require ../libs/ngDialog.js
//= require ../libs/angular-ui-notification.min.js
//= require ../libs/angular-file-upload.min.js
//= require productPage/productPage.js
//= require productList/productList.js
//= require storeInfo/storeInfo.js
//= require metaInfo/metaInfo.js
//= require adminLogin/adminLogin.js
//= require productCover/productCover.js
//= require mailHistory/mailHistory.js

$(document).ready(function() {
    Materialize.updateTextFields();
 });

(function() {

	var app = angular.module('store', ['navigation', 'productPage', 'productList', 'storeInfo', 'metaInfo', 'adminLogin', 'productCover', 'mailHistory'], function($httpProvider) {

		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

		$httpProvider.defaults.transformRequest = [function(data) {

		   var param = function(obj) {
			 var query = '';
			 var name, value, fullSubName, subValue, innerObj, i;

			 for(name in obj)
			 {
			   value = obj[name];

			   if(value instanceof Array)
			   {
				 for(i=0; i<value.length; ++i)
				 {
				   subValue = value[i];
				   fullSubName = name + '[' + i + ']';
				   innerObj = {};
				   innerObj[fullSubName] = subValue;
				   query += param(innerObj) + '&';
				 }
			   }
			   else if(value instanceof Object)
			   {
				 for(subName in value)
				 {
				   subValue = value[subName];
				   fullSubName = name + '[' + subName + ']';
				   innerObj = {};
				   innerObj[fullSubName] = subValue;
				   query += param(innerObj) + '&';
				 }
			   }
			   else if(value !== undefined && value !== null)
			   {
				 query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
			   }
			 }

			 return query.length ? query.substr(0, query.length - 1) : query;
		   };

		   return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
		 }];

	});

	app.service('productsList', ['$http', function($http) {

		var products = [];

		var xhr = new XMLHttpRequest();

		var body = 'type=read&item=product';

		xhr.open("POST", '/connects/connects.php', false)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

		xhr.send(body);

		products = JSON.parse(xhr.responseText);

		for (var i = 0; i < products.length; i++) {
			products[i].video = products[i].video.split(',');
		}

		return products;

    }]);

    app.service('getYotubeVideoID', ['$sce', function($sce) {

		this.ID = function(url) {

            if (typeof url === 'string' && url != '') {

				var videoID = url.match(/=([\w-]+)/)[1];
	            var videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoID);

                console.log(videoUrl);

                return videoUrl;

			} else {
                // console.error('bad url ' + url);
            }

		}

    }]);

	app.directive('backImg', function(){
        return function(scope, element, attrs){
            var url = attrs.backImg;
			var size = attrs.backSize;

            element.css({
                'background': 'url(' + url +') no-repeat center',
                'background-size' : size || 'cover'
            });
        };
    });

	app.directive('elastic', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, element) {
                $scope.initialHeight = $scope.initialHeight || element[0].style.height;
                var resize = function() {
                    element[0].style.height = $scope.initialHeight;
                    element[0].style.height = "" + element[0].scrollHeight + "px";
                };
                element.on("input change", resize);
                $timeout(resize, 0);
            }
        };
		}
	]);

	app.directive('ngThumb', ['$window', function($window) {
	        var helper = {
	            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
	            isFile: function(item) {
	                return angular.isObject(item) && item instanceof $window.File;
	            },
	            isImage: function(file) {
	                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
	                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	            }
	        };

	        return {
	            restrict: 'A',
	            template: '<canvas/>',
	            link: function(scope, element, attributes) {
	                if (!helper.support) return;

	                var params = scope.$eval(attributes.ngThumb);

	                if (!helper.isFile(params.file)) return;
	                if (!helper.isImage(params.file)) return;

	                var canvas = element.find('canvas');
	                var reader = new FileReader();

	                reader.onload = onLoadFile;
	                reader.readAsDataURL(params.file);

	                function onLoadFile(event) {
	                    var img = new Image();
	                    img.onload = onLoadImage;
	                    img.src = event.target.result;
	                }

	                function onLoadImage() {
	                    var width = params.width || this.width / this.height * params.height;
	                    var height = params.height || this.height / this.width * params.width;
	                    canvas.attr({ width: width, height: height });
	                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
	                }
	            }
	        };
	    }]);

})();
