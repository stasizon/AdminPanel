(function() {

	var app = angular.module('metaInfo', ['ui-notification']);

    app.controller('MetaInfoController', ['ngDialog', 'Notification', '$http', function(ngDialog, Notification, $http) {

		var metaInfo = function() {

			var xhr = new XMLHttpRequest();

			var body = 'type=read&item=meta';

			xhr.open("POST", '/connects/connects.php', false)
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

			xhr.send(body);

			var meta = JSON.parse(xhr.responseText);

			return meta;

		};

		this.metaInfo = metaInfo();

		this.save = function(info) {

			console.log(info);

			$http.post('/connects/connects.php', {

                type: 'write',
                item: 'meta',
				title: document.getElementById('title').value,
				description: document.getElementById('metaDescription').value,
				keywords: document.getElementById('metaTags').value

				}).success(function(response) {

				Notification.success('Сохранено');

            });

		};

    }]);

})();
