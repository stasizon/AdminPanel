(function() {

	var app = angular.module('productList', ['ngDialog', 'ui-notification']);

    app.controller('ProductController', ['$http', 'ngDialog', 'Notification', 'productsList', function($http, ngDialog, Notification, productsList) {

		var store = this;
		store.products = productsList;

		// $http.post('/connects/connects.php', {
		// 	type: 'read',
		// 	item: 'product'
		// 	}).success(function(response) {
		// 	store.products = response;
		// 	console.log(response);
		// });

		// var handleSuccess = function(data, status) {
		// 	store.products = data;
  //  		};
		//
		// productsList.getSessions().success(handleSuccess);

        this.removeProduct = function(index) {
            document.getElementsByClassName('productList__item')[index].style.display='none';
            delete productsList[index];
			console.log(productsList);
        }

		this.openEditPage = function(index) {

			console.log(index);

			ngDialog.open({
				template: 'productEditPage',
				className: 'ngdialog-theme-plain',
                data: {
					product: store.products[index],
					index: index
				}
			});

		}

		this.addProduct = function() {
			store.products[store.products.length] = {
				name: "Новый товар",
		        description: "Описание товара.",
		        price: "100",
		        image: "img/server.svg",
		        video: ""
			}

			console.log(store.products);
		}

		this.save = function() {

			// product.product.video = product.product.video.join(',');

			var changedList = {};

			for (var i = 1; i < store.products.length + 1; i++) {
				changedList.type = 'write';
				changedList.item = 'product';
				changedList['product' + i] = store.products[i - 1];

				if (changedList['product' + i] === true) {

					changedList['product' + i].video = changedList['product' + i].video.join(',');
					console.log(changedList['product' + i]);

				}

				delete changedList.id;
			}

			console.log(changedList);

			$http.post('/connects/connects.php', changedList).success(function(response) {

				Notification.success(response);

				// location.reload();

			 });

		}

    }]);

})();
