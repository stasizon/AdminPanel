(function() {

	var app = angular.module('productCover', []);

    app.controller('productCoverController', ['$sce', 'getYotubeVideoID', function($sce, getYotubeVideoID) {

        this.isVideo = false;
        this.isImage = true;

        this.changeType = function () {

            switch (this.productCoverType) {
                case 'image':
                    this.isImage = true;
                    this.isVideo = false;
                    break;
                case 'video':
                    this.isVideo = true;
                    this.isImage = false;
                    break;

            }

        };

        this.isVideoCover = function (product) {

            if (product == 'video') {
                return true;
            } else {
                return false;
            }
        };

        this.setVideo = function(url) {

			return getYotubeVideoID.ID(url);

        };

		this.test = 'work';


    }]);

})();
