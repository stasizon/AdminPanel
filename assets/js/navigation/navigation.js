(function() {

	var app = angular.module('navigation', []);

    app.controller('NavigationController', function() {

        this.showOnWorkField = function(element, event) {

			for (var i = 0; i < document.getElementById('nav').children.length; i++) {
                document.getElementById('nav').children[i].classList.remove('nav__item-active');
            }

			event.target.classList.add('nav__item-active');

            for (var i = 0; i < document.getElementById('workField').children.length; i++) {
                document.getElementById('workField').children[i].style.display = 'none';
            }

    	    document.getElementById(element).style.display = 'block';
    	}

    });

})();
