(function() {

	var app = angular.module('storeInfo', ['ui-notification', 'angularFileUpload']);

    app.controller('StoreInfoController', ['ngDialog', 'Notification', '$http', 'FileUploader', function(ngDialog, Notification, $http, FileUploader) {

		this.uploaderOne = new FileUploader({ url: '/connects/upload.php' });
		this.uploaderTwo = new FileUploader({ url: '/connects/upload.php' });

		this.uploaderOne.onSuccessItem = function(fileItem, response, status, headers) {
			console.info('onSuccessItem', fileItem, response, status, headers);
		};

		this.uploaderTwo.onSuccessItem = function(fileItem, response, status, headers) {
			console.info('onSuccessItem', fileItem, response, status, headers);
		};

        var info = function() {

			var xhr = new XMLHttpRequest();

			var body = 'type=read&item=configs';

			xhr.open("POST", '/connects/connects.php', false)
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

			xhr.send(body);

			var info = JSON.parse(xhr.responseText);

			return info;

        }

        this.info = info();

		this.save = function(info, img1, img2) {

			console.log(img1.length, img2.length);

			if (img1.length != 0 && img2.length != 0) {

				$http.post('/connects/connects.php', {
					type: 'write',
					item: 'configs',
					configs1: {
						name: 'text1',
						content: info[0].content
					},
					configs2: {
						name: 'text2',
						content: info[1].content
					},
					configs3: {
						name: 'text3',
						content: info[2].content
					},
					configs4: {
						name: 'text4',
						content: info[3].content
					},
					configs5: {
						name: 'text5',
						content: info[4].content
					},
					configs6: {
						name: 'text6',
						content: info[5].content
					},
					configs7: {
						name: 'text7',
						content: info[6].content
					},
					configs8: {
						name: 'text8',
						content: info[7].content
					},
					configs9: {
						name: 'imagePath1',
						content: img1[0].file.name
					},
					configs10: {
						name: 'imagePath2',
						content: img2[0].file.name
					},
					configs11: {
						name: 'heroVideo',
						content: info[10].content || undefined
					}
				}).success(function (e) {
					Notification.success('Сохранено');
				});

			} else {
				Notification.error('Не все поля заполнены');
			}


		}

    }]);

})();
