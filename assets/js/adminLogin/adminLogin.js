(function() {

	var app = angular.module('adminLogin', []);

    app.controller('AdminLoginController', ['$http', 'Notification', function($http, Notification) {

        this.submit = function() {


            $http.post('/connects/connects.php', {
                type: 'admlogin',
                login: document.getElementById('adminLogin').value,
                pass: document.getElementById('adminPass').value
                }).success(function(response) {

                if (response.replace(/\n$/m, '') == 'ok') {
                    location.reload();
                } else {
					console.log(response, 'wrong');
					console.log(response == 'ok&#010', response == 'ok');
					Notification.error({message: response, delay: 1000});
                    location.reload();
				}
            });

        }


    }]);


})();
