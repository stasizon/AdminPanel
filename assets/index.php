<?php
include("../classes/_class.config.php"); //файл с доступами к БД
include("../classes/_class.db.php"); //файл с классом БД

# Класс с доступами к БД
$config = new config;

# Класс Базы данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);
@session_start();
?>
<html lang="en" ng-app="store"></html>
<head>
  <meta charset="UTF-8"/>
  <meta name="description" content="example"/>
  <meta name="keywords" content="example"/>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
  <script src="js/index.js"></script>
  <link rel="stylesheet" href="index.css"/>
  <title>ADMIN PANEL</title>
</head>
<body>
<?php
# Блокировка сессии
if(isset($_SESSION["admin"])){
?>
  <nav id="nav" ng-controller="NavigationController as navCtrl" class="nav column">
    <div ng-click="navCtrl.showOnWorkField('productList', $event)" class="nav__item">Список продуктов</div>
    <div ng-click="navCtrl.showOnWorkField('storeInfo', $event)" class="nav__item">Информация в магазине</div>
    <div ng-click="navCtrl.showOnWorkField('metaInfo', $event)" class="nav__item">Мета теги</div>
  </nav>
  <div id="workField" class="workField">
    <div id="productList" ng-controller="ProductController as list" class="productList">
      <h2 class="text_color_dark">Список товаров</h2>
      <div class="productList__list">
        <div ng-repeat="product in list.products track by $index" class="productList__item">
          <div ng-click="list.removeProduct($index)" class="button button_light productList__item-remove">×</div>
          <div class="productList__item-name">{{product.name}}</div>
          <div back-img="{{'../' + product.image}}" back-size="contain" class="productList__item-image"></div>
          <p class="productList__item-description">{{product.description}}</p>
          <div class="productList__item-price">{{product.price}} руб.</div>
          <div ng-click="list.openEditPage($index)" class="button button_dark productList__item-button">Редактировать</div>
        </div>
      </div>
      <div ng-click="list.save()" class="button button__save button_color_dark"></div>
    </div>
  </div><script type="text/ng-template" id="productEditPage">
  <div ng-controller="ProductPageController as page" class="productPage">
    <div class="row productPage__row"><span class="productPage__label">Название товара:</span>
      <input ng-model="ngDialogData.product.name" class="input productPage__input"/>
    </div>
    <div class="row productPage__row"><span class="productPage__label">Изображение:</span>
      <input type="file" ng-model="ngDialogData.product.image" class="input productPage__input"/>
    </div>
    <div class="row productPage__row"><span class="productPage__label">Описание:</span>
      <textarea ng-model="ngDialogData.product.description" elastic="elastic" class="textarea input productPage__input"></textarea>
    </div>
    <div class="row productPage__row"><span class="productPage__label">Цена:</span>
      <input ng-model="ngDialogData.product.price" class="input productPage__input"/>
    </div>
    <div class="row productPage__row"><span class="productPage__label">Видео:</span>
      <input ng-repeat="video in ngDialogData.product.video track by $index" ng-model="ngDialogData.product.video[$index]" class="input productPage__input"/>
    </div>
    <div class="row productPage__row">
      <button ng-click="page.saveProduct(ngDialogData)" class="button button_dark productPage__button">Сохранить</button>
    </div>
  </div></script>
<?php
}else{
?>
<form ng-submit="login.submit()" ng-controller="AdminLoginController as login">

    Логин:
      <input type="text" name="login" value="">
    Пароль:
      <input type="text" name="password" value="">
      <input type="submit" value="Авторизироватся">

</form>
<?php
}
?>
</body>
